// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// ignore_for_file: avoid_print

import 'package:flutter_test/flutter_test.dart';
import 'package:orginone/utils/encryption_util.dart';

void main() {
  test('Test String', () {
    // String str = "dddd不在线aaad";
    // LogUtil.d(str.contains('不在线'));
    // LogUtil.d(str.contains('ccc'));
    String str =
        "^!:fnyJpeJxTNXXKT8pSNXVRNXdSNTJKyk+pBFKqxo5A0tgYxDRyBpK5qXklmfl5xVA5UyewDhcA6zoPcQ**3bEji";
    print(EncryptionUtil.inflate(str));
    print(EncryptionUtil.inflate(
        "^!:rfBKeeJxTNXXKT8pSNXVRNXdSNTJKyk+pBFKqxo5AEkiDkDOQzE3NK8nMzyuGypk6gXW4AADq9w9vJFXYi"));
    print(EncryptionUtil.inflate(
        "^!:wTFSDeJxTNXXKT8pSNXVRNXdSNTJKyk+pBFKqxo5A0tAQxDRyBpK5qXklmfl5xVA5UyewDhcA6rQPbQ**GGtYh"));
    print(EncryptionUtil.inflate(
        "^!:Q8YBpeJwtjsEOwjAMQ7/m3aEQmh4T2n3IBIdNYj3Ahb8n3ZAix7KtOIj3eUUq2Ulp7o9vLC42sN0oSplogjU0k060giY0iGITfoiKy5hIFsfqLgpq2EHiQrgZb9h15PU83GhJ98DXc/ssfXv/y8X3l+oP6xsjGg**pp6JK"));
  });
}
