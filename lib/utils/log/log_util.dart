// ignore_for_file: depend_on_referenced_packages

import 'package:logger/logger.dart';

import '../../dart/base/common/systemError.dart';

class XLogUtil {
  static final Logger _logger = Logger(
    printer: PrefixPrinter(PrettyPrinter(
      stackTraceBeginIndex: 1,
      methodCount: 1,
      // lineLength: 12800,
      printEmojis: true,
      colors: true,
    )),
  );

  static void d(dynamic message, {String? tag}) {
    _logger.d(message);
  }

  static void v(dynamic message) {
    _logger.v(message);
  }

  static void i(dynamic message) {
    _logger.i(message);
  }

  static void w(dynamic message) {
    _logger.w(message);
  }

  static void e(dynamic message) {
    _logger.e(message);
    SystemLog.err(message);
  }

  static void dd(dynamic message) {
    // print(message);
    _logger.d(message);
    // if (kDebugMode) {
    //   SystemLog.err(message);
    // }
  }

  static void wtf(dynamic message) {
    _logger.wtf(message);
  }
}
