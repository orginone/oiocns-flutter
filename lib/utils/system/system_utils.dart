/*
 * @Descripttion: 
 * @version: 
 * @Author: congsir
 * @Date: 2022-12-15 15:05:37
 * @LastEditors: 
 * @LastEditTime: 2022-12-15 16:21:22
 */

import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// 系统工具类
class SystemUtils {
  /// 拷贝文本内容到剪切板
  static bool copyToClipboard(String? text,
      {String? successMessage, BuildContext? context}) {
    if (null != text) {
      Clipboard.setData(ClipboardData(text: text));
      if (context != null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: const Duration(seconds: 1),
            content: Text(successMessage ?? "copy success")));
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  /// 隐藏软键盘，具体可看：TextInputChannel
  static void hideKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  /// 展示软键盘，具体可看：TextInputChannel
  static void showKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.show');
  }

  /// 清除数据
  static void clearClientKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.clearClient');
  }

  /// 获得设备ID
  static String? _deviceId;
  static String getDeviceId() {
    return _deviceId ?? "";
  }

  static Future<String> getDeviceIdSync() async {
    if (kIsWeb) {
      _deviceId = "web";
      return _deviceId!;
    } else if (null != _deviceId) {
      return _deviceId ?? "";
    }

    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      _deviceId = androidInfo.id;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      _deviceId = iosInfo.identifierForVendor;
    } else {
      _deviceId = "unknown";
    }
    return _deviceId ?? "";
  }

  /// 数据换位
  static void swapEntities<T>(List<T> list, int indexA, int indexB) {
    final T temp = list[indexA];
    list[indexA] = list[indexB];
    list[indexB] = temp;
  }
}
