
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../components/TextKeyWidget/TextKeyWidget.dart';
import '../../../../components/form/components/XChoice/XChoice.dart';
import '../../../../config/theme/unified_style.dart';
import '../../../../routers/pages.dart';

class ClauseWidget extends StatelessWidget {
  final ValueChanged<bool>? changed;
  final bool isSelected;

  const ClauseWidget({Key? key,
    required this.isSelected,
    this.changed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        XChoice(
            isSelected: isSelected,
            iconSize: 24.w,
            changed: changed),
        SizedBox(
          width: 10.w,
        ),
        TextKeyWidget(
          data: "同意《服务条款》与《隐私条款》",
          keys: const ['《服务条款》', '《隐私条款》'],
          style: TextStyle(color: Colors.black, fontSize: 20.sp),
          keyStyle: TextStyle(color: XColors.themeColor, fontSize: 20.sp),
          onTapCallback: (String key) {
            if (key == '《服务条款》') {
              RoutePages.jumpAssectMarkDown(
                  title: "服务条款",
                  path:
                  "assets/markdown/term_service.md");
            } else if (key == '《隐私条款》') {
              RoutePages.jumpAssectMarkDown(
                  title: "隐私条款",
                  path:
                  "assets/markdown/privacy_clause.md");
            }
          },
        ),
      ],
    );
  }
}