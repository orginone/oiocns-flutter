import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/index.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../../../components/MySettingWidget/components/ErrorWidget/ErrorListWidget.dart';

class LoginTransPage extends StatefulWidget {
  const LoginTransPage({super.key});

  @override
  State<LoginTransPage> createState() => _LoginTransPageState();
}

class _LoginTransPageState extends State<LoginTransPage>
    with ErrorListenerMixin {
  @override
  late BuildContext context;
  late String _key = "login_key";

  @override
  void initState() {
    super.initState();
    try {
      XLogUtil.dd(">>>>>>login_trans ${relationCtrl.provider.inited}");
      _key = relationCtrl.subscribe((key, args) {
        XLogUtil.dd(
            "App启动：$args ${RoutePages.routeData.pageHistorys.isNotEmpty}");
        if (RoutePages.routeData.pageHistorys.isNotEmpty) {
          return;
        }

        bool success = false;
        if (null != args && args.isNotEmpty) {
          success = args[0];
        }
        relationCtrl.appStartController.onStarted(success);
        if (success) {
          RoutePages.jumpHome();
        }
      }, relationCtrl.provider.inited);
    } catch (e) {
      rethrow;
    }
  }

  @override
  void dispose() {
    super.dispose();
    relationCtrl.unsubscribe(_key);
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    init(context);
    return Scaffold(
      body: _login(),
    );
  }

  Widget _login() {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: const BoxDecoration(color: Colors.white),
      child: Stack(
        children: [
          //背景图模块
          background(),
          //奥集能 模块
          logo(),
          //文字Orginone 区域
          Positioned(
            left: 0,
            right: 0,
            top: (MediaQuery.maybeOf(context)?.size.height ?? 600) * 0.30,
            child: const Text(
              '物以类聚  人以群分',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF366EF4),
                fontSize: 25,
                fontFamily: 'DingTalk',
                fontWeight: FontWeight.w400,
                height: 0.06,
              ),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            top: (MediaQuery.maybeOf(context)?.size.height ?? 600) * 0.33,
            child: const Text(
              'Orginone',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF366EF4),
                fontSize: 18,
                fontFamily: 'DingTalk',
                fontWeight: FontWeight.w400,
                height: 0,
              ),
            ),
          ),
          // Positioned(left: 0, right: 0, bottom: 0, child: OrginoneProgressBar())
        ],
      ),
    );
  }

  //logo
  Widget logo() {
    return Positioned(
      bottom: 100.h,
      left: 0,
      right: 0,
      child: GestureDetector(
        onLongPress: () {
          RoutePages.to(
            context: context,
            path: Routers.errorPage,
          );
        },
        child: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            XImage.localImage(XImage.logoLoading, width: 120)
            // Container(
            //   margin: const EdgeInsets.fromLTRB(0, 5, 5, 0),
            //   width: 30,
            //   height: 30,
            //   clipBehavior: Clip.antiAlias,
            //   decoration: const BoxDecoration(
            //     image: DecorationImage(
            //       image: AssetImage(XImage.logoNotBg),
            //       fit: BoxFit.fill,
            //     ),
            //   ),
            // ),
            // const Text.rich(TextSpan(
            //   text: '奥集能',
            //   style: TextStyle(
            //     color: Color(0xFF15181D),
            //     fontSize: 17,
            //     fontFamily: 'PingFang SC',
            //     fontWeight: FontWeight.w500,
            //   ),
            // )),
          ],
        )),
      ),
    );
  }

  //背景图
  Widget background() {
    return Stack(
      children: [
        Positioned(
          left: -200,
          child: Container(
            width: 900,
            height: 500,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(XImage.logoBackground),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Positioned(
          left: -200,
          child: Container(
            width: 900,
            height: 500,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromRGBO(249, 249, 249, 0),
                  Color.fromRGBO(255, 255, 255, 1),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
