import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/work/task.dart';
import 'package:orginone/routers/pages.dart';
import 'package:orginone/utils/date_util.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../../../../components/ListSearchWidget/ListSearchWidget.dart';
import '../../../../dart/base/ui.dart';

/// 集群列表
class GroupDataList<T extends IEntityUI> extends StatefulWidget {
  GroupDataList({Key? key, this.data = const [], this.spaceEntity})
      : super(key: key);
  final List<T> data;
  IEntityUI? spaceEntity;
  @override
  State<GroupDataList> createState() => _GroupDataListState<T>();
}

class _GroupDataListState<T extends IEntityUI> extends State<GroupDataList> {
  late List<T> data;
  IEntityUI? spaceEntity;
  TextEditingController searchController = TextEditingController();
  dynamic searchData;

  @override
  void initState() {
    super.initState();
    data = widget.data as List<T>;
    spaceEntity = widget.spaceEntity;
    searchData = widget.data;
  }

  @override
  Widget build(BuildContext context) {
    return XScaffold(
        titleWidget: _titleWidget(), body: _buildList(context, searchData));
  }

  _buildList(BuildContext context, List<T> datas) {
    return ListSearchWidget<T>(
      initDatas: datas,
      // getDatas: ([dynamic parentData]) {
      //   if (null == parentData) {
      //     return datas;
      //   }
      //   return [];
      // },
      getBadge: (dynamic data) {
        if (data is ISession) {
          return data.noReadCount;
        }
        return null;
      },
      getAction: (dynamic dataitem) {
        return Text(CustomDateUtil.getSessionTime(dataitem.updateTime),
            style: XFonts.chatSMTimeTip, textAlign: TextAlign.right
            // ),
            );
      },
      onTap: (dynamic data, List children) async {
        XLogUtil.d('>>>>>>======点击了列表项 ${data.name}');
        if (data is ISession) {
          RoutePages.jumpChatSession(data: data);
        } else if (data is IWorkTask) {
          await RoutePages.jumpWorkInfo(work: data);
        }
      },
    );
  }

  Widget _titleWidget() {
    String name = spaceEntity?.name ?? "";
    String endTypeTxt = '';
    if (data is List<ISession>) {
      endTypeTxt = '集群消息';
    } else if (data is List<IWorkTask>) {
      endTypeTxt = '待办';
    }
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 5),
          child: XImage.entityIcon(spaceEntity, width: 40.w),
        ),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('$name$endTypeTxt',
                maxLines: 1,
                style: XFonts.size22Black3
                    .merge(const TextStyle(overflow: TextOverflow.ellipsis))),
          ],
        ))
      ],
    );
  }
}
