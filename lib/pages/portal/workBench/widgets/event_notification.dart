import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/dart/base/ui.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/target/team/company.dart';
import 'package:orginone/dart/core/work/task.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/index.dart';
import 'widget_workbench_common.dart';

/// 事项提醒
class EventNotificationPage extends StatefulWidget {
  EventNotificationPage({Key? key, this.currentCompany}) : super(key: key);
  IEntityUI? currentCompany;

  @override
  State<EventNotificationPage> createState() => _EventNotificationPageState();
}

class _EventNotificationPageState extends State<EventNotificationPage> {
  String noReadCount = '0';
  List<IWorkTask> todoList = [];

  @override
  void initState() {
    super.initState();
    // relationCtrl.work.subscribe((key, args) {
    //   updateData();
    // });
    // relationCtrl.provider.chatProvider?.subscribe((key, args) {
    //   updateData();
    // });
  }

  updateData() {
    if (mounted) {
      setState(() {
        if (widget.currentCompany == null) {
          todoList = relationCtrl.work.todos.value;
          noReadCount =
              relationCtrl.provider.chatProvider?.noReadChatCount.toString() ??
                  "0";
        } else {
          todoList = relationCtrl.work.todos.value
              .where(
                  (element) => element.ibelong?.id == widget.currentCompany?.id)
              .toList();
          if (widget.currentCompany is ICompany) {
            ICompany iCompany = widget.currentCompany as ICompany;
            int noread = 0;
            for (var item in iCompany.groups) {
              for (var chat in item.chats) {
                noread += chat.chatdata.noReadCount;
              }
            }
            noReadCount = noread.toString();
          }
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    updateData();
    return _renderWorkV1();
  }

  Widget _renderWorkV1() {
    return modelWindow("事项提醒",
        contentWidget: Container(
            // color: Colors.red,
            padding: EdgeInsets.only(left: 10.w, bottom: 10.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween, // 子元素之间的对齐方式
              mainAxisSize: MainAxisSize.max,
              children: [
                _buildWorkItemV1("集群未读", noReadCount, XImage.homeChat,
                    const Color(0x205B66FF), onTab: () {
                  // RoutePages.jumpChat();
                  jumpSession();
                }),
                _buildWorkItemV1("待办", todoList.length.toString(),
                    XImage.homeWork, const Color(0x20FF9A5C), onTab: () {
                  // RoutePages.jumpWork();
                  RoutePages.jumpGroupDataListByCompany<IWorkTask>(
                      context, todoList, widget.currentCompany);
                }),
                _buildWorkItemV1(
                    "任务", "0", XImage.homeTask, const Color(0x2059D8A5)),
                _buildWorkItemV1(
                    "提醒", "0", XImage.homeRemind, const Color(0x20E1516B))
              ],
            )),
        moreWidget: Container());
  }

  Widget _buildWorkItemV1(
      String title, String count, String icon, Color bgColor,
      {void Function()? onTab}) {
    return Expanded(
        child: GestureDetector(
            onTap: onTab,
            child: Container(
                decoration: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  color: bgColor,
                ),
                padding: const EdgeInsets.all(10),
                margin: EdgeInsets.only(right: 10.w),
                child: Column(
                  children: [
                    XImage.localImage(icon, color: bgColor.withOpacity(1)),
                    Text(title, style: const TextStyle(fontSize: 12)),
                    Text(count,
                        style: const TextStyle(
                          fontSize: 22,
                          height: 1.5,
                        )),
                  ],
                ))));
  }

  jumpSession() {
    // IChatProvider? chatProvider = relationCtrl.provider.chatProvider;
    // List<ISession>? chats = chatProvider?.chats ?? [];
    List<ISession>? chats = [];
    if (widget.currentCompany is ICompany) {
      ICompany company = widget.currentCompany as ICompany;
      chats = company.chats;
    }
    chats = chats
        .where((element) =>
            element.groupTags.contains("集群") ||
            element.groupTags.contains("组织群"))
        .toList();
    RoutePages.jumpGroupDataListByCompany<ISession>(
        context, chats, widget.currentCompany);
  }
}
