//更多操作
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XButton/XButton.dart';
import 'package:orginone/components/XDialogs/XDialog.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/routers/pages.dart';

import '../../../../components/XImage/XImage.dart';
import 'dashed_lind.dart';

Widget modelWindow(String title,
    {Function()? onMoreTap, Widget? moreWidget, Widget? contentWidget}) {
  return Container(
    decoration: const BoxDecoration(
      color: Colors.white,
      // borderRadius: BorderRadius.all(Radius.circular(5.0)),
    ),
    margin: const EdgeInsets.fromLTRB(0, 1, 0, 0),
    padding: const EdgeInsets.only(top: 5),
    child: Column(
      children: [
        XDialog.headInfoWidget(title,
            action: moreWidget ??
                (null != onMoreTap
                    ? XButton.iconCustomText(
                        text: '更多',
                        icon: XImage.localImage(XImage.iconArrawright, width: 8),
                        iconLeft: false,
                        bgcolor: XColors.transparent,
                        textColor: XColors.black,
                        onPressed: () {
                          if (title == "快捷操作") {
                            RoutePages.jumpRelation();
                          } else {
                            onMoreTap.call();
                          }
                        },
                      )
                    : Container())),
        SizedBox(
          height: 10.h,
        ),
        contentWidget ?? const Column(),
      ],
    ),
  );
}

renderDataWidget(String name, String useSize, String totalSize) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 4),
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: Row(
            children: [
              Row(children: [
                Container(
                  width: 2,
                  height: 20.sp,
                  color: XColors.primary,
                ),
                const SizedBox(width: 2),
                Text(name,
                    style: TextStyle(
                        fontSize: 20.sp, color: const Color(0xff4e5969)))
              ]),
              Expanded(
                flex: 5,
                child: Text(useSize.toString(),
                    textDirection: TextDirection.rtl,
                    maxLines: 1,
                    style: TextStyle(fontSize: 20.sp, color: XColors.black)),
              ),
              Expanded(
                flex: 4,
                child: Text(totalSize,
                    textDirection: TextDirection.rtl,
                    maxLines: 1,
                    style: TextStyle(fontSize: 20.sp, color: XColors.black)),
              ),
            ],
          ),
        ),
        const DashedLind(
          axis: Axis.horizontal, // 垂直方向设置
          dashedWidth: 8,
          count: 20,
          color: Color(0xffe5e6eb),
        ),
      ],
    ),
  );
}
