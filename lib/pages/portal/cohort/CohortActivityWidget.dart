import 'package:flutter/material.dart';
import 'package:orginone/components/EmptyWidget/EmptyActivity.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import '../../../components/ActivityWidget/ActivityListWidget/ActivityListWidget.dart';
import '../../../components/CommandWidget/index.dart';
import '../../../components/EntityWidget/EntitySwitchWidget/EntitySwitchWidget.dart';
import '../../../components/EntityWidget/EntitySwitchWidget/model.dart';
import '../../../routers/app_route.dart';

//动态页面
class CohortActivityWidget extends StatefulWidget {
  late String type;
  late String label;
  late GroupActivity cohortActivity;

  CohortActivityWidget(
      this.type,
      this.label,
      this.cohortActivity, {
        super.key,
      }) {
  }

  @override
  State<StatefulWidget> createState() => _CohortActivityPage();
}

class _CohortActivityPage extends State<CohortActivityWidget> {
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return CommandWidget<AppRoute>(
        flag: 'switchSpace',
        builder: (context, args) {
          return EntitySwitchWidget<IActivity>(
              data: ActivityData("动态", widget.cohortActivity),
              buildChildren: (IActivity act) {
                return Future(() async {
                  return null == act || act.activitys.isEmpty
                      ? const Center(
                    child: EmptyActivity(),
                  )
                      : ActivityListWidget(
                    scrollController: scrollController,
                    activity: act,
                  );
                });
              });
        });
  }
}
IActivity? currentActivity;
class ActivityData extends EntitySwitchModel<IActivity> {
  GroupActivity cohortActivityData;
  ActivityData(super.title, this.cohortActivityData);

  @override
  Future<IActivity> getInitData() async {
    currentActivity = cohortActivityData.activitys.isEmpty ? (cohortActivityData as IActivity) : cohortActivityData.activitys?.first;
    return cohortActivityData;
  }

  @override
  Future<List<IActivity>> getInitDatas() async {
    // if (cohortActivityData!.activitys.isEmpty ||
    //     cohortActivityData!.activitys.length <= 1) {
    //   await cohortActivityData!.load();
    // }
    return Future(() => cohortActivityData.activitys);
  }

  @override
  void setCurrData(IActivity? currData) {
    if (null != currData) {
      currentActivity = currData;
    }
  }
}
