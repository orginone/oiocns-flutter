import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/UIConfig.dart';
import 'package:orginone/config/constant.dart';
import 'package:orginone/config/theme/unified_style.dart';

import '../../../components/XImage/ImageWidget.dart';

// ignore: must_be_immutable
class ItemCard extends StatelessWidget {
  ItemCard({Key? key, this.item}) : super(key: key);
  dynamic item;
  @override
  Widget build(BuildContext context) {
    double width = (UIConfig.screenWidth - 36) / 2;
    // double height = width * (4.00 / 3);
    return Container(
      width: width,
      // height: height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      padding: EdgeInsets.zero,
      clipBehavior: Clip.hardEdge,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          // XImage.localImage(XImage.emptyFile,
          //     size: width, fit: BoxFit.contain, radius: 8),
          ImageWidget(
              item['image'] == null
                  ? XImage.emptyFile
                  : '${Constant.host}${item['image'][0]['shareLink']}',
              size: width),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            width: width - 16,
            child: Text('${item['first'] ?? '暂无数据'} ${item['fourth'] ?? ''}',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 22.sp)),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            width: width - 16,
            child: Text("数量：${item['second'] ?? "暂无数据"}",
                maxLines: 1,
                style: TextStyle(
                    color: XColors.primary,
                    fontWeight: FontWeight.w700,
                    fontSize: 22.sp)),
          ),
          _buildBtn(context)
          // Container(
          //     margin: const EdgeInsets.only(bottom: 10),
          //     width: width - 16,
          //     child: Row(
          //       children: [
          //         CommonWidget.getClickButton('加入购物车', width - 16, () {},
          //             height: 36,
          //             borderRadius: 6,
          //             color: Colors.white,
          //             textColor: XColors.doorDesGrey,
          //             borderColor: XColors.dividerLineColor,
          //             fontWeight: FontWeight.w500,
          //             margin: const EdgeInsets.only(top: 5),
          //             textSize: 14)
          //       ],
          //     )),
        ],
      ),
    );
  }

  Widget _buildBtn(BuildContext context) {
    // if (item['']) {}
    return Container();
  }
}
