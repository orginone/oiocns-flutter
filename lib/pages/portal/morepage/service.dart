import 'dart:convert';

import 'package:orginone/components/form/form_widget/form_tool.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/core/thing/standard/page.dart';
import 'package:orginone/main.dart';
import 'package:orginone/dart/core/thing/standard/form.dart' as form1;

class ShareServices {
  Future<ResultType> loadShopData(
      List<String> relations, dynamic options, String belongId,
      {int take = 10}) async {
    return await kernel.loadThing(belongId, belongId, relations, options);
  }

  getRealKey(dynamic pageTempleteElement) {
    dynamic rootEle =
        pageTempleteElement['children'][0]?['slots']['content']?['slots'];
    String first = 'T${rootEle?['first']?['props']?['property']?['id']}';
    String second = 'T${rootEle?['second']?['props']?['property']?['id']}';
    String third = 'T${rootEle?['third']?['props']?['property']?['id']}';
    String fourth = 'T${rootEle?['fourth']?['props']?['property']?['id']}';
    String fifth = 'T${rootEle?['fifth']?['props']?['property']?['id']}';
    String image = 'T${rootEle?['image']?['props']?['property']?['id']}';
    return {
      'first': first,
      'second': second,
      'third': third,
      'fourth': fourth,
      'fifth': fifth,
      'image': image
    };
  }

  ///商城展示数据解析
  getMallData(List mallData, dynamic pageTempleteElement) {
    List list = [];
    dynamic key = getRealKey(pageTempleteElement);
    for (var item in mallData) {
      dynamic mapItem = item;
      mapItem['first'] = item[key['first']];
      mapItem['second'] = item[key['second']];
      mapItem['fourth'] = item[key['fourth']];
      mapItem['first'] = item[key['first']];
      mapItem['image'] =
          item[key['image']] == null ? null : jsonDecode(item[key['image']]);
      list.add(mapItem);
    }
    return list;
  }

  static List assembleData(
      Map<String, dynamic> formCellInfo, List<FieldModel> fiedModels) {
    List datas = [];
    var info = formCellInfo;
    for (var fs in fiedModels) {
      if (info.containsKey('${fs.code}')) {
        var v = info['${fs.code}'];
        //如果value是字典项 或者分类项 需要去lookup取对应值
        FiledLookup? lookup = fs.lookups?.firstWhere(
          (e) => v == e.value,
          orElse: () => FiledLookup(),
        );
        if (lookup != null && lookup.id != null) {
          v = lookup.text;
        }
        fs.value = v.toString();
      }
      datas.add(fs.toJson());
    }
    return datas;
  }

  Future<form1.Form?> getFormBypagetemplete(IPageTemplate iPageTemplate) async {
    List? formList =
        iPageTemplate.metadata.rootElement['children']?[0]?['props']?['forms'];
    if (formList == null || formList.isEmpty) return null;
    String? formId = formList[0]?['id'];
    if (formId != null) {
      List<form1.IForm> formList = await iPageTemplate.loadForms([formId]);
      if (formList.isNotEmpty) {
        form1.Form form = await FormTool.loadForm(formList[0]);
        form.metadata.showAttributes = true;
        return form;
      }
    }
    return null;
  }
}
