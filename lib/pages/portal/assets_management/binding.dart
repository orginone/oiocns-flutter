import 'package:get/get.dart';

import 'logic.dart';

class AssetsManagementBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AssetsManagementLogic());
  }
}
