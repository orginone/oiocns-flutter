import 'package:flutter/material.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/UIConfig.dart';
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';
import 'package:orginone/components/EmptyWidget/EmptyWidget.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/core/thing/standard/application.dart';
import 'package:orginone/dart/core/work/index.dart';
import 'package:orginone/pages/portal/assets_management/assets_module/asset_type.dart';

// ignore: must_be_immutable
class AssetsTypeListPage extends XStatefulWidget<IApplication> {
  AssetsTypeListPage({super.key, super.data});
  // IApplication? app = super.data;

  @override
  State<AssetsTypeListPage> createState() => _AssetsTypeListPageState();
}

class _AssetsTypeListPageState
    extends XStatefulState<AssetsTypeListPage, IApplication> {
  late IApplication app;
  List<IWork> iwork = [];
  @override
  void initState() {
    super.initState();
    setState(() {
      app = widget.data;
    });
    if (app.children.isEmpty) {
      loadWorks();
    }
  }

  loadWorks() async {
    iwork = await app.loadWorks();
    setState(() {});
  }

  // @override
  // Widget build(BuildContext context) {
  //   return GyScaffold(
  //     titleName: app.name,
  //     backgroundColor: Colors.white,
  //     body: Container(
  //         decoration: const BoxDecoration(
  //             border: Border(
  //                 top: BorderSide(color: XColors.dividerLineColor, width: 1))),
  //         width: UIConfig.screenWidth,
  //         height: UIConfig.screenHeight,
  //         child: app.children.isEmpty && iwork.isEmpty
  //             ? EmptyWidget(title: "暂无数据", iconPath: XImage.empty)
  //             : app.children.isEmpty
  //                 ? buildTypeWithWork()
  //                 : SingleChildScrollView(
  //                     child: Column(
  //                     children: buildType(),
  //                   ))),
  //   );
  // }

  buildType() {
    return app.children.map((e) => AssetsTypePage(typeData: e)).toList();
  }

  buildTypeWithWork() {
    return buildItemApp(context, iwork);
  }

  @override
  Widget buildWidget(BuildContext context, IApplication data) {
    // app = data;
    return Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(
                top: BorderSide(color: XColors.dividerLineColor, width: 1))),
        width: UIConfig.screenWidth,
        height: UIConfig.screenHeight,
        child: data.children.isEmpty && iwork.isEmpty
            ? EmptyWidget(title: "暂无数据", iconPath: XImage.empty)
            : data.children.isEmpty
                ? buildTypeWithWork()
                : SingleChildScrollView(
                    child: Column(
                    children: buildType(),
                  )));
  }
}
