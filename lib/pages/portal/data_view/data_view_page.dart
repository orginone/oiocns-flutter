import 'package:flutter/material.dart';
import 'package:orginone/components/EntityWidget/EntitySwitchWidget/model.dart';
import 'package:orginone/components/form/widgets/form_datas_view.dart';
import 'package:orginone/dart/core/thing/standard/form.dart';
import 'package:orginone/dart/core/thing/standard/form.dart' as core;
import 'package:orginone/main.dart';
import '../../../components/PortalManagerWidget/portalrefresh_event.dart';

/// 数据试图页面
class DataViewPage extends StatefulWidget {
  core.Form form;
  DataViewPage({Key? key, required this.form}) : super(key: key);

  @override
  State<DataViewPage> createState() => _DataViewPageState();
}

class _DataViewPageState extends State<DataViewPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widget.form.metadata.showAttributes = true;
    return FormDataView(form: widget.form, xForm: widget.form.metadata, showType: "all", type: 2,);
    // return EntitySwitchWidget<IForm>(
    //     data: FormData("表单"),
    //     buildChildren: (IForm iForm) {
    //       return Future(() async {
    //         core.Form form = await FormTool.(iForm);
    //         form.metadata.showAttributes = true;
    //         return FormDataView(form: form, xForm: form.metadata, showType: "all", type: 1,);
    //       });
    //     });
  }
}

// class FormData extends EntitySwitchModel<IForm> {
//   FormData(super.title);
//
//   @override
//   Future<IForm?> getInitData() async {
//     IForm? iForm = await relationCtrl.findForm();
//     return iForm;
//   }
//
//   @override
//   void setCurrData(IForm? currData) async {
//     if (null != currData) {
//       IForm? iForm = await relationCtrl.findForm();
//       var navigationItems =
//           relationCtrl.user!.cacheObj.getValue("portalTemplate");
//       if (null != navigationItems && navigationItems.isNotEmpty) {
//         navigationItems.forEach((value) {
//           if (value['name'] == iForm?.metadata.name) {
//             value['belongId'] = currData.belongId;
//             value['name'] = currData.metadata.name;
//             value['tags'] = ['数据模板', currData.directory.metadata.name];
//             value['id'] =
//                 '${currData.metadata.belongId}_${currData.metadata.id}';
//             value['metadata'] = currData.metadata;
//             value['key'] =
//                 'home_Module${currData.metadata.belongId}_${currData.metadata.id}';
//           }
//         });
//         relationCtrl.user!.cacheObj.set("portalTemplate", navigationItems);
//         relationCtrl.setForm(currData);
//         portalRefreshEventBus.fire(PortalRefreshEvent(-1));
//       }
//     }
//   }
//
//   @override
//   Future<List<IForm>> getInitDatas() async {
//     // app = await relationCtrl.findApplication();
//     List<IForm> iForms = await relationCtrl.loadForms();
//     return iForms;
//   }
// }
