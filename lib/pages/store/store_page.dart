import 'dart:math';

import 'package:flutter/material.dart';
import 'package:orginone/components/EmptyWidget/EmptyWidget.dart';
import 'package:orginone/components/XImage/components/icon.dart';
import 'package:orginone/components/TabContainerWidget/TabContainerWidget.dart';
import 'package:orginone/components/TabContainerWidget/types.dart';
import 'package:orginone/components/ListWidget/ListWidget.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/public/entity.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/dart/core/target/base/target.dart';
import 'package:orginone/dart/core/target/outTeam/istorage.dart';
import 'package:orginone/dart/core/target/person.dart';
import 'package:orginone/dart/core/target/team/company.dart';
import 'package:orginone/dart/core/thing/directory.dart';
import 'package:orginone/dart/core/thing/fileinfo.dart';
import 'package:orginone/dart/core/thing/standard/index.dart';
import 'package:orginone/dart/core/thing/standard/page.dart';
import 'package:orginone/dart/core/thing/systemfile.dart';
import 'package:orginone/dart/core/work/index.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/app_route.dart';
import 'package:orginone/routers/pages.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../../components/CommandWidget/index.dart';
import '../../components/ListWidget/components/ListItemWidget.dart';
import '../../components/XConsumer/XConsumer.dart';
import '../../components/XLazy/XLazy.dart';
import '../../dart/base/common/commands.dart';
import '../home/components/BadgeTabWidget/BadgeTabWidget.dart';

/// 关系页面
class StorePage extends StatefulWidget {
  // late TabContainerModel? storeModel;
  // late dynamic datas;

  // StorePage({super.key, dynamic datas}) {
  //   storeModel = null;
  //   this.datas = datas ?? RoutePages.getRouteParams(homeEnum: HomeEnum.store);
  // }
  const StorePage({super.key});
  @override
  State<StatefulWidget> createState() => _StorePageState();
}

class _StorePageState extends State<StorePage> {
  // TabContainerModel? get storeModel => widget.storeModel;
  // set storeModel(TabContainerModel? storeModel) {
  //   widget.storeModel = storeModel;
  // }

  // dynamic get datas => widget.datas;
  // set datas(dynamic value) {
  //   widget.datas = value;
  // }
  dynamic parentDataCurr;
  final ScrollController _scrollController = ScrollController();
  late TabContainerModel? storeModel;
  late dynamic datas;
  late var parentDirtoryName;
  _StorePageState() {
    // if (relationCtrl.homeEnum.value == HomeEnum.store &&
    //     RoutePages.getRouteLevel() > 0) {
    // relationCtrl.homeEnum.listen((homeEnum) {
    //   if (homeEnum == HomeEnum.store) {
    //     if (mounted) {
    //       setState(() {
    //         storeModel = null;
    //         datas = RoutePages.getRouteParams(homeEnum: HomeEnum.store);
    //       });
    //     }
    //   }
    // });
    // }
  }
  @override
  void initState() {
    super.initState();
    storeModel = null;
    datas = null;
  }

  @override
  void didUpdateWidget(StorePage oldWidget) {
    super.didUpdateWidget(oldWidget);
    // storeModel = oldWidget.storeModel;
    // datas = oldWidget.datas;
  }

  @override
  Widget build(BuildContext context) {
    // if (null == storeModel) {
    //   // datas = RoutePages.getRouteParams();
    //   load();
    // }

    return XConsumer<AppRoute>(
      builder: (context, ar, child) {
        // if (datas != ar.currPageData.data) {
        datas = ar.currPageData.data;
        // }
        return CommandWidget<TabContainerWidget>(
            type: 'store',
            cmd: 'refresh',
            builder: (context, args) {
              load();
              return TabContainerWidget(storeModel!);
            });
      },
    );
  }

  void load() {
    storeModel =
        TabContainerModel(title: RoutePages.getRouteTitle() ?? "数据", tabItems: [
      createTabItemsModel(title: "全部"),
      if (null == datas) ...[
        createTabItemsModel(title: "个人"),
        createTabItemsModel(title: "单位")
      ],
    ]);
  }

  TabItemsModel createTabItemsModel({
    required String title,
  }) {
    List<IEntity<dynamic>> initDatas = [];
    if (null == datas) {
      initDatas = getFirstLevelDirectories(title);
    } else if (datas is List<IEntity<dynamic>>) {
      initDatas = datas;
    }
    return TabItemsModel(
        title: title,
        content: ListWidget(
          initDatas: initDatas,
          scrollController: _scrollController,
          onInitLoad: () async {
            return initDatas;
          },
          onLoad: () async {
            if (null == parentDataCurr) {
              return loadStorages(
                  RoutePages.routeData.currPageData.entity as ITarget);
            } else if (parentDataCurr is ICompany &&
                parentDataCurr.typeName == TargetType.company.label) {
              return loadStorages(parentDataCurr);
            } else if (parentDataCurr is IPerson &&
                parentDataCurr.typeName == TargetType.person.label) {
              return loadStorages(parentDataCurr);
            } else if (parentDataCurr is IStorage &&
                parentDataCurr.typeName == TargetType.storage.label) {
              return loadStoragesDirectory(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                [
                  SpaceEnum.dataStandards.label,
                  SpaceEnum.businessModeling.label,
                  // SpaceEnum.applications.label,
                  SpaceEnum.view.label,
                ].contains(parentDataCurr.name) &&
                parentDataCurr is Directory) {
              parentDirtoryName = parentDataCurr.name;
              return loadSystemDirectory2(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.applications.label &&
                parentDataCurr is Application) {
              // XDirectory tmpDir;
              // tmpDir = XDirectory(
              //     id: parentData.id,
              //     directoryId: parentData.id,
              //     isDeleted: false);
              // tmpDir.name = parentData.typeName;
              // tmpDir.typeName = parentData.typeName;
              // Directory directory = Directory(
              //   tmpDir,
              //   relationCtrl.user!,
              // );
              // return loadSystemDirectory(parentData);
              return loadApplicationDirectory(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.property.label &&
                parentDataCurr is Directory) {
              return loadProperties(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.species.label &&
                parentDataCurr is Directory) {
              return loadSpecies(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.dict.label &&
                parentDataCurr is Directory) {
              return loadDicts(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.work.label &&
                parentDataCurr is FixedDirectory) {
              return loadWorks(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.file.label &&
                parentDataCurr is FixedDirectory) {
              //系统文件类型
              return loadFixedFiles(parentDataCurr.parent!.superior as Directory);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr is Directory &&
                ![
                  DirectoryType.form.label,
                  DirectoryType.report.label,
                  DirectoryType.app.label,
                  SpaceEnum.table.label,
                  SpaceEnum.bulletinBoard.label,
                  SpaceEnum.module.label,
                  SpaceEnum.role.label,
                  SpaceEnum.transfer.label,
                  SpaceEnum.dataMigration.label,
                  SpaceEnum.shoppingPage.label,
                  SpaceEnum.code.label,
                  DirectoryType.mirror.label,
                  DirectoryType.model.label,
                  DirectoryType.form.label,
                  DirectoryType.pageTemplate.label,
                ].contains(parentDataCurr.name)) {
              //    dict species property   work app  dataStandard storage
              parentDirtoryName = parentDataCurr.name;
              //系统文件类型--子目录
              return loadFixedFiles2(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.app.label &&
                parentDataCurr is FixedDirectory) {
              return loadApps(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == SpaceEnum.module.label &&
                parentDataCurr is FixedDirectory) {
              return loadModules(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.model.label &&
                parentDataCurr is Directory) {
              // return loadApps(parentData);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.form.label &&
                parentDataCurr is FixedDirectory) {
              return loadForms(parentDataCurr);
            } else if (parentDataCurr.typeName == SpaceEnum.directory.label &&
                parentDataCurr.name == DirectoryType.pageTemplate.label &&
                parentDataCurr is Directory) {
              return loadPageTemplates(parentDataCurr);
            }
            return [];
            // return initDatas;
          },
          getDatas: ([dynamic parentData]) async {
            parentDataCurr = parentData;
            if (null == parentData) {
              return loadStorages(
                  RoutePages.routeData.currPageData.entity as ITarget);
            } else if (parentData is ICompany &&
                parentData.typeName == TargetType.company.label) {
              return loadStorages(parentData);
            } else if (parentData is IPerson &&
                parentData.typeName == TargetType.person.label) {
              return loadStorages(parentData);
            } else if (parentData is IStorage &&
                parentData.typeName == TargetType.storage.label) {
              return loadStoragesDirectory(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                [
                  SpaceEnum.dataStandards.label,
                  SpaceEnum.businessModeling.label,
                  // SpaceEnum.applications.label,
                  SpaceEnum.view.label,
                ].contains(parentData.name) &&
                parentData is Directory) {
              parentDirtoryName = parentData.name;
              return loadSystemDirectory(parentData);
            } else if (parentData.typeName == SpaceEnum.applications.label &&
                parentData is Application) {
              // XDirectory tmpDir;
              // tmpDir = XDirectory(
              //     id: parentData.id,
              //     directoryId: parentData.id,
              //     isDeleted: false);
              // tmpDir.name = parentData.typeName;
              // tmpDir.typeName = parentData.typeName;
              // Directory directory = Directory(
              //   tmpDir,
              //   relationCtrl.user!,
              // );
              // return loadSystemDirectory(parentData);
              return loadApplicationDirectory(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.property.label &&
                parentData is Directory) {
              parentData.standard.propertys = [];
              return loadProperties(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.species.label &&
                parentData is Directory) {
              parentData.standard.specieses = [];
              return loadSpecies(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.dict.label &&
                parentData is Directory) {
              parentData.standard.dicts = [];
              return loadDicts(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.work.label &&
                parentData is FixedDirectory) {
              return loadWorks(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.file.label &&
                parentData is FixedDirectory) {
              //系统文件类型

              return loadFixedFiles(parentData.parent!.superior as Directory);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData is Directory &&
                ![
                  DirectoryType.form.label,
                  DirectoryType.report.label,
                  DirectoryType.app.label,
                  SpaceEnum.table.label,
                  SpaceEnum.bulletinBoard.label,
                  SpaceEnum.module.label,
                  SpaceEnum.role.label,
                  SpaceEnum.transfer.label,
                  SpaceEnum.dataMigration.label,
                  SpaceEnum.shoppingPage.label,
                  SpaceEnum.code.label,
                  DirectoryType.mirror.label,
                  DirectoryType.model.label,
                  DirectoryType.form.label,
                  DirectoryType.pageTemplate.label,
                ].contains(parentData.name)) {
              //    dict species property   work app  dataStandard storage
              parentDirtoryName = parentData.name;
              //系统文件类型--子目录
              return loadFixedFiles(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.app.label &&
                parentData is FixedDirectory) {
              return loadApps(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == SpaceEnum.module.label &&
                parentData is FixedDirectory) {
              return loadModules(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.model.label &&
                parentData is Directory) {
              // return loadApps(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.form.label &&
                parentData is FixedDirectory) {
              return loadForms(parentData);
            } else if (parentData.typeName == SpaceEnum.directory.label &&
                parentData.name == DirectoryType.pageTemplate.label &&
                parentData is Directory) {
              return loadPageTemplates(parentData);
            }

            return [];
          },
          getAction: (dynamic data) {
            if (data is! IDirectory) {
              return GestureDetector(
                onTap: () {
                  XLogUtil.d('>>>>>>======点击了感叹号');
                  RoutePages.jumpStoreInfoPage(context, data: data);
                },
                child: const IconWidget(
                  color: XColors.chatHintColors,
                  iconData: Icons.info_outlined,
                ),
              );
            }
            return null;
          },
          onTap: (dynamic data, List children) {
            XLogUtil.d('>>>>>>======点击了列表项 ${data.name} ${children.length}');
            RoutePages.jumpStore(context,
                parentData: data, listDatas: children);
          },
        )
    );
  }

  buildActivityItem(IEntity<dynamic> item) {
    return InkWell(
      highlightColor: Colors.transparent,
      radius: 0,
      onTap: () {

      },
      child: const IconWidget(
        color: XColors.chatHintColors,
        iconData: Icons.info_outlined,
      ),
    );
  }

  // 获得一级目录
  List<IEntity<dynamic>> getFirstLevelDirectories(String title) {
    List<IEntity<dynamic>> datas = [];
    if (null != relationCtrl.user) {
      if (title == "个人" || title == "全部") {
        datas.add(relationCtrl.user!);
      }
      if (title == "单位" || title == "全部") {
        datas.addAll(
            relationCtrl.user?.companys.map((item) => item).toList() ?? []);
      }
    }
    return datas;
  }

  // 加载存储列表
  List<IStorage> loadStorages(ITarget? target) {
    List<IStorage> storages = [];
    if (null != target) {
      TargetType? type = TargetType.getType(target.typeName);
      if (type == TargetType.person) {
        storages = relationCtrl.user?.storages ?? [];
      } else if (type == TargetType.company) {
        storages = relationCtrl.user?.findCompany(target.id)?.storages ?? [];
      }
    }

    return storages;
  }

  /// 加载存储列表
  List<Directory> loadStoragesDirectory(IStorage data) {
    if (data.isActivate == false) {
      return [];
    }
    List<Directory> datas = [];
    XDirectory tmpDir;
    int id = 0;
    if (null != relationCtrl.user) {
      DirectoryGroupType.getType(data.typeName)?.types.forEach((e) {
        tmpDir = XDirectory(
            id: "${data.id}_$id",
            directoryId: "${data.id}_$id",
            isDeleted: false);
        tmpDir.name = e.label;
        tmpDir.typeName = SpaceEnum.directory.label;
        datas.add(FixedDirectory(
          tmpDir,
          data.space ?? relationCtrl.user!,
          standard: getCurrentCompany()?.standard,
          parent: data.directory,
        ));
        id++;
      });
    }
    return datas;
  }

  /// 加载系统目录
  List<Directory> loadSystemDirectory <T extends StandardFileInfo>(T item) {
    List<Directory> datas = [];
    XDirectory tmpDir;
    int id = 0;
    String type = item is Application ? item.typeName : item.name;
    if (null != relationCtrl.user) {
      DirectoryGroupType.getType(type)?.types.forEach((e) {
        // LogUtil.i("loadSystemDirectory=id:${item.id}");
        tmpDir = XDirectory(
            id: "${item.id}_$id",
            directoryId: "${item.id}_$id",
            isDeleted: false);
        tmpDir.name = e.label;
        tmpDir.typeName = SpaceEnum.directory.label;
        datas.add(FixedDirectory(tmpDir, item.target ?? relationCtrl.user!,
            standard: getCurrentCompany()?.standard));
        id++;
      });
    }
    return datas;
  }

  /// 加载系统目录
  List<Directory> loadSystemDirectory2(Directory item) {
    List<Directory> datas = [];
    XDirectory tmpDir;
    int id = 0;
    String type = item is Application ? item.typeName : item.name;
    if (null != relationCtrl.user) {
      DirectoryGroupType.getType(type)?.types.forEach((e) {
        // LogUtil.i("loadSystemDirectory=id:${item.id}");
        tmpDir = XDirectory(
            id: "${item.id}_$id",
            directoryId: "${item.id}_$id",
            isDeleted: false);
        tmpDir.name = e.label;
        tmpDir.typeName = SpaceEnum.directory.label;
        datas.add(FixedDirectory(tmpDir, item.target ?? relationCtrl.user!,
            standard: getCurrentCompany()?.standard));
        id++;
      });
    }
    return datas;
  }


  /// 获得当前单位
  ICompany? getCurrentCompany({String? companyId}) {
    return null != relationCtrl.user
        ? relationCtrl.user!.findCompany(
            companyId ?? RoutePages.routeData.currPageData.entity?.id ?? "")
        : null;
  }

  /// 加载属性
  Future<List<IProperty>> loadProperties(Directory item) async {
    // List<IProperty> files = item.standard.propertys;
    // XLogUtil.d('>>>>>>======loadProperties ${files.length}');
    // if (files.isEmpty) {
    List<IProperty> files = await item.standard.loadPropertys(reload: true, isAll: "标准" == parentDirtoryName ? true : false);
    // }
    command.emitter('store', 'refresh');
    return files;
  }

  ///加载分类
  Future<List<ISpecies>> loadSpecies(Directory item) async {
    // List<ISpecies> files = item.standard.specieses;
    // XLogUtil.d('>>>>>>======loadSpecies ${files.length}');
    // if (files.isEmpty) {
    List<ISpecies> files = await item.standard.loadSpecieses(isAll: "标准" == parentDirtoryName ? true : false);
    // }
    command.emitter('store', 'refresh');
    return files;
  }

  ///加载字典
  Future<List<ISpecies>> loadDicts(Directory item) async {
    // List<ISpecies> files = item.standard.dicts;
    // XLogUtil.d('>>>>>>======loadDicts ${files.length}');
    // if (files.isEmpty) {
    List<ISpecies> files = await item.standard.loadDicts(isAll: "标准" == parentDirtoryName ? true : false);
    // }
    command.emitter('store', 'refresh');
    return files;
  }

  ///加载应用
  Future<List<IApplication>> loadApps(Directory item) async {
    List<IApplication> files = [];
    files = await item.standard.loadStoreApplications();
    //过滤已删除目录
    files = files.where((e) => !e.groupTags.contains("已删除")).toList();
    XLogUtil.d(files.map((e) => e.id));
    return files;
  }

  ///加载模块
  Future<List<IApplication>> loadModules(FixedDirectory item) async {
    List<IApplication> files = [];
    files = await item.standard
        .loadModules(applicationId: item.belongApplication?.id ?? '');
    //过滤已删除目录
    files = files.where((e) => !e.groupTags.contains("已删除")).toList();
    XLogUtil.d(files.map((e) => e.id));
    return files;
  }

  ///加载表单
  Future<List<IForm>> loadForms(FixedDirectory item) async {
    // List<IForm> files = [];
    // LogUtil.e('>>>>>>======loadForms ${item.id}');
    // LogUtil.e('>>>>>>======loadForms ${item.belongApplication?.id}');

    // files = await item.standard
    //     .loadForms(reload: true, applicationId: item.belongApplication?.id); //

    // files = files.where((e) => !e.groupTags.contains("已删除")).toList();

    // LogUtil.e('>>>>>>======loadForms 加载完成');
    // return files;
    List<IForm> files = [];

    files = await item.belongApplication!.loadAllForms(); //

    files = files.where((e) => !e.groupTags.contains("已删除")).toList();

    XLogUtil.dd('>>>>>>======loadForms 加载完成');
    return files;
  }

  ///加载页面模版
  Future<List<IPageTemplate>> loadPageTemplates(Directory item) async {
    List<IPageTemplate> files = item.standard.templates;
    XLogUtil.d('>>>>>>======loadPageTemplates ${files.length}');
    if (files.isEmpty) {
      return await item.standard.loadTemplates();
    }

    return files;
  }

  ///加载办事
  Future<List<IWork>> loadWorks(FixedDirectory item) async {
    List<IWork> works = await item.belongApplication!.loadWorks();

    //过滤已删除目录
    // works = works.where((e) => !e.groupTags.contains("已删除")).toList();
    return works;
  }

  //加载应用子目录
  Future<List<IEntity>> loadFixedFiles2/*<T extends IDirectory>(T target)*/(Directory target) async {
    List<IDirectory> directorys = target.children;
    if (directorys.isEmpty) {
      await target.loadContent();
      directorys = target.children;
    }
    //过滤已删除目录
    directorys = directorys.where((e) => !e.groupTags.contains("已删除")).toList();
    //加载文件
    List<ISysFileInfo> files = target.files;
    if (files.isEmpty) {
      // await target.loadContent();
      await target.loadFiles();
      files = target.files;
    }
    //过滤非文件类型数据
    List<IEntity> entitys = [];
    entitys.addAll(directorys);
    entitys.addAll(files);
    // directorys = _filterDeleteed(directorys);

    return entitys;
  }

  //加载应用子目录
  Future<List<IEntity>> loadFixedFiles <T extends IDirectory>(T target) async {
    List<IDirectory> directorys = target.children;
    if (directorys.isEmpty) {
      await target.loadContent();
      directorys = target.children;
    }
    //过滤已删除目录
    directorys = directorys.where((e) => !e.groupTags.contains("已删除")).toList();
    //加载文件
    List<ISysFileInfo> files = target.files;
    if (files.isEmpty) {
      // await target.loadContent();
      await target.loadFiles();
      files = target.files;
    }
    //过滤非文件类型数据
    List<IEntity> entitys = [];
    entitys.addAll(directorys);
    entitys.addAll(files);
    // directorys = _filterDeleteed(directorys);

    return entitys;
  }

  ///加载文件
  Future<List<ISysFileInfo>> loadFiles<T extends IDirectory>(T target) async {
    List<ISysFileInfo> files = target.files;
    XLogUtil.d('>>>>>>======files ${files.length}');
    if (files.isEmpty) {
      // await target.loadContent();
      await target.loadFiles();
      return target.files;
    }
    return files;
  }

  ///@ 加载文件
  Future<List<IStandardFileInfo<XStandard>>> loadFiles2(Directory item) async {
    List<IStandardFileInfo<XStandard>> files = item.standard.standardFiles;
    XLogUtil.dd('>>>>>>======loadFiles ${files.length}');
    if (files.isEmpty) {
      return await item.standard.loadStandardFiles();
    }

    return files;
  }

  /// 加载应用目录
  List<Directory> loadApplicationDirectory(Application item) {
    XLogUtil.i("loadApplicationDirectory applicationId:${item.id}");
    List<Directory> datas = [];
    XDirectory tmpDir;
    int id = 0;
    if (null != relationCtrl.user) {
      DirectoryGroupType.getType(item.typeName)?.types.forEach((e) {
        tmpDir = XDirectory(
            id: "${item.id}_$id",
            directoryId: "${item.id}_$id",
            isDeleted: false);
        tmpDir.name = e.label;
        tmpDir.typeName = SpaceEnum.directory.label;
        datas.add(FixedDirectory(tmpDir, relationCtrl.user!,
            standard: getCurrentCompany()?.standard, belongApplication: item));
        id++;
      });
    }
    return datas;
  }
}
