import 'package:flutter/material.dart';

class AppThemeData {
  late ThemeData defaultThemeData;

  AppThemeData() {
    AppBarTheme appbarTheme = const AppBarTheme(
        color: Colors.white,
        // backgroundColor: Colors.black,
        // shadowColor: Color(0xffffffff),
        // surfaceTintColor: Color(0xffff00ff),
        foregroundColor: Colors.black,
        iconTheme: IconThemeData(color: Colors.black),
        titleTextStyle: TextStyle(color: Color(0x00000000)),
        toolbarTextStyle: TextStyle(color: Color(0x00000000)));

    defaultThemeData = ThemeData(
        useMaterial3: false,
        //主题色
        primaryColor: const Color(0xFF366EF4),
        unselectedWidgetColor: const Color(0xFF6F7686),
        appBarTheme: appbarTheme,

        //主标题
        textTheme: const TextTheme(
          titleLarge: TextStyle(fontSize: 19, color: Colors.black),
        ),
        listTileTheme: const ListTileThemeData(
            titleAlignment: ListTileTitleAlignment.center));
  }
}
