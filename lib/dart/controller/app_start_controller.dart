import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:orginone/components/Tip/ToastUtils.dart';
import 'package:orginone/dart/base/common/commands.dart';
import 'package:orginone/dart/base/common/emitter.dart';
import 'package:orginone/dart/core/provider/auth.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/main.dart';
import 'package:orginone/utils/log/log_util.dart';

///app启动控制器
class AppStartController with EmitterMixin, AuthMixin {
  ///app网络状态（手机网络，内核长链接状态）
  late ConnectivityResult _result;

  ///手机生命周期状态
  late String _appLifecycleState;

  ///app启动状态
  late ExecuteStatus appStartStatus;

  ///调用进行中计数
  int _warpUnderwayCount = 0;

  ///断网络计数
  int _noneConnectivityCount = 0;

  AppStartController() {
    _result = ConnectivityResult.none;
    _appLifecycleState = "";
    _stop();
    subscribe((key, args) async {
      XLogUtil.dd("开始刷新数据");
      await checkNetWorkConnected();
      await _networkFluctuations();
    }, false);
    kernel.onConnectedChanged((isConnected) async {
      XLogUtil.dd("长链接状态：$isConnected");
      if (isConnected) {
        _noneConnectivityCount += 1;
        changeCallback();
      } else {
        if (isActive && isNetworkConnected && !kernel.isOnline) {
          kernel.start();
        }
        // RoutePages.jumpTo(initialRoute);
      }
    });
  }

  bool get isStart {
    return appStartStatus == ExecuteStatus.success ||
        appStartStatus == ExecuteStatus.failed;
  }

  ///检查网络连接并更新最新状态
  Future<void> checkNetWorkConnected() async {
    var res = await Connectivity().checkConnectivity();
    if (_result != res) {
      connectivityResult = res;
    }
  }

  // 判断网络连接
  bool get isNetworkConnected {
    return _result == ConnectivityResult.wifi ||
        _result == ConnectivityResult.mobile;
  }

  bool get hasDataDelay {
    return _noneConnectivityCount > 0;
  }

  /// 设置手机网络状态
  set connectivityResult(ConnectivityResult result) {
    // ToastUtils.showMsg(
    //     msg: "手机网络状态：${_result.name} ${result.name} $_noneConnectivityCount");
    _result = result;
    if (_result == ConnectivityResult.none) {
      _noneConnectivityCount += 1;
      // RoutePages.jumpTo(initialRoute);
    } else {
      kernel.checkOnline();
    }
    command.emitter('network', 'refresh');
    // changeCallback();
  }

  // Timer? _time;

  /// 设置手机生命周期状态
  /// resumed APP的状态从paused切换到resumed进入前台状态
  /// paused 应用挂起，比如退到后台
  /// inactive 界面退到后台或弹出对话框情况下
  /// suspending iOS中没有该状态，安卓里就是挂起
  set appLifecycleState(String state) {
    ToastUtils.showMsgDev(msg: state);
    _appLifecycleState = state;
    if (state == 'AppLifecycleState.resumed') {
      kernel.checkOnline();
      // _wakeUp();
      // changeCallback();
    } else if (state == "AppLifecycleState.paused") {
      ToastUtils.showMsgDev(msg: "App退到后台");
    }
  }

  bool get isActive {
    return _appLifecycleState == 'AppLifecycleState.resumed';
  }

  ///网络波动处理
  Future<void> _networkFluctuations() async {
    XLogUtil.dd(
        "hasDataDelay:$hasDataDelay isNetworkConnected:$isNetworkConnected appStartStatus:$appStartStatus");
    if ((hasDataDelay && isNetworkConnected) ||
        appStartStatus == ExecuteStatus.failed) {
      // relationCtrl.wakeUp();

      // ToastUtils.showMsg(msg: "手机网络状态：${_result.name}刷新数据");
      await refreshData();
    }
  }

  Future<void> refreshData() async {
    XLogUtil.dd(
        "isNetworkConnected:$isNetworkConnected loginStatus:$loginStatus appStartStatus:$appStartStatus");
    if (!isNetworkConnected ||
        appStartStatus == ExecuteStatus.starting ||
        !loginStatus) {
      ///TODO带优化跳转到网络无法使用的页面
      // ToastUtils.showMsg(
      //     msg:
      //         "唤醒：直接退出 $isNetworkConnected ${appStartStatus == ExecuteStatus.starting}");
      return;
    }

    bool res = await kernel.checkOnline();
    if (!res) return await refreshData();

    ///TODO判断token是否过期
    if (kernel.user == null) {
      XLogUtil.dd("唤醒：user销毁重连");
      await onInit();
    } else if (tokenExpired()) {
      ToastUtils.showMsg(msg: "唤醒：token过期刷新");
      XLogUtil.dd("token过期刷新");
      await _refreshToken();
    } else if (_noneConnectivityCount > 0) {
      XLogUtil.dd("唤醒：网络断开$_noneConnectivityCount重连");
      // ToastUtils.showMsg(msg: "唤醒：网络断开$_noneConnectivityCount重连");
      // await _refreshData();
      await _wakeUp();
      command.emitterFlag('session');
    } else {
      XLogUtil.dd("唤醒：刷新数据");
      await _wakeUp();
      command.emitterFlag('session');
    }
  }

  Future<void> _wakeUp() async {
    if (!_start()) {
      return;
    }
    try {
      await relationCtrl.provider.wakeUp();
    } finally {
      _end();
    }
  }

  Future<void> _refreshToken() async {
    if (!_start()) {
      return;
    }
    try {
      await relationCtrl.refreshToken();
    } finally {
      _end();
    }
  }

  ///开始启动
  Future<void> onStarting() async {
    if (!loginStatus && tokenExpired()) {
      _start();
      return;
    }
  }

  Future<void> onStarted(bool success) async {
    _end(success: success);
  }

  Future<void> onInit() async {
    bool res = true;
    if (!loginStatus || !_start()) {
      return;
    }

    try {
      // if (tokenExpired()) {
      res = await relationCtrl.autoLogin();
      // } else {
      //   await _refreshData();
      // }
    } finally {
      _end(success: res);
    }
  }

  bool _start() {
    if (_warpUnderwayCount > 0) {
      return false;
    }
    appStartStatus = ExecuteStatus.starting;
    _warpUnderwayCount += 1;
    XLogUtil.d("APP启动====================开始");
    return true;
  }

  void _end({bool success = true}) {
    if (success) {
      _noneConnectivityCount = 0;
      appStartStatus = ExecuteStatus.success;
      _warpUnderwayCount -= 1;
    } else {
      appStartStatus = ExecuteStatus.failed;
      // ToastUtils.showMsg(msg: "登陆异常重试");
      changeCallback();
    }
    XLogUtil.d("APP启动####################$success结束");
    XLogUtil.d("APP启动 ${kernel.statisticsInfo}");
  }

  void _stop() {
    appStartStatus = ExecuteStatus.notStart;
  }
}
