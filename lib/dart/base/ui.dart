import 'model.dart';

mixin IEntityUI {
  //id
  String get id;
  //名称
  String get name;
  //描述
  String get desc {
    return '';
  }

  //标签
  List<String> get groupTags {
    return [];
  }

  //共享信息
  ShareIcon get shareIcon;
  //搜索方法
  bool search(String key) {
    return name.contains(key) ||
        desc.contains(key) ||
        (groupTags.isNotEmpty && groupTags.contains(key));
  }
}
