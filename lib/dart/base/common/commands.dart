import 'package:orginone/utils/log/log_util.dart';
import 'package:uuid/uuid.dart';

typedef CmdType = void Function(String type, String cmd, dynamic args);

/// 日志
class Command {
  late Map<String, CmdType> callbacks;
  late Map<String, Function> flagCallbacks;

  Command() {
    callbacks = {};
    flagCallbacks = {};
  }

  String get id {
    return const Uuid().v1();
  }

  /// 订阅变更
  /// @param callback 变更回调
  /// @returns 订阅ID
  String subscribe(CmdType? callback) {
    var key = id;
    if (callback != null) {
      callbacks[key] = callback;
    }
    return key;
  }

  /// 取消订阅，支持取消多个
  /// @param id 订阅ID
  void unsubscribe(dynamic id) {
    if (id is String) {
      callbacks.remove(id);
      XLogUtil.dd(">>>>>>command 注销 $id ${callbacks.length}");
    } else if (id is List<String>) {
      for (var _id in id) {
        callbacks.remove(_id);
        XLogUtil.dd(">>>>>>command 注销 $_id ${callbacks.length}");
      }
    }
  }

  /// 发送命令
  /// @param type 类型，目前支持 config、data
  /// @param cmd 命令
  /// @param args 参数
  void emitter(String type, String cmd, [dynamic args]) {
    XLogUtil.dd(">>>>>>command 触发 $type $cmd ${callbacks.length}");
    for (var key in callbacks.keys) {
      Function.apply(callbacks[key] as Function, [type, cmd, args]);
    }
  }

  /// 根据标识订阅变更
  String subscribeByFlag(String flag, Function([dynamic args]) callback,
      [bool target = true]) {
    flagCallbacks['$id-$flag'] = callback;
    if (target) {
      callback.call();
    }
    return id;
  }

  /// 取消标识订阅
  void unsubscribeByFlag(String id) {
    final ids = flagCallbacks.keys.where((i) => i.startsWith('$id-')).toList();
    for (var id in ids) {
      flagCallbacks.remove(id);
    }
  }

  /// 发送命令
  void emitterFlag([String? flag = '', dynamic args]) {
    XLogUtil.dd(">>>>>>command flat 触发 $flag ${flagCallbacks.length}");
    for (var id in flagCallbacks.keys) {
      XLogUtil.dd(">>>>>>command flat 触发 $id");
      if (flag == '' || id.endsWith('-$flag')) {
        flagCallbacks[id]?.call(args);
      }
    }
  }
}

final Command command = Command();
