import 'dart:async';

/// 互斥锁
mixin Mutex {
  Completer<void> _lock = Completer<void>();

  /// 获取锁
  Future<void> acquire() async {
    await _lock.future;
  }

  /// 释放锁
  void release() {
    _lock.complete(null);
    _lock = Completer<void>();
  }
}
