import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/target/base/belong.dart';
import 'package:orginone/dart/core/target/base/target.dart';
import 'package:orginone/dart/core/target/person.dart';
import 'package:orginone/dart/core/target/team/company.dart';

/// 群组
abstract class ICohort extends ITarget {
  // // 按需加载信息
  // Future<void> deepLoadLazy({bool? reload = false});
}

/// 群组
class Cohort extends Target implements ICohort {
  Cohort(this.metadata, this.space, this.relationId)
      : super(
          [space!.key],
          metadata,
          [relationId],
          space: space,
          user: space.user,
        );

  @override
  XTarget metadata;
  @override
  late IBelong? space;
  @override
  String relationId;

  @override
  Future<bool> exit() async {
    if (metadata.belongId != space?.metadata.id) {
      if (await removeMembers(
          space?.user?.metadata != null ? [space!.user!.metadata] : [])) {
        space?.cohorts.removeWhere((i) => i == this);
        return true;
      }
    }
    return false;
  }

  @override
  Future<bool> delete({bool? notity}) async {
    final success = await super.delete(notity: notity);
    if (success) {
      space?.cohorts.removeWhere((i) => i == this);
    }
    return success;
  }

  @override
  List<ITarget> get subTarget => [];
  @override
  List<ISession> get chats => targets.map((i) => i.session).toList();
  @override
  List<ITarget> get targets => [this];

  @override
  Future<void> deepLoad({bool? reload = false}) async {
    // XLogUtil.dd('>>>>>> members $name cohort $hashCode');
    await Future.wait([
      loadIdentitys(reload: reload!),
      loadMembers(reload: reload),
      // directory.loadDirectoryResource(reload: reload),
    ]);
  }

  // // 按需加载信息
  // @override
  // Future<void> deepLoadLazy({bool? reload = false}) async {
  //   await directory.loadDirectoryResource(reload: reload);
  // }

  @override
  List<String> get groupTags {
    var res = super.groupTags;
    if (space is IPerson) {
      res = ["群组"];
    } else if (space is ICompany) {
      res = ["群组"];
    } else {
      res = ["集群"];
    }
    return res;
  }

  @override
  dynamic noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}
