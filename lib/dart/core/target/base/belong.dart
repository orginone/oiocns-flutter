import 'package:get/get.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/public/consts.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/dart/core/public/operates.dart';
import 'package:orginone/dart/core/target/authority/authority.dart';
import 'package:orginone/dart/core/target/base/resource.dart';
import 'package:orginone/dart/core/target/base/target.dart';
import 'package:orginone/dart/core/target/base/team.dart';
import 'package:orginone/dart/core/target/outTeam/cohort.dart';
import 'package:orginone/dart/core/target/outTeam/istorage.dart';
import 'package:orginone/main.dart';

import '../../../base/common/commands.dart';
import '../../chat/activity.dart';
import '../../public/objects.dart';

abstract class IBelong extends ITarget {
  //超管权限，权限为树结构
  late IAuthority? superAuth;
  //加入/管理的群
  List<ICohort> cohorts = [];
  // 用户相关的所有动态会话
  List<IActivity> activitys = [];
  /// 激活的数据核
  IStorage? get activated;
  //存储资源群
  List<IStorage> storages = [];
  //上级用户
  List<ITarget> parentTarget = [];
  //群会话
  List<ISession> cohortChats = [];
  List<String> relations = [];

  ///共享组织
  List<ITarget> shareTarget = [];

  /// 用户缓存对象
  late XObject<Xbase> cacheObj;

  ///常用文件
  List<XCommon> commons = [];

  /// 获取存储占用情况
  Future<DiskInfoType?> getDiskInfo();
  //加载超管权限
  Future<IAuthority?> loadSuperAuth({bool reload = false});
  //申请加用户
  Future<bool> applyJoin(List<XTarget> members);
  //设立人员群
  Future<ICohort?> createCohort(TargetModel data);
  //发送职权变更消息
  Future<bool> sendAuthorityChangeMsg(String operate, XAuthority authority);
  //更新常用
  Future<bool> updateCommon(XCommon data, bool set);
}

//自归属用户基类实现
abstract class Belong extends Target implements IBelong {
  ///构造函数
  Belong(
    metadata,
    List<String> relations, {
    user,
    memberTypes = mTypes,
  }) : super([], metadata, relations,
            space: null, user: user, memberTypes: memberTypes) {
    memberTypes = [TargetType.person];
    cacheObj = XObject(metadata, 'target-cache', [], [key]);
    // kernel.subscribe(
    //     '${metadata.belongId}-${metadata.id}-authority',
    //     [key],
    //     (data) => superAuth
    //         ?.receiveAuthority(AuthorityOperateModel.fromJson(data))); ///////
  }

  @override
  List<ICohort> cohorts = [];

  @override
  List<IStorage> storages = [];

  @override
  IAuthority? superAuth;

  @override
  List<ITarget> get shareTarget;

  @override
  List<ISession> get cohortChats; /////////
  @override
  List<ITarget> get parentTarget;

  /// 用户缓存对象
  @override
  late XObject<Xbase> cacheObj;

  ///常用文件
  @override
  List<XCommon> commons = [];

  @override
  IStorage? get activated {
    return storages.firstWhereOrNull((i) => i.isActivate);
  }

  @override
  List<IActivity> get activitys;

  // @override
  // IFileInfo<XEntity> get superior {

  //     return 'disk' as IFileInfo<XEntity>;

  // }

  @override
  Future<IAuthority?> loadSuperAuth({bool reload = false}) async {
    if (superAuth != null || reload) {
      var res = await kernel.queryAuthorityTree(IdPageModel(
        id: metadata.id,
        page: pageAll,
      ));
      if (res.success) {
        superAuth = Authority(res.data as XAuthority, this);
      }
    }
    return superAuth;
  }

  @override
  Future<ICohort?> createCohort(TargetModel data) async {
    data.typeName = TargetType.cohort.label;
    var metadata = await create(data);
    if (metadata != null) {
      metadata.belong = this.metadata;
      var cohort = Cohort(metadata, this, metadata.belongId ?? '');
      if (this.metadata.typeName != TargetType.person.label) {
        if (!(await pullSubTarget(cohort))) {
          return null;
        }
      }
      cohorts.add(cohort);
      await cohort.pullMembers([user!.metadata]);
      return cohort;
    }
    return null;
  }

  @override
  Future<DiskInfoType?> getDiskInfo() async {
    final res = await kernel.diskInfo(id, id, relations);
    if (res.success && null != res.data) {
      return res.data;
    }
    return null;
  }

  @override
  Future<List<XTarget>> loadMembers(
      {bool? reload = false, String? filter}) async {
    if (isMyTeam && (!TargetResource.loaded(id) || reload!)) {
      TargetResource.setLoaded(id);
      var res = await getPartMembers(0);
      res.offset = 0;
      TargetResource.pullMembers(id, res.result, res.total);
      while (res.offset + res.limit < res.total) {
        res.offset += res.limit;
        var part = await getPartMembers(res.offset);
        TargetResource.pullMembers(id, part.result, res.total);
      }
      for (var i in members) {
        updateMetadata(i);
      }
      loadMemberChats(members, true);
    }

    return members;
  }

  @override
  void loadMemberChats(List<XTarget> newMembers, bool isAdd,
      {bool isAddNew = false}) {
    super.loadMemberChats(members, isAdd);
    newMembers = newMembers.where((i) => i.id != userId).toList();
    if (isAdd) {
      var labels = id == user?.id ? ['好友'] : [name, '同事'];
      for (var i in newMembers) {
        if (!memberChats.any((a) => a.id == i.id)) {
          Session session = Session(i.id, this, i, tags: labels);
          // session.activity.load();
          if (isAddNew) {
            session.chatdata.recently = true;
          }
          memberChats.add(session);
          // LogUtil.e("loadMemberChats");

          // LogUtil.d('loadMemberChats  ------ ${DateUtil.getNowDateMs()}');
          // LogUtil.e('${memberChats.map((e) => e.name).toList()}');
        }
      }
    } else {
      memberChats = memberChats
          .where(
            (i) => newMembers.every((a) => a.id != i.sessionId),
          )
          .toList();
    }
  }

  @override
  Future<bool> loadContent({bool reload = false}) async {
    await super.loadContent(reload: reload);
    await loadSuperAuth(reload: reload);
    return true;
  }

  @override
  List<OperateModel> operates({int? mode}) {
    var operates = super.operates(); ///////
    if (hasRelationAuth()) {
      operates.insert(
        0,
        OperateModel.fromJson(TargetOperates.newCohort.toJson()),
      );
    }
    return operates;
  }

  @override
  Future<bool> applyJoin(List<XTarget> members);

  @override
  Future<bool> sendAuthorityChangeMsg(
      String operate, XAuthority authority) async {
    var res = await kernel.dataNotify(DataNotityType(
      data: {
        operate,
        authority,
        user?.metadata,
      },
      flag: 'authority',
      onlineOnly: true,
      belongId: metadata.belongId!,
      relations: relations,
      onlyTarget: true,
      ignoreSelf: true,
      targetId: metadata.id,
    ));
    return res.success;
  }

  @override
  Future<bool> updateCommon(XCommon data, bool set) async {
    if (set) {
      commons.insert(0, data);
    } else {
      commons = commons
          .where(
            (i) => !(i.id == data.id && i.spaceId == data.spaceId),
          )
          .toList();
    }
    if (await cacheObj.set('commons', commons)) {
      await cacheObj.notity('commons', commons, onlyTarget: id == userId);
      return true;
    }
    return false;
  }

  Future<void> loadCommons() async {
    var data = await cacheObj.get<List<XCommon>>('commons');
    if (null != data && data.isNotEmpty) {
      commons = data;
    }
    cacheObj.subscribe('commons', (dynamic res) {
      if (null != res && res is List<XCommon>) {
        commons = res;
        if (id == userId) {
          command.emitterFlag('commons', [true]);
        } else {
          command.emitterFlag('${id}commons', [true]);
        }
      }
    });
  }
}
