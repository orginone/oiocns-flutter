import 'package:orginone/dart/base/schema.dart';

class TargetResource {
  static List<String> loadedTargetIds = [];
  static Map<String, List<XTarget>> membersMap = {};
  static Map<String, int> memberCountMap = {};

  static bool loaded(String targetId) {
    return loadedTargetIds.contains(targetId);
  }

  static setLoaded(String targetId) {
    if (!loaded(targetId)) {
      loadedTargetIds = [targetId, ...loadedTargetIds];
    }
  }

  static List<XTarget> members(String targetId) {
    return membersMap[targetId] ?? [];
  }

  static int memberCount(String targetId) {
    return memberCountMap[targetId] ?? 0;
  }

  static pullMembers(String targetId, List<XTarget> members, [int? count]) {
    members = members
        .where(
            (i) => TargetResource.members(targetId).every((j) => i.id != j.id))
        .toList();
    membersMap[targetId] = [...TargetResource.members(targetId), ...members];
    if (null != count) {
      memberCountMap[targetId] = count;
    }
  }

  static removeMembers(String targetId, List<XTarget> members) {
    membersMap[targetId] = TargetResource.members(targetId)
        .where((i) => members.every((j) => i.id != j.id))
        .toList();
  }

  static clear(String targetId) {
    membersMap.remove(targetId);
  }
}
