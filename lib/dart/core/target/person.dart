import 'package:get/get.dart';
import 'package:orginone/dart/base/common/commands.dart';
import 'package:orginone/dart/base/index.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/public/consts.dart';
import 'package:orginone/dart/core/public/operates.dart';
import 'package:orginone/dart/core/target/base/target.dart';
import 'package:orginone/dart/core/target/base/team.dart';
import 'package:orginone/dart/core/target/innerTeam/department.dart';
import 'package:orginone/dart/core/target/outTeam/cohort.dart';
import 'package:orginone/dart/core/target/outTeam/istorage.dart';
import 'package:orginone/dart/core/target/team/company.dart';
import 'package:orginone/dart/core/target/team/hospital.dart';
import 'package:orginone/dart/core/target/team/university.dart';
import 'package:orginone/dart/core/thing/fileinfo.dart';
import 'package:orginone/dart/extension/ex_list.dart';
import 'package:orginone/main.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../../base/model.dart';
import '../../base/schema.dart';
import '../public/enums.dart';
import '../public/objects.dart';
import 'base/belong.dart';

abstract class IPerson extends IBelong {
  //加入/管理的单位
  late List<ICompany> companys;

  /// 加入的部门
  late List<XTarget> departments;
  //赋予人的身份(角色)实体
  late List<XIdProof> givedIdentitys;
  //用户缓存对象
  @override
  late XObject<Xbase> cacheObj;

  /// 个人常用文件
  @override
  late List<XCommon> commons;

  /// 个人收藏
  late List<ActivityType> collectionsList;

  ///拉黑的人员列表
  late List blackList;

  ///当前选择的空间
  late IBelong currentSpace;
  late List<IBelong> belongList;

  //拷贝的文件
  late Map<String, IFileInfo<XEntity>> copyFiles;

  /// 判断是否已加入 部门
  bool hasJoinedTeam(String teamId);

  /// 更新常用
  Future<bool> updateCommons(List<XCommon> datas);

  /// 门户模板
  Future<bool> updatePortalTemplate(List<NavigationItem> datas);

  /// 常用设置
  Future<bool> toggleCommon(XCommon data, bool isSet);
  //根据ID查询共享信息
  ShareIcon findShareById(String id);
  //根据Id查询共享信息
  Future<XEntity?> findEntityAsync(String id);
  //判断是否拥有某些用户的权限
  bool authenticate(List<String> orgIds, List<String> authIds);
  // 加载赋予人的身份(角色)实体
  Future<List<XIdProof>> loadGivedIdentitys({bool reload = false});
  //赋予身份
  void giveIdentity(List<XIdentity> identitys, {String teamId});
  //移除赋予人的身份（角色）实体
  void removeGivedIdentity(List<String> identityIds, {String? teamId});
  // //加载单位
  // Future<List<ICompany>> loadCompanys({bool reload = false}); /////
  //创建单位
  Future<ICompany?> createCompany(TargetModel data);
  //搜索用户
  Future<List<XTarget>> searchTargets(String filter, List<String> typeNames);

  /// 根据归属单位获取本人所在部门
  List<IDepartment> getJoinedDepts(String belongId);
  //加载群组
  Future<List<ICohort>> loadCohorts({bool? reload});
  //查找单位
  ICompany? findCompany(String id);

  /// 更新收藏列表
  Future<bool> updateCollections(List<ActivityType> collections);

  /// 更新拉黑列表
  Future<bool> updateBlackList(String userId);

  ///更新当前选择的空间
  Future<bool> updateCurrentSpace(IBelong space);
}

//人员类型实现
class Person extends Belong implements IPerson {
  // @override
  // final XTarget metadata;
  @override
  List<ICompany> companys = [];
  @override
  List<XTarget> departments = [];
  late IActivity friendsActivity;
  @override
  late XObject<Xbase> cacheObj;

  /// 个人常用文件
  @override
  late List<XCommon> commons;

  ///
  @override
  late List<NavigationItem> itabitems;

  ///我的收藏
  @override
  late List<ActivityType> collectionsList;

  ///黑名单
  @override
  late List blackList;

  @override
  late IBelong currentSpace;

  @override
  List<XIdProof> givedIdentitys = [];

  @override
  List<String> get groupTags {
    return ['人员'];
  }

  @override
  Map<String, IFileInfo<XEntity>> copyFiles = {};
  bool _cohortLoaded = false;
  bool _givedIdentityLoaded = false;

  Person(metadata) : super(metadata, []) {
    cacheObj = XObject(metadata, 'target-cache', [], [key]);
    commons = [];
    collectionsList = [];
    blackList = [];
    currentSpace = this;

    ///TODO:FriendsActivity类需创建
// friendsActivity = FriendsActivity(this);
  }

  @override
  bool hasJoinedTeam(String id) {
    return [...companys, ...departments, ...cohorts].some(
      (i) => i.id == id,
    );
  }

  @override
  Future<List<XIdProof>> loadGivedIdentitys({bool reload = false}) async {
    if (!_givedIdentityLoaded || reload) {
      var res = await kernel.queryGivedIdentitys();
      if (res.success) {
        _givedIdentityLoaded = true;
        givedIdentitys = res.data?.result ?? [];
      }
    }
    return givedIdentitys;
  }

  @override
  void giveIdentity(List<XIdentity> identitys, {String teamId = ''}) {
    for (var identity in identitys) {
      if (!givedIdentitys.any(
        (a) => a.identityId == identity.id && a.teamId == teamId,
      )) {
        XIdProof xIdProof = XIdProof.fromJson(identity.toJson());
        xIdProof.identity = identity;
        xIdProof.teamId = teamId;
        xIdProof.identityId = identity.id;
        xIdProof.targetId = id;
        xIdProof.target = metadata;

        givedIdentitys.add(xIdProof);
      }
    }
  }

  @override
  void removeGivedIdentity(List<String> identityIds, {String? teamId}) {
    var idProofs = givedIdentitys
        .where((a) => identityIds.contains(a.identityId))
        .toList();
    if (teamId != null) {
      idProofs = idProofs.where((a) => a.teamId == teamId).toList();
    } else {
      idProofs = idProofs.where((a) => a.teamId == null).toList();
    }
    givedIdentitys
        .removeWhere((a) => idProofs.every((i) => i.id != a.identity?.id));
  }

  @override
  Future<List<ICohort>> loadCohorts({bool? reload = false}) async {
    if (!_cohortLoaded || reload!) {
      var res = await kernel.queryJoinedTargetById(GetJoinedModel(
        id: metadata.id,
        typeNames: [
          TargetType.cohort.label,
          TargetType.storage.label,
          TargetType.company.label,
          TargetType.hospital.label,
          TargetType.university.label,
        ],
        page: pageAll,
      ));
      if (res.success) {
        _cohortLoaded = true;
        cohorts = [];
        storages = [];
        companys = [];
        for (var i in res.data?.result ?? []) {
          switch (TargetType.getType(i.typeName)) {
            case TargetType.cohort:
              cohorts.add(Cohort(i, this, i.id));
              break;
            case TargetType.storage:
              storages.add(IIStorage(i, [], this));
              break;
            default:
              companys.add(_createCompany(i, this));
          }
        }
      } else {
        _cohortLoaded = false;
      }
    }
    return cohorts;
  }

  Future<List<ICohort>> loadTeams(bool? reload) async {
    if (!_cohortLoaded || (null != reload && reload)) {
      var res = await kernel.queryJoinedTargetById(GetJoinedModel(
        id: id,
        typeNames: [
          TargetType.cohort.label,
          TargetType.storage.label,
          TargetType.company.label,
          ...departmentTypes.map((e) => e.label),
        ],
        page: pageAll,
      ));
      if (res.success) {
        _cohortLoaded = true;
        cohorts = [];
        storages = [];
        companys = [];
        departments = [];
        for (var i in (res.data?.result ?? [])) {
          switch (TargetType.getType(i.typeName)) {
            case TargetType.cohort:
              cohorts.add(Cohort(i, this, i.id));
              break;
            case TargetType.storage:
              storages.add(IIStorage(i, [], this));
              break;
            case TargetType.company:
              companys.add(Company(i, this));
              break;
            default:
              if (departmentTypes.includes(TargetType.getType(i.typeName))) {
                departments.add(i);
              }
          }
        }
      }
    }
    return cohorts;
  }

  Company _createCompany(XTarget metadata, IPerson user) {
    switch (TargetType.getType(metadata.typeName ?? '')) {
      case TargetType.hospital:
        return Hospital(metadata, user);
      case TargetType.university:
        return University(metadata, user);
      default:
        return Company(metadata, user);
    }
  }

  @override
  Future<ICompany?> createCompany(TargetModel data) async {
    if (!companyTypes.contains(TargetType.getType(metadata.typeName ?? ''))) {
      data.typeName = TargetType.company.label;
    }
    data.public = false;
    data.teamCode = data.teamCode ?? data.code;
    data.teamName = data.teamName ?? data.name;
    var res = await create(data);
    if (res != null) {
      var company = _createCompany(res, this);
      await company.deepLoad();
      companys.add(company);
      await company.pullMembers([metadata]);
      return company;
    }
    return null;
  }

  Future<IStorage?> createStorage(TargetModel data) async {
    data.typeName = TargetType.storage.label;
    var metadata = await create(data);
    if (metadata != null) {
      var storage = IIStorage(metadata, [], this);
      await storage.deepLoad();
      storages.add(storage);
      await storage.pullMembers(user == null ? [] : [user!.metadata]);
      return storage;
    }
    return null;
  }

  @override
  Future<ITeam?> createTarget(TargetModel data) {
    switch (TargetType.getType(data.typeName)) {
      case TargetType.cohort:
        return createCohort(data);
      case TargetType.storage:
        return createStorage(data);
      default:
        return createCompany(data);
    }
  }

  @override
  bool authenticate(List<String> orgIds, List<String> authIds) {
    return givedIdentitys
        .where((element) => element.identity != null)
        .where((element) => orgIds.contains(element.identity!.shareId))
        .where((element) => authIds.contains(element.identity!.authId))
        .isNotEmpty;
  }

  // @override
  // Future<bool> pullMembers(List<XTarget> members, {bool? notity}) async {
  //   return await applyJoin(members);
  // }

  @override
  Future<bool> applyJoin(List<XTarget> members) async {
    var filter = members.where((element) {
      return [
        TargetType.person,
        TargetType.cohort,
        TargetType.storage,
        ...companyTypes
      ].contains(TargetType.getType(element.typeName!));
    }).toList();
    for (var value in filter) {
      if (TargetType.getType(value.typeName!) == TargetType.person) {
        await super.pullMembers([value]);
      }
      ResultType<bool> result = await kernel.applyJoinTeam(GainModel(
        id: value.id,
        subId: metadata.id,
      ));
    }
    return true;
  }

  @override
  Future<List<XTarget>> searchTargets(
      String filter, List<String> typeNames) async {
    var res = await kernel.searchTargets(SearchModel(
      filter,
      typeNames,
      page: pageAll,
    ));
    if (res.success) {
      return res.data?.result ?? [];
    }
    return [];
  }

  @override
  Future<bool> exit() async {
    return false;
  }

  @override
  Future<bool> delete({bool? notity = false}) async {
    if (notity == null || !notity) {
      return false;
    } else {
      await sendTargetNotity(OperateType.remove,
          sub: metadata, subTargetId: id);
      var res = await kernel.deleteTarget(IdModel(metadata.id));
      return res.success;
    }
  }

  @override
  List<ITarget> get subTarget {
    return [];
  }

  @override
  List<ITarget> get shareTarget => [this, ...cohorts];

  @override
  List<ITarget> get parentTarget => [...cohorts, ...companys];

  @override
  List<IBelong> get belongList {
    return [user as IBelong, ...(user?.companys as List<IBelong>)];
  }

  @override
  List<ISession> get chats {
    List<ISession> chats = [session];
    chats.addAll(cohortChats);
    chats.addAll(memberChats);
    return chats;
  }

  @override
  List<IActivity> get activitys {
    List<IActivity> activityDatas = [session.activity];
    for(var item in cohorts) {
      if (!activityDatas.contains(item.session.activity)) {
        activityDatas.add(item.session.activity);
      }
    }
    for(var item in memberChats) {
      if (!activityDatas.contains(item.activity)) {
        activityDatas.add(item.activity);
      }
    }
    return activityDatas;
  }

  @override
  List<ISession> get cohortChats {
    List<ISession> chats = [];
    var companyChatIds = <String>[];
    for (var company in companys) {
      for (var item in company.cohorts) {
        companyChatIds.add(item.session.chatdata.fullId);
      }
    }
    for (var value in cohorts) {
      if (!companyChatIds.contains(value.session.chatdata.fullId)) {
        chats.addAll(value.chats);
      }
    }
    // for (var item in storages) {
    //   chats.addAll(item.chats);
    // }
    return chats;
  }

  @override
  List<ITarget> get targets {
    List<ITarget> targets = [this, ...storages];

    for (var item in cohorts) {
      targets.addAll(item.targets);
    }
    return targets;
  }

  /// 深度加载数据
  @override
  Future<void> deepLoad({bool? reload = false}) async {
    XLogUtil.dd('>>>>>>Person.deepLoad.start $name');
    await cacheObj.all(reload!);
    await Future.wait([
      loadTeams(reload),
      _loadCommons(), //常用
      _loadCollections(), //收藏
      _loadBlackList(),
      // loadCohorts(reload: reload),
      // loadTeams(reload),
      loadMembers(reload: reload),
      loadSuperAuth(reload: reload),
      loadGivedIdentitys(reload: reload),
      // directory.loadDirectoryResource(reload: reload), ////////thing文件夹中
    ]);

    await _loadCurrentSpace();
    await Future.wait(companys.map((e) => e.deepLoad(reload: reload)));
    await Future.wait(cohorts.map((e) => e.deepLoad(reload: reload)));
    await Future.wait(storages.map((e) => e.deepLoad(reload: reload)));

    superAuth?.deepLoad(reload: reload);
    XLogUtil.dd('>>>>>>Person.deepLoad.end $name');
    changeCallback();
  }

  ///操作类未实现
  @override
  List<OperateModel> operates({int? mode}) {
    var operates = super.operates();
    OperateModel.fromJson(PersonJoins().toJson());
    OperateModel.fromJson(TargetOperates.newCompany.toJson());
    OperateModel.fromJson(TargetOperates.newStorage.toJson());

    operates.insert(0, OperateModel.fromJson(PersonJoins().toJson()));
    operates.insert(
        0, OperateModel.fromJson(TargetOperates.newCompany.toJson()));
    operates.insert(
        0, OperateModel.fromJson(TargetOperates.newStorage.toJson()));

    return operates;
  }

  @override
  List<IFileInfo<XEntity>> content({bool? args}) =>
      [...cohorts.map((e) => e), ...storages.map((e) => e)];
  @override
  Future<XEntity?> findEntityAsync(String id) async {
    if (id.isEmpty) return null;
    var metadata = findMetadata<XEntity>(id);
    if (metadata != null) return metadata;

    var res = await kernel.queryEntityById(IdModel(id));
    if (res.success && res.data != null) {
      updateMetadata(res.data!);
      return res.data;
    }
    return null;
  }

  @override
  ShareIcon findShareById(String id) {
    var metadata = findMetadata<XEntity>(id);
    if (metadata == null) {
      findEntityAsync(id);
    }
    return ShareIcon(
      name: metadata?.name ?? '',
      typeName: metadata?.typeName ?? '',
      avatar: parseAvatar(metadata?.icon),
    );
  }

  @override
  Future<bool> toggleCommon(XCommon data, bool isSet) async {
    if (isSet) {
      commons.insert(0, data);
    } else {
      commons = commons
          .where(
            (i) => !(i.id == data.id && i.spaceId == data.spaceId),
          )
          .toList();
    }
    if (await cacheObj.set('commons', commons)) {
      await cacheObj.notity('commons', commons,
          onlyTarget: true, ignoreSelf: false);
      return true;
    }
    return false;
  }

  @override
  Future<bool> updateCommons(List<XCommon> datas) async {
    if (commons.isEmpty) return false;
    commons = datas;
    if (await cacheObj.set('commons', commons)) {
      await cacheObj.notity('commons', commons,
          onlyTarget: true, ignoreSelf: false);
      return true;
    }
    return false;
  }

  @override
  Future<bool> updatePortalTemplate(List<NavigationItem> datas) async {
    if (itabitems.isEmpty) return false;
    itabitems = datas;
    if (await cacheObj.set('portalTemplate', itabitems)) {
      await cacheObj.notity('portalTemplate', itabitems,
          onlyTarget: true, ignoreSelf: false);
      return true;
    }
    return false;
  }

  @override
  Future<String> removeJoinTarget(XTarget target, {XTarget? operater}) async {
    var index = [...cohorts, ...companys, ...storages]
        .indexWhere((i) => i.id == target.id);

    if (index != -1) {
      var find = [...cohorts, ...companys, ...storages]
          .firstWhere((i) => i.id == target.id);
      await find.delete(notity: true);
      return '您已被从${target.name}移除';
    }
    if (id != target.id && operater?.id == target.id) {
      // return '您的好友${target.name}把您删除.';
      return '';
    }
    var memberIndex = members.indexWhere((i) => i.id == target.id);
    if (memberIndex != -1) {
      var find = members.firstWhere((i) => i.id == target.id);
      XLogUtil.d("removeJoinTarget");
      // await delete(notity: true);
      XLogUtil.d(members.map((e) => e.name).toList());
      members.remove(find);
      XLogUtil.d(members.map((e) => e.name).toList());
      return '您已经和${target.name}解除好友关系';
    }
    return '';
  }

  @override
  Future<String> addJoinTarget(XTarget target, {XTarget? operater}) async {
    TargetType? type = TargetType.getType(target.typeName ?? '');
    switch (type) {
      case TargetType.cohort:
        if (cohorts.every((i) => i.id != target.id)) {
          final cohort = Cohort(target, this, target.id);
          await cohort.deepLoad(reload: true);
          cohorts.add(cohort);
          return '您已成功加入到 ${target.name}.';
        }
        break;
      case TargetType.storage:
        if (storages.every((i) => i.id != target.id)) {
          final storage = IIStorage(target, [], this);
          await storage.deepLoad();
          storages.add(storage);
          return '您已成功加入到 ${target.name}.';
        }
        break;
      case TargetType.person:
        XLogUtil.d("addJoinTarget");
        XLogUtil.d(members.map((e) => e.name).toList());
        if (id != target.id && operater?.id == target.id) {
          // return ' ${target.name} 发来加好友申请.';
          return '';
        }
        if (members.every((i) => i.id != target.id)) {
          // LogUtil.d(members.map((e) => e.name).toList());
          members.add(target);
          // LogUtil.d(members.map((e) => e.name).toList());

          deepLoad();
          return '您已经和 ${target.name} 成为好友.';
        }
        return '';

      default:
        if (companyTypes.contains(TargetType.getType(target.typeName ?? ''))) {
          if (companys.every((i) => i.id != target.id)) {
            final company = _createCompany(target, this);
            await company.deepLoad();
            companys.add(company);
            return '您已成功加入到 ${target.name}.';
          }
        }
        break;
    }
    return '';
  }

  //查找单位
  @override
  ICompany? findCompany(String id) {
    ICompany? company = companys.firstWhereOrNull((e) => e.id == id);
    return company;
  }

  Future<void> _loadCommons() async {
    var data = await cacheObj.get<List<XCommon>>('commons', XCommon.fromList);
    if (null != data && data.isNotEmpty) {
      commons = data;
    }
    cacheObj.subscribe('commons', (dynamic res) {
      if (res && res is List<XCommon>) {
        commons = res;
        command.emitterFlag('commons', [true]);
      }
    });
  }

  // Future<void> _loadPortalTemplate() async {
  //   var data = await cacheObj.get<List<NavigationItem>>('portalTemplate', NavigationItem.fromList);
  //   if (null != data && data.isNotEmpty) {
  //     itabitems = data;
  //   }
  //   cacheObj.subscribe('portalTemplate', (dynamic res) {
  //     if (res && res is List<NavigationItem>) {
  //       itabitems = res;
  //       command.emitterFlag('portalTemplate', [true]);
  //     }
  //   });
  // }

  @override
  List<IDepartment> getJoinedDepts(String companyId) {
    final results = <IDepartment>[];
    final company = companys.firstWhere((company) => company.id == companyId);
    // for (var element in company.departments) {
    //   element.members.addAll(company.members);
    // }
    findMyDepts(company.departments ?? [], results);
    return results;
  }

  findMyDepts(List<IDepartment> depts, List<IDepartment> results) {
    for (final item in depts) {
      if (item.members.any((i) => i.id == id)) {
        if (item.children.isNotEmpty) {
          findMyDepts(item.children, results);
        }
        results.add(item);
      }
    }
  }

  @override
  Future<bool> updateCollections(List<ActivityType> datas) async {
    // if (collectionsList.isEmpty) return false;
    collectionsList = datas;
    if (await cacheObj.set('collections', collectionsList)) {
      await cacheObj.notity('collections', collectionsList,
          onlyTarget: true, ignoreSelf: false);
      return true;
    }
    return false;
  }

  @override
  Future<bool> updateBlackList(String userId) async {
    List tempBlackList = blackList;
    bool result = tempBlackList.contains(userId);
    if (result) {
      tempBlackList.removeWhere((element) => element == userId);
    } else {
      tempBlackList.add(userId);
    }
    if (await cacheObj.set('blackList', tempBlackList)) {
      await cacheObj.notity('blackList', tempBlackList,
          onlyTarget: true, ignoreSelf: false);
      return true;
    }
    return false;
  }

  Future<void> _loadCollections() async {
    var data = await cacheObj.get<List<ActivityType>>(
        'collections', ActivityType.fromList);
    if (null != data && data.isNotEmpty) {
      collectionsList = data;
    }
    cacheObj.subscribe('collections', (dynamic res) {
      if (res && res is List<ActivityType>) {
        collectionsList = res;
        command.emitterFlag('collections', [true]);
      }
    });
  }

  Future<void> _loadBlackList() async {
    // var data = await cacheObj.get<List<String>>('blackList');
    var data = cacheObj.getValue<List<String>>('blackList');
    if (null != data) {
      blackList = data;
    }
    cacheObj.subscribe('blackList', (dynamic res) {
      if (res && res is List<String>) {
        blackList = res;
        command.emitterFlag('blackList', [true]);
      }
    });
  }

  @override
  Future<bool> updateCurrentSpace(IBelong space) async {
    if (await cacheObj.set('currentSpace', space.id)) {
      currentSpace = space;
      await cacheObj.notity('currentSpace', space,
          onlyTarget: true, ignoreSelf: false);
      return true;
    }
    return false;
  }

  Future<void> _loadCurrentSpace() async {
    var data = cacheObj.getValue('currentSpace');
    if (null != data && data.isNotEmpty && data != "") {
      IBelong? res =
          belongList.firstWhereOrNull((element) => element.id == data);
      if (res != null) {
        currentSpace = res;
      } else {
        currentSpace = belongList[0];
      }
    }
    cacheObj.subscribe('currentSpace', (dynamic res) {
      if (res) {
        currentSpace = res;
        command.emitterFlag('currentSpace', [true]);
      }
    });
  }

  @override
  dynamic noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}
