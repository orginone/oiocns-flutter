import 'package:get/get.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/dart/base/regex/regex_constants.dart';

///执行状态
enum ExecuteStatus {
  /// 未开始
  notStart,

  /// 进行中
  starting,

  /// 失败
  failed,

  /// 完成
  success,
}

/// 用户对象类型
enum WorkType {
  //外部用户
  addPerson("加人员", XImage.workApplyAddPerson),
  addUnit("加单位", XImage.workApplyAddUnit),
  addGroup("加群组", XImage.workApplyAddGroup),
  addStorage("加存储资源", XImage.workApplyAddStorage),
  addCohort("加组织群", XImage.workApplyAddCohort),
  workSend("子流程", XImage.workSend),
  thing("事项", XImage.formWork);

  const WorkType(this.label, this.icon);

  final String label;
  final String icon;

  static String getName(TargetType type) {
    return type.label;
  }

  //判断是否是加操作
  static bool isAdd(String workType) {
    List adds = [
      WorkType.addPerson.label,
      WorkType.addUnit.label,
      WorkType.addGroup.label,
      WorkType.addStorage.label,
      WorkType.addCohort.label,
    ];
    return adds.contains(workType);
  }

  static WorkType? getType(String name) {
    return WorkType.values.firstWhereOrNull((element) => element.label == name);
  }
}

/// 用户对象类型
enum TargetType {
  //外部用户
  group("组织群", XImage.cluster),
  cohort("群组", XImage.communicationGroup),
  //内部用户
  college("学院", XImage.folder),
  department("部门", XImage.unitInstitution),
  office("办事处", XImage.unitInstitution),
  section("科室", XImage.unitInstitution),
  major("专业", XImage.unitInstitution),
  working("工作组", XImage.unitInstitution),
  research("研究所", XImage.unitInstitution),
  laboratory("实验室", XImage.unitInstitution),

  ///岗位
  station("岗位", XImage.folder),
  //自归属用户
  person("人员", XImage.user),
  company("单位", XImage.unit),
  university("大学", XImage.folder),
  hospital("医院", XImage.folder),
  storage("存储资源", XImage.folderStore),
  jobCohort("工作群", XImage.folder), //内核不存在了
  activity("动态", XImage.activity),
  ;

  const TargetType(this.label, this.icon);

  final String label;

  final String icon;

  static String getName(TargetType type) {
    return type.label;
  }

  static TargetType? getType(String name) {
    return TargetType.values
        .firstWhereOrNull((element) => element.label == name);
  }
}

/// 目录分类
// @Deprecated("DirectoryType")
enum DirectoryGroupType {
  // 人员
  person("人员", [SpaceEnum.groups, SpaceEnum.resources]),

  // 单位
  company("单位", [
    SpaceEnum.groups,
    SpaceEnum.internalAgent,
    SpaceEnum.cohorts,
    SpaceEnum.resources
  ]),

  // 存储
  storage("存储资源", [
    SpaceEnum.dataStandards,
    SpaceEnum.view,
    // SpaceEnum.businessModeling,
    SpaceEnum.applications,
    SpaceEnum.file,
    SpaceEnum.code,
    SpaceEnum.mirroring,
  ]),

  /// 数据标准
  dataStandards("标准", [
    SpaceEnum.property,
    SpaceEnum.species,
    SpaceEnum.dict,
  ]),

  /// 业务模型
  businessModeling("模型", [
    SpaceEnum.form,
    SpaceEnum.report,
    SpaceEnum.transfer,
    SpaceEnum.pageTemplate,
  ]),

  ///视图
  view("视图", [
    SpaceEnum.table,
    SpaceEnum.report,
    SpaceEnum.bulletinBoard,
  ]),

  ///应用
  report("应用", [
    SpaceEnum.module,
    SpaceEnum.work,
    SpaceEnum.form,
    SpaceEnum.role,
    SpaceEnum.dataMigration,
    SpaceEnum.shoppingPage,
  ]);

  const DirectoryGroupType(this.name, this.types);

  final List<SpaceEnum> types;
  final String name;

  static List<SpaceEnum> getName(DirectoryGroupType type) {
    return type.types;
  }

  static DirectoryGroupType? getType(String type) {
    return DirectoryGroupType.values
        .firstWhereOrNull((element) => element.name == type);
  }
}

///分类基础类型
enum SpeciesType {
  // 类别目录
  market('流通类'),
  resource('资源类'), //ts内核不存在
  store('属性类'),
  application('应用类'),
  dict('字典类'),
  // 类别类目
  flow('流程类'),
  work('事项配置'),
  thing('实体配置'),
  data('数据类');

  final String label;

  const SpeciesType(this.label);

  static SpeciesType? getType(String name) {
    try {
      return SpeciesType.values.firstWhere((element) => element.label == name);
    } catch (e) {
      return null;
    }
  }
}

/// 消息类型
enum MessageType {
  file("文件"),
  text("文本"),
  html("网页"),
  image("图片"),
  video("视频"),
  voice("语音"),
  recall("撤回"),
  activity("群动态"),
  card("名片"),
  notify('通知'), //ts还未实现
  uploading('upload'), //ts还未实现
  forward('转发'),
  task("任务");

  const MessageType(this.label);

  final String label;

  static String getName(MessageType type) {
    return type.label;
  }

  static MessageType? getType(String name) {
    return MessageType.values
        .firstWhereOrNull((element) => element.label == name);
  }
}

///任务状态
enum TaskStatus {
  applyStart(0),
  approvalStart(100), //已同意
  refuseStart(200), //已拒绝
  backStartStatus(230),
  undefinedStatus(250);

  final int status;
  const TaskStatus(this.status);
}

///节点类型
enum AddNodeType {
  CC('抄送'),
  ROOT('起始'), //已同意
  EMPTY('空节点'), //已拒绝
  APPROVAL('审批'),
  Confluence('汇流'),
  CUSTOM('自由节点'),
  CONDITION('条件'),
  CONCURRENTS('全部'),
  ORGANIZATIONA('组织'),
  GATEWAY('网关'),
  END('归档');

  final String nodetype;
  const AddNodeType(this.nodetype);
}

/// 通用状态
enum CommonStatus {
  applyStartStatus(1),
  approveStartStatus(100),
  rejectStartStatus(200);

  const CommonStatus(this.value);

  final int value;

  static int getValue(CommonStatus status) {
    return status.value;
  }
}

///变更操作
enum OperateType {
  add('新增'),
  create('创建'),
  remove('移除'),
  update('更新'),
  delete('删除');

  final String label;

  const OperateType(this.label);

  static OperateType? getType(String name) {
    return OperateType.values
        .firstWhereOrNull((element) => element.label == name);
  }
}

///值类型 getType操作需要根据实际情况再做修改
enum ValueType {
  number('数值型'),
  remark('描述性'),
  select('选择型'),
  species('分类型'),
  file('附件型'),
  time('时间型'),
  date('日期型'),
  target('用户型');

  final String label;

  const ValueType(this.label);

  static String getName(ValueType type) {
    return type.label;
  }

  static ValueType getType(String name) {
    return ValueType.values.firstWhere((element) => element.label == name);
  }
}

///规则触发时机 ts上存在需要根据实际情况删除或保留
enum RuleTriggers {
  start("Start"), //初始化
  running("Running"), //修改后
  submit("Submit"), //提交前
  thingsChanged("ThingsChanged"); //子表变化后

  final String label;

  const RuleTriggers(this.label);

  static RuleTriggers getTrigger(String name) {
    return RuleTriggers.values.firstWhere((element) => element.label == name);
  }
}

/// 域类型
enum DomainTypes {
  user("user"),
  company("company"),
  all("all");

  const DomainTypes(this.label);

  final String label;

  static String getName(MessageType type) {
    return type.label;
  }
}

enum ProductType {
  webApp("web应用");

  const ProductType(this.label);

  final String label;

  static String getName(ProductType type) {
    return type.label;
  }
}

/// 订单状态
enum OrderStatus {
  deliver(102),
  buyerCancel(220),
  sellerCancel(221),
  rejectOrder(222);

  const OrderStatus(this.value);

  final int value;

  static int getValue(OrderStatus status) {
    return status.value;
  }
}

enum TodoType {
  frientTodo("好友申请"),
  orgTodo("组织审批"),
  orderTodo("订单管理"),
  marketTodo("市场管理"),
  applicationTodo("应用待办");

  const TodoType(this.label);

  final String label;

  static String getName(ProductType type) {
    return type.label;
  }
}

enum SpaceEnum {
  directory("目录", XImage.folder),
  species("分类", XImage.species),
  filter("筛选", XImage.folder),
  property("属性", XImage.property),
  applications("应用", XImage.application),
  module("模块", XImage.module),
  work("办事", XImage.formWork),
  workStore("事项", XImage.formWork),

  ///表单
  form("表单", XImage.form),
  report("报表", XImage.folder),
  table("表格", XImage.folder),
  dataMigration("数据迁移", XImage.folder),
  shoppingPage("商城页面", XImage.folder),
  // model("模块", XImage.module),
  role("规则", XImage.folder),
  bulletinBoard("看板", XImage.folder),
  transfer("迁移", XImage.folder),
  pageTemplate("页面模板", XImage.pageTemplate),
  file("文件", XImage.folder),
  user('个人', XImage.folder),
  company("公司", XImage.folder),
  person("成员", XImage.folder),
  departments("部门", XImage.unitInstitution),

  ///群组
  groups("群组", "communicationGroup"),

  ///组织
  cohorts("组织群", XImage.cluster),

  ///好友
  firend("好友", XImage.folder),

  /// 资源
  resources("资源", XImage.folderStore),

  /// 成员
  member("成员", XImage.folder),

  /// 内设机构
  internalAgent("内设机构", XImage.unitInstitution),

  /// 数据标准
  dataStandards("标准", XImage.folder),

  ///视图
  view("视图", XImage.folder),

  /// 业务模型
  businessModeling("业务模型", XImage.folder),

  /// 代码
  code("代码", XImage.folder),

  /// 镜像
  mirroring("镜像", XImage.folder),

  /// 字典
  dict("字典", XImage.dictionary),
  ;

  final String label;
  final String icon;

  const SpaceEnum(this.label, this.icon);

  static String getName(SpaceEnum type) {
    return type.label;
  }

  static SpaceEnum? getType(String name) {
    return SpaceEnum.values
        .firstWhereOrNull((element) => element.label == name);
  }
}

enum StorageFileType {
  ///音频
  music("音频", XImage.music, ["video/mp3"], RegexConstants.audio),

  ///视频
  video("视频", XImage.video, ["video/mp4"], RegexConstants.vector),

  ///图片
  image(
      "图片",
      XImage.image,
      ["image/png", "image/jpeg", "image/jpg", "image/svg"],
      RegexConstants.image),

  ///pdf
  pdf("pdf", XImage.pdf, ["application/pdf"], RegexConstants.pdf),

  ///excel
  excel("excel", XImage.excel, ["xls", "xlsx"], RegexConstants.excel),

  ///word
  word("word", XImage.word, ["doc", "docx"], RegexConstants.doc),

  ///ppt
  ppt("ppt", XImage.ppt, ["ppt", "pptx"], RegexConstants.ppt),

  ///app
  app("app", XImage.app, ["apk", "ipa"], RegexConstants.app),

  ///文件
  file("文件", XImage.file, [], r'.*$'),
  ;

  final String label;
  final String icon;
  final List<String> suffixes;
  final Pattern pattern;
  const StorageFileType(this.label, this.icon, this.suffixes, this.pattern);

  static String getName(TargetType type) {
    return type.label;
  }

  static StorageFileType? getType(String name) {
    return StorageFileType.values
        .firstWhereOrNull((element) => element.suffixes.contains(name));
  }

  static StorageFileType getTypeByFileName(String name) {
    return StorageFileType.values.firstWhereOrNull((element) =>
            RegExp(element.pattern.toString(), caseSensitive: false)
                .hasMatch(name)) ??
        file;
  }
}

/// 职权类型
enum AuthorityType {
  superAdmin("super-admin"),
  relationAdmin("relation-admin"),
  thingAdmin("thing-admin"),
  marketAdmin("market-admin"),
  applicationAdmin("application-admin");

  const AuthorityType(this.label);

  final String label;

  static String getName(MessageType type) {
    return type.label;
  }
}

enum PopupMenuKey {
  createDir("新建目录"),
  createApplication("新建应用"),
  createSpecies("新建分类"),
  createDict("新建字典"),
  createAttr("新建属性"),
  createThing("新建实体配置"),
  createWork("新建事项配置"),
  createDepartment("新建部门"),
  createCompany("新建单位"),
  createStation("新建岗位"),
  createGroup("新建集群"),
  createCohort("新建群组"),
  updateInfo('更新信息'),
  rename("重命名"),
  delete("删除"),
  upload("上传文件"),
  shareQr("分享二维码"),
  setCommon("设置常用"),
  removeCommon("移除常用"),
  openChat("打开会话"),
  topping("置顶"),
  cancelTopping("取消置顶"),
  addPerson("邀请成员"),
  permission("权限设置"),
  role("角色设置"),
  station("岗位设置");

  final String label;

  const PopupMenuKey(this.label);
}

/// 文件类型
enum DirectoryType {
  /// 一级类目
  ///
  /// 存储资源
  storage("存储资源", XImage.folder),

  ///数据标准
  dataStandard("标准", XImage.folder),

  ///业务模型
  model("业务模型", XImage.folderModel),

  ///应用
  app("应用", XImage.folderApplication),

  ///办事
  work("办事", XImage.folder),

  ///文件
  file("文件", XImage.folderStore),

  ///代码
  code("代码", XImage.folderCode),

  ///镜像
  mirror("镜像", XImage.folderImage),

  /// 二级类目
  ///
  ///属性
  property("属性", XImage.folderProperties),

  ///分类
  species("分类", XImage.folderClassification),

  ///字典
  dict("字典", XImage.folderDictionary),

  ///表单
  form("表单", XImage.folderForm),

  ///报表
  report("报表", XImage.folder),

  ///模板
  pageTemplate("模板", XImage.folder),

  ///迁移
  transfer("迁移", XImage.folder),

  ///默认
  def("默认", XImage.folder);

  const DirectoryType(this.label, this.icon);

  final String label;
  final String icon;

  static String getName(DirectoryType type) {
    return type.label;
  }

  static DirectoryType? getType(String name) {
    return DirectoryType.values
        .firstWhereOrNull((element) => element.label == name);
  }
}

enum PeriodType {
  year('年'),
  quarter('季度'),
  month('月'),
  week('周'),
  day('日'),
  once('一次'),
  any('不定时');

  const PeriodType(this.label);
  final String label;

  static String getName(PeriodType type) {
    return type.label;
  }

  static PeriodType? getType(String name) {
    return PeriodType.values
        .firstWhereOrNull((element) => element.label == name);
  }
}

enum ReportTreeNodeTypes {
  // 单户（本级）类节点 0
  /// 实体本级节点
  normal('单户表'),

  /// 虚拟单户节点
  virtualUnit('虚拟单位表'),

  /// 外部单位节点，境外企业，在系统中不存在或者已注销
  externalUnit('外部单位表'),

  // 汇总（合并）类节点 1
  /// 集团合并（汇总）节点
  merge('集团合并表'),

  /// 完全（虚拟）汇总节点
  fullSummary('完全汇总表'),

  /// 针对涉密或其他因素，对一家单位单独开设的可填写汇总表
  singleSummary('单独汇总表'),

  // 调整（差额）类节点 2
  /// 集团差额节点
  balance('集团差额表'),

  /// 汇总调整节点
  summaryAdjust('汇总调整表'),
  other("未知表");

  const ReportTreeNodeTypes(this.label);
  factory ReportTreeNodeTypes.any(String lbl) => other;

  final String label;

  static String getName(ReportTreeNodeTypes type) {
    return type.label;
  }

  static ReportTreeNodeTypes getType(String name) {
    return ReportTreeNodeTypes.values
            .firstWhereOrNull((element) => element.label == name) ??
        ReportTreeNodeTypes.any(name);
  }
}

enum CacheKey {
  activity("切换动态", "activityId"),
  filter("切换应用", "assetmodule"),
  property("切换页面", "pagetemplatemodule"),
  ;

  final String label;
  final String key;

  const CacheKey(this.label, this.key);
}
