import 'session.dart';

///会话资源
class SessionResource {
  static Map<String, ISession> sessionsMap = {};

  static bool loaded(String sessionId) {
    return sessionsMap.containsKey(sessionId);
  }

  static ISession? sessions(String sessionId) {
    return sessionsMap[sessionId];
  }

  // static ISession _createSession(String sessionId) {
  //   return Session(sessionId, , metadata);
  // }

  static pullSession(String sessionId, ISession session) {
    sessionsMap[sessionId] = session;
  }

  static removeSession(String sessionId, ISession session) {
    sessionsMap.remove(sessionId);
  }
}
