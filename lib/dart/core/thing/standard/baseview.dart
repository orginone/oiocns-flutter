// import 'package:orginone/dart/base/model.dart';
// import 'package:orginone/dart/base/schema.dart';
// import 'package:orginone/dart/core/public/collection.dart';
// import 'package:orginone/dart/core/public/consts.dart';
// import 'package:orginone/dart/core/public/enums.dart';
// import 'package:orginone/dart/core/thing/directory.dart';
// import 'package:orginone/dart/core/thing/fileinfo.dart';
// import 'package:orginone/utils/log/log_util.dart';
//
// abstract class IBaseView implements IStandardFileInfo<XView> {
//   ///构造函数
//   IBaseView(this.attributes, this.fields);
//
//   ///表单特性
//   final List<XAttribute> attributes;
//
//   ///表单字段
//   final List<FieldModel> fields;
//
//   /// 加载字段
//   Future<List<FieldModel>> loadFields({bool reload = false});
//
//   ///新建表单特性
//   Future<XAttribute?> createAttribute(XAttribute data, {XProperty? property});
//
//   ///更新表单特性
//   Future<bool> updateAttribute(XAttribute data, {XProperty? property});
//
//   ///删除表单特性
//   Future<bool> deleteAttribute(XAttribute data);
//
//   /// 查询表数据
//   Future<LoadResult<dynamic>> loadThing(dynamic loadOptions);
//
//   /// 查询表汇总数据
//   Future<dynamic> loadSummary(dynamic loadOptions);
// }
//
// class BaseView extends StandardFileInfo<XView> implements IBaseView {
//   @override
//   final XView metadata;
//   @override
//   final IDirectory directory;
//   late bool _fieldsLoaded = false;
//   @override
//   late List<FieldModel> fields = [];
//   @override
//   List<XAttribute> get attributes => metadata.attributes ?? [];
//   @override
//   String get id => metadata.id.replaceAll('_', '');
//   @override
//   String get cacheFlag => 'forms';
//
//   ///构造函数
//   BaseView(this.metadata, this.directory)
//       : super(metadata, directory, directory.resource.viewColl) {
//     setEntity();
//     final resource = directory.resource;
//     if (metadata.collName != null) {
//       thingColl = resource.genColl<XThing>(metadata.collName!);
//     } else {
//       thingColl = resource.thingColl;
//     }
//   }
//
//   XCollection<XThing>? thingColl;
//   @override
//   Future<bool> loadContent({bool reload = false}) async {
//     fields = await loadFields(reload: reload);
//     // LogUtil.d('loadContent');
//     // LogUtil.d(fieds.map((e) => e.toJson()).toList());
//     return true;
//   }
//
//   @override
//   Future<List<FieldModel>> loadFields({bool reload = false}) async {
//     if (!_fieldsLoaded || reload) {
//       fields = [];
//       final speciesIds = attributes
//           .map((i) => i.property?.speciesId)
//           .toList()
//           .where((i) => i != null && i.isNotEmpty)
//           .toList()
//           .map((i) => i!)
//           .toList();
//       final data = await loadItems(speciesIds);
//       await Future.wait(attributes.map((attr) async {
//         if (attr.property != null) {
//           final field = FieldModel(
//             id: attr.id,
//             rule: attr.rule,
//             name: attr.name,
//             options: attr.options,
//             code: 'T${attr.property?.id}',
//             info: attr.code,
//             remark: attr.remark,
//             speciesId: attr.property?.speciesId,
//             lookups: [],
//             valueType: attr.property?.valueType,
//           );
//           if (attr.property!.speciesId != null &&
//               attr.property!.speciesId!.isNotEmpty) {
//             if (data.isNotEmpty) {
//               field.lookups = data
//                   .where((i) => i.speciesId == attr.property!.speciesId)
//                   .toList()
//                   .map((i) => FiledLookup(
//                         id: i.id,
//                         text: i.name,
//                         value: 'S${i.id}',
//                         icon: i.icon,
//                         info: i.info,
//                         remark: i.remark,
//                         parentId: i.parentId,
//                       ))
//                   .toList();
//             }
//           }
//           fields.add(field);
//         }
//       }));
//     }
//     _fieldsLoaded = true;
//     return fields;
//   }
//
//   Future<List<XSpeciesItem>> loadItems(List<String> speciesIds) async {
//     final ids = speciesIds.where((i) => i.isNotEmpty).toList();
//     if (ids.isEmpty) return [];
//
//     List<XSpeciesItem> data = await directory.resource.speciesItemColl.load({
//       'options': {
//         'match': {
//           'shareId': '',
//           'speciesId': {'_in_': ids}
//         }
//       },
//     }, (json) => XSpeciesItem.fromJson(json));
//     return data.toList();
//   }
//
//   @override
//   Future<XAttribute?> createAttribute(XAttribute data,
//       {XProperty? property}) async {
//     if (property != null) {
//       data.property = property;
//       data.propId = property.id;
//     }
//     if (data.authId != null || data.authId!.length < 5) {
//       data.authId = OrgAuth.superAuthId.label;
//     }
//     data.id = 'snowId()';
//     metadata.attributes = [...metadata.attributes ?? [], data];
//
//     final res = await update(metadata);
//     if (res) {
//       return data;
//     }
//     return null;
//   }
//
//   @override
//   Future<bool> updateAttribute(XAttribute data, {XProperty? property}) async {
//     final index = attributes.indexWhere((i) => i.id == data.id);
//     XAttribute oldData = attributes.firstWhere((i) => i.id == data.id);
//     if (index > -1) {
//       data = XAttribute.fromJson({...oldData.toJson(), ...data.toJson()});
//       if (property != null) {
//         data.propId = property.id;
//         data.property = property;
//       }
//       metadata.attributes = [
//         ...attributes.where((a) => a.id != data.id).toList(),
//         data
//       ];
//       final res = await update(metadata);
//       return res;
//     }
//     return false;
//   }
//
//   @override
//   Future<bool> deleteAttribute(XAttribute data) async {
//     final index = attributes.indexWhere((i) => i.id == data.id);
//     if (index > -1) {
//       metadata.attributes = [
//         ...attributes.where((a) => a.id != data.id).toList(),
//         data
//       ];
//       final res = await update(metadata);
//       return res;
//     }
//     return false;
//   }
//
//   @override
//   Future<bool> copy(IDirectory destination) async {
//     if (allowCopy(destination)) {
//       return await super
//           .copyTo(destination.id, coll: destination.resource.viewColl);
//     }
//     return false;
//   }
//
//   @override
//   Future<bool> move(IDirectory destination) async {
//     if (allowMove(destination)) {
//       return await super
//           .moveTo(destination.id, coll: destination.resource.viewColl);
//     }
//     return false;
//   }
//
//   /// 查询表数据
//   @override
//   Future<LoadResult<dynamic>> loadThing(dynamic loadOptions) async {
//     loadOptions = loadOptions ?? {};
//     loadOptions['options'] = loadOptions['options'] ?? {};
//     loadOptions['options']['match'] = loadOptions["options"]["match"] ?? {};
//     loadOptions["options"]["match"]["isDeleted"] = false;
//
//     // Map match=  loadOptions["options"]["match"] ;
// //  match.addAll(other)
//     final belongs = [TargetType.company.label, TargetType.person.label];
//     if (belongs.contains(target.typeName)) {
//       loadOptions["options"]["match"]["belongId"] = belongId;
//     }
//     XLogUtil.d("表单 加载数据loadThing");
//     XLogUtil.d(loadOptions);
//     var res = await thingColl?.loadResult(loadOptions);
//     if (res?.success != null && res?.success == true && res?.data is! List) {
//       res?.data = [];
//     }
//
//     // res?.summary = await loadSummary(loadOptions);
//     return LoadResult.fromJson(res!.toJson());
//   }
//
//   /// 查询表汇总数据
//   // @override
//   // Future<dynamic> loadSummary(dynamic loadOptions) {
//   //     const filters = fields.filter((item) => item.options?.isSummary);
//   //   if (filters.length == 0) {
//   //     return [];
//   //   }
//   //   const group: { [key: string]: any } = { key: [] };
//   //   for (const field of filters) {
//   //     group[field.code] = { _sum_: '$' + field.code };
//   //   }
//   //   const match = {
//   //     ...loadOptions.options.match,
//   //     ...common.filterConvert(JSON.stringify(loadOptions.filter)),
//   //   };
//   //   if (loadOptions.userData.length > 0) {
//   //     if (match.labels) {
//   //       match._and_ = match._and_ || [];
//   //       match._and_.push({ labels: { _in_: loadOptions.userData } });
//   //       match._and_.push({ labels: { ...match.labels } });
//   //       delete match.labels;
//   //     } else {
//   //       match.labels = { _in_: loadOptions.userData };
//   //     }
//   //   }
//   //   const options = [{ match }, { group }, { limit: 1 }];
//   //   const result = await kernel.collectionAggregate(
//   //     belongId,
//   //     [belongId],
//   //     thingColl.collName,
//   //     options,
//   //   );
//   //   return filters.map((item) => {
//   //     if (result.data && result.data.length > 0) {
//   //       return result.data[0][item.code];
//   //     }
//   //     return 0;
//   //   });
//   // }
//
//   @override
//   dynamic noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
// }
