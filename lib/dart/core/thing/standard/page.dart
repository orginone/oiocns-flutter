import 'package:orginone/dart/base/common/commands.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/thing/directory.dart';
import 'package:orginone/dart/core/thing/fileinfo.dart';
import 'package:orginone/dart/core/work/index.dart';

import 'application.dart';
import 'form.dart';
import 'species.dart';

abstract class IPageTemplate extends IStandardFileInfo<XPageTemplate> {
  late Command command;
  List<ISpecies> species = [];
  late String relations;
  Future<List<ISpecies>> loadSpecies(List<String> speciesIds);

  /// 表单
  List<IForm> forms = [];
  Future<List<IForm>> loadForms(List<String> formsIds);

  Future<IWork?> loadWorks(String workId);
}

class PageTemplate extends StandardFileInfo<XPageTemplate>
    implements IPageTemplate {
  PageTemplate(XPageTemplate metadata, IDirectory directory)
      : super(metadata, directory, directory.resource.templateColl) {
    command = Command();
  }
  @override
  bool canDesign = true;
  @override
  late Command command;
  @override
  List<ISpecies> species = [];

  @override
  List<IForm> forms = [];
  @override
  String get cacheFlag => 'pages';

  @override
  String get relations =>
      '$belongId:${directory.target.spaceId}-${directory.target.id}';

  @override
  Future<bool> copy(IDirectory destination) async {
    if (allowCopy(destination)) {
      return await super
          .copyTo(destination.id, coll: destination.resource.templateColl);
    }
    return false;
  }

  @override
  Future<bool> move(IDirectory destination) async {
    if (allowMove(destination)) {
      return await super
          .moveTo(destination.id, coll: destination.resource.templateColl);
    }
    return false;
  }

  @override
  bool receive(String operate, dynamic data) {
    var d = data as XStandard;
    coll.removeCache((i) => i.id != d.id);
    // super.receive(operate, data);
    coll.cache.add(metadata);
    return true;
  }

  @override
  Future<List<ISpecies>> loadSpecies(List<String> speciesIds) async {
    final already = species.map((item) => item.id).toList();
    final filter =
        speciesIds.where((speciesId) => !already.contains(speciesId)).toList();
    if (filter.isNotEmpty) {
      final species = await directory.resource.speciesColl.find(ids: filter);
      for (final item in species) {
        final meta = Species(item, directory);
        await meta.loadContent();
        this.species.add(meta);
      }
    }
    final result = <ISpecies>[];
    for (final speciesId in speciesIds) {
      final item = species.firstWhere((one) => one.id == speciesId);
      result.add(item);
    }
    return result;
  }

  @override
  Future<List<IForm>> loadForms(List<String> formIds) async {
    final already = forms.map((item) => item.id).toList();
    final filter =
        formIds.where((formId) => !already.contains(formId)).toList();
    if (filter.isNotEmpty) {
      // final formList = await directory.resource.formColl.find(ids: filter);
      final formList = await directory.target.resource.formColl.load({
        'options': {
          'match': {
            '_id': {'_in_': formIds}
          }
        }
      }, (json) => XForm.fromJson(json));
      for (final item in formList) {
        final meta = Form(item, directory);
        await meta.loadContent();
        forms.add(meta);
      }
    }
    final result = <IForm>[];
    if (forms.isNotEmpty) {
      for (final formId in formIds) {
        final item = forms.firstWhere((one) => one.id == formId);
        result.add(item);
      }
    }

    return result;
  }

  @override
  Future<IWork?> loadWorks(String wordId) async {
    List<IApplication> appList =
        await directory.target.directory.loadAllApplication();
    for (var app in appList) {
      IWork? result = await app.findWork(wordId);
      if (result != null) {
        return result;
      }
    }
    return null;
  }

  @override
  dynamic noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}
