/// 扩展 int
extension ExInt on int {
  static int? parse(dynamic val) {
    int? number;
    if (null != val) {
      if (val is String) {
        number = int.tryParse(val);
      } else if (val is int) {
        number = val;
      }
    }

    return number;
  }
}
