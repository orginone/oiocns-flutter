import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import '../../routers/pages.dart';

class XConsumer<T> extends Consumer<T> {
  late Widget _cacheWidget;
  late final String routePath;

  XConsumer({super.key, required super.builder}) {
    this.routePath = currentRoutePath;
  }

  @override
  Widget buildWithChild(BuildContext context, Widget? child) {
    ///判断是否当前路由
    if (isCurrentRoute()) {
      _cacheWidget = builder(
        context,
        Provider.of<T>(context),
        child,
      );
    }
    return _cacheWidget;
  }

  String get currentRoutePath {
    return RoutePages.routeData.currPageData.path ?? '';
  }

  bool isCurrentRoute() {
    return routePath == currentRoutePath;
  }
}
