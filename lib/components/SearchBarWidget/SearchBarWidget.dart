import 'package:flutter/material.dart';
import 'package:orginone/components/ListWidget/ListWidget.dart';
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';
import 'package:orginone/components/ActivityWidget/ActivityListWidget/ActivityListWidget.dart';
import 'package:orginone/dart/controller/index.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/work/task.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/pages.dart';
import 'package:orginone/utils/log/log_util.dart';

class SearchBarWidget<T> extends SearchDelegate {
  final HomeEnum homeEnum;

  final List<T> data;

  SearchBarWidget(
      {required this.homeEnum,
      required this.data,
      super.searchFieldLabel = "请输入关键字"});

  List<T> searchData = [];

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(
          Icons.clear,
          color: Colors.black,
        ),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    XLogUtil.d('>>>>>>>>>>>>>buildActions');
    return BackButton(
      color: Colors.black,
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    XLogUtil.d('>>>>>>>>>>>>>buildResults');
    search();
    return _body(context);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    XLogUtil.d('>>>>>>>>>>>>>buildSuggestions');
    search();
    return _body(context);
  }

  void search() {
    searchData.clear();
    if (query.isEmpty) {
      searchData.addAll(data);
      return;
    }

    for (var element in data) {
      switch (homeEnum) {
        case HomeEnum.chat:
          if ((element as ISession).chatdata.chatName.contains(query) ??
              false) {
            searchData.add(element);
          }
          break;
        case HomeEnum.work:
          if ((element as IWorkTask).taskdata.title!.contains(query)) {
            searchData.add(element);
          }
          break;
        case HomeEnum.store:
          // var recent = (element as RecentlyUseModel);
          // if (recent.type == StoreEnum.file.label) {
          //   if (recent.file!.name!.contains(query)) {
          //     searchData.add(element);
          //   }
          // } else {
          //   if (recent.thing!.id!.contains(query)) {
          //     searchData.add(element);
          //   }
          // }
          break;
        case HomeEnum.door:
          if ((element as ISession).chatdata.chatName.contains(query) ??
              false) {
            searchData.add(element);
          }
          break;
        default:
      }
    }
  }

  Widget _body(BuildContext context) {
    return ListWidget<T>(
      initDatas: searchData,
      getDatas: ([dynamic data]) {
        if (null == data) {
          return searchData ?? [];
        }
        return [];
      },
      // getAction: (dynamic data) {
      //   return Text(
      //     CustomDateUtil.getSessionTime(data.updateTime),
      //     style: XFonts.chatSMTimeTip,
      //     textAlign: TextAlign.right,
      //   );
      // },
      onTap: (dynamic data, List children) {
        XLogUtil.d('>>>>>>======点击了列表项 ${data.name}');
        if (data is ISession) {
          switch (relationCtrl.homeEnum.value) {
            case HomeEnum.chat:
              RoutePages.jumpChatSession(data: data);
              break;
            case HomeEnum.work:
              break;
            case HomeEnum.door:
              // RoutePages.jumpChatSession(data: data, defaultTabs: ["动态"]);
              RoutePages.jumpActivityPage(context, data: data.activity);
              break;
            case HomeEnum.store:
              break;
            case HomeEnum.relation:
              break;
            case HomeEnum.setting:
              break;
          }
        } else if (data is IWorkTask) {
          RoutePages.jumpWorkInfo(work: data);
        }
      },
    );
  }

  Widget body() {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: searchData.length,
        itemBuilder: (BuildContext context, int index) {
          var item = searchData[index];
          switch (homeEnum) {
            case HomeEnum.chat:
            // return ListItem(
            //   adapter: ListAdapter.chat(item as ISession),
            // );
            case HomeEnum.work:
            // return ListItem(adapter: ListAdapter.work(item as IWorkTask));
            case HomeEnum.store:
            // return ListItem(
            //     adapter: ListAdapter.store(item as RecentlyUseModel));
            default:
          }
          return Container();
        });
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    XLogUtil.d('>>>>>>>>>>>>>appBarTheme');
    return super.appBarTheme(context).copyWith(
          appBarTheme: super.appBarTheme(context).appBarTheme.copyWith(
                elevation: 0.0,
              ),
        );
  }
}

class ShowActivityPage extends XStatefulWidget {
  @override
  ShowActivityPage({super.key, super.data});

  @override
  State<ShowActivityPage> createState() => _ShowActivityPageState();
}

class _ShowActivityPageState extends XStatefulState<ShowActivityPage, dynamic> {
  @override
  Widget buildWidget(BuildContext context, data) {
    return ActivityListWidget(
      activity: data,
    );
  }
}
