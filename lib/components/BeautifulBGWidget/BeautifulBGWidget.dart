import 'package:flutter/material.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/routers/pages.dart';

class BeautifulBGWidget extends BeautifulBGStatelessWidget {
  bool showBack;
  bool showLogo;
  Widget body;

  BeautifulBGWidget(
      {super.key,
      required this.body,
      this.showBack = true,
      this.showLogo = true});

  @override
  Widget buildWidget(BuildContext context) {
    return formLayout(context,
        form: body, showBack: showBack, showLogo: showLogo);
  }
}

///有背景的静态组件
abstract class BeautifulBGStatelessWidget extends StatelessWidget
    with _BeautifulBGController {
  BeautifulBGStatelessWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return _build(context);
  }
}

/// 有背景的动态组件
abstract class BeautifulBGStatefulWidget extends StatefulWidget {
  const BeautifulBGStatefulWidget({super.key});
}

abstract class BeautifulBGStatefulState<T extends BeautifulBGStatefulWidget>
    extends State<T> with _BeautifulBGController {
  @override
  Widget build(BuildContext context) {
    return _build(context);
  }
}

mixin _BeautifulBGController {
  @protected
  Widget buildWidget(BuildContext context);

  Widget _build(BuildContext context) {
    return Scaffold(
        body: Container(
      clipBehavior: Clip.antiAlias,
      decoration: const BoxDecoration(color: Colors.white),
      child: Stack(
        children: [
          //背景图模块
          _background(),
          buildWidget(context)
        ],
      ),
    ));
  }

  ///上对其布局
  Widget topLayout(BuildContext context,
      {bool showBack = true, required Widget top}) {
    return Stack(
      children: [
        if (showBack)
          Positioned(
            top: 60,
            left: 0,
            right: 0,
            child: Container(
                padding: const EdgeInsets.only(left: 20),
                alignment: Alignment.centerLeft,
                child: GestureDetector(
                  onTap: (() {
                    RoutePages.back(context);
                  }),
                  child: XIcons.arrowBack32,
                )),
          ),
        Positioned(top: 200, left: 0, right: 0, child: top),
      ],
    );
  }

  ///上下对其布局
  Widget topAndBottomLayout(BuildContext context,
      {bool showBack = true, required Widget top, required Widget bottom}) {
    return Stack(
      children: [
        if (showBack)
          Positioned(
            top: 60,
            left: 0,
            right: 0,
            child: Container(
                padding: const EdgeInsets.only(left: 20),
                alignment: Alignment.centerLeft,
                child: GestureDetector(
                  onTap: (() {
                    RoutePages.back(context);
                  }),
                  child: XIcons.arrowBack32,
                )),
          ),
        Positioned(top: 0, left: 0, right: 0, child: top),
        Positioned(bottom: 0, left: 0, right: 0, child: bottom),
      ],
    );
  }

  ///表单布局
  Widget formLayout(BuildContext context,
      {required Widget form, bool showBack = true, bool showLogo = true}) {
    return Stack(
      children: [
        if (showBack)
          Positioned(
            top: 50,
            left: 0,
            right: 0,
            child: Container(
                padding: const EdgeInsets.only(left: 20),
                alignment: Alignment.centerLeft,
                child: GestureDetector(
                  onTap: (() {
                    RoutePages.back(context);
                  }),
                  child: XIcons.arrowBack32,
                )),
          ),
        if (showLogo)
          Positioned(top: 100, left: 0, right: 0, child: buildLogoRow()),
        Positioned(
          top: showLogo ? 160 : 75,
          left: 0,
          right: 0,
          child: form,
        ),
      ],
    );
  }

  /// 图标（左图右文字）
  Widget buildLogoRow() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(0, 5, 5, 0),
            width: 30,
            height: 30,
            clipBehavior: Clip.antiAlias,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(XImage.logoNotBg),
                fit: BoxFit.fill,
              ),
            ),
          ),
          const Text.rich(TextSpan(
            text: '奥集能',
            style: TextStyle(
                color: Color(0xFF15181D),
                fontSize: 22.91,
                fontFamily: 'PingFang SC',
                fontWeight: FontWeight.w500,
                decoration: TextDecoration.none),
          )),
        ],
      ),
    );
  }

  // 图标（上图下文字）
  Widget buildLogoColumn() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(0, 5, 5, 20),
            width: 40,
            height: 40,
            clipBehavior: Clip.antiAlias,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(XImage.logoNotBg),
                fit: BoxFit.fill,
              ),
            ),
          ),
          const Text.rich(TextSpan(
            text: '奥集能',
            style: TextStyle(
                color: Color(0xFF15181D),
                fontSize: 23,
                fontFamily: 'PingFang SC',
                fontWeight: FontWeight.w500,
                decoration: TextDecoration.none),
          )),
        ],
      ),
    );
  }

  //背景图
  Widget _background() {
    return Stack(
      children: [
        Positioned(
          left: -200,
          child: Container(
            width: 900,
            height: 500,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(XImage.logoBackground),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Positioned(
          left: -200,
          child: Container(
            width: 900,
            height: 500,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromRGBO(249, 249, 249, 0),
                  Color.fromRGBO(255, 255, 255, 1),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
