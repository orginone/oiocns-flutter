/*
 * @Descripttion: 
 * @version: 
 * @Author: 
 * @Date: 
 */

import 'package:extended_tabs/extended_tabs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_utils/src/extensions/export.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/components/ExpandTabBar/ExpandTabBar.dart';
import 'package:orginone/components/Tip/NetworkTipWidget.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/utils/log/log_util.dart';
import '../TabContainerWidget.dart';
import '../types.dart';

/// 移动端列表信息页面
class TabWidget extends StatefulWidget {
  ///信息列表页签模型
  ITabModel iTabModel;

  TabWidget(this.iTabModel, {super.key});

  @override
  _TabPageState createState() => _TabPageState();
}

class _TabPageState extends State<TabWidget> {
  ///活跃页签
  late TabItemsModel activeTab;

  @override
  void initState() {
    super.initState();
    if (widget.iTabModel.activeTabTitle != null) {
      TabItemsModel? activeTabTmp = widget.iTabModel.tabItems.firstWhereOrNull(
          (element) => element.title == widget.iTabModel.activeTabTitle);
      activeTab = activeTabTmp ?? widget.iTabModel.tabItems.first;
    } else {
      activeTab = widget.iTabModel.tabItems.first;
    }
  }

  bool hasSubTabPage() {
    return widget.iTabModel.tabItems
            .any((element) => element.tabItems.isNotEmpty) ||
        !equalsContent();
  }

  bool hasIcon() {
    return widget.iTabModel.tabItems.any((element) => null != element.icon);
  }

  bool equalsContent() {
    Widget? content;
    return widget.iTabModel.tabItems.every((element) {
      if (null == content) {
        content = element.content;
        return true;
      }
      XLogUtil.d(
          '>>>>>>======${element.content.runtimeType} ${content.runtimeType} ${element.content.runtimeType == content.runtimeType}');
      return element.content.runtimeType == content.runtimeType;
    });
  }

  bool _hasSubTabPage(TabItemsModel item) {
    return item.tabItems.isNotEmpty;
  }

  changeTab(int index) {
    activeTab = widget.iTabModel.tabItems[index];
    widget.iTabModel.activeTabTitle = activeTab.title;
  }

  bool isActiveTab(TabItemsModel tab) {
    return activeTab == tab;
  }

  @override
  Widget build(BuildContext context) {
    final ancestorState = context.findAncestorStateOfType<InfoListPageState>();
    XLogUtil.d('>>>>>>======$ancestorState');

    return _tabPage();
  }

  int getDefaultTabIndex() {
    int index = widget.iTabModel.tabItems.indexWhere(
        (element) => element.title == widget.iTabModel.activeTabTitle);
    if (index < 0) {
      index = 0;
    }
    return index;
  }

  ///页签
  Widget _tabPage() {
    return DefaultTabController(
      length: widget.iTabModel.tabItems.length,
      initialIndex: getDefaultTabIndex(),
      child: Scaffold(
        // backgroundColor: Colors.white,
        appBar: _tabBar(widget.iTabModel.tabItems),
        body: ExtendedTabBarView(
            children:
            widget.iTabModel.tabItems.map((tab) => _content(tab)).toList()),
      ),
    );
  }

  PreferredSizeWidget _tabBar(List<TabItemsModel> tabItems) {
    XLogUtil.d('>>>>>>======${tabItems.first.title} ${tabItems.length}');
    return hasIcon() ? _tabItems(tabItems) : _subTabBar(tabItems);
  }

  PreferredSizeWidget _tabItems(List<TabItemsModel> tabItems) {
    bool hasIcon = tabItems.first.icon != null;
    return PreferredSize(
        preferredSize: const Size.fromHeight(45.0),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                  bottom: BorderSide(color: Colors.grey.shade400, width: 0.4),
                )),
            child: ExpandTabBar(
              // bgColor: Colors.white,
              isScrollable: tabItems[0].icon != 'p' ? false : true,
              labelColor: XColors.selectedColor,
              unselectedLabelColor: XColors.black666,
              labelPadding: tabItems[0].icon != 'p' ? const EdgeInsets.only(left: 5, right: 5, bottom: 5) : const EdgeInsets.only(left: 10, right: 10, bottom: 10),
              labelStyle:
                  TextStyle(fontSize: 14),
              unselectedLabelStyle: TextStyle(fontSize: 14),
              indicator: UnderlineTabIndicator(
                  borderSide: const BorderSide(
                      width: 2.0, color: XColors.selectedColor),
                  insets:
                      EdgeInsets.symmetric(horizontal: tabItems.length * 10)),
              tabNames: tabItems.map((TabItemsModel tab) {
                return tab.title;
              }).toList(),
              buildTabItem: (BuildContext context,
                  String tabName,
                  Color? color,
                  Color? bgColor,
                  Color? borderColor,
                  TextStyle? labelStyle,
                  int index,
                  bool selected) {
                return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if (hasIcon && tabItems[0].icon != 'p')
                          XImage.localImage(tabItems[index].icon!,
                              color: color, width: 32.w),
                        Text(tabName),
                      ],
                    ) ??
                    Tab(
                      text: tabName,
                      iconMargin: const EdgeInsets.only(bottom: 0.0),
                      icon: hasIcon
                          ? XImage.localImage(tabItems[index].icon!,
                              color: color)
                          : null,
                    );
              },
              // tabs: tabItems.map((TabItemsModel tab) {
              //   return Tab(
              //       text: null == tab.icon ? tab.title : null,
              //       icon: null != tab.icon ? XImage.localImage(tab.icon!) : null);
              // }).toList(),
            )));
  }

  PreferredSizeWidget _subTabBar(List<TabItemsModel> tabItems) {
    return PreferredSize(
        preferredSize: Size.fromHeight(42.w),
        child: Container(
          decoration: BoxDecoration(
              border: Border(
               bottom: BorderSide(color: Colors.grey.shade400, width: 0.4),
              )
          ),
          child: Container(
            padding: EdgeInsets.only(bottom: 10.h, left: 5.w),
            color: Colors.white,
            child: Row(
              children: [
                Expanded(child: _subTabItems(tabItems)),
                // IconButton(
                //   onPressed: () {
                //     // _showSubTabSettings(infoListPageController.getActiveSubTab());
                //   },
                //   alignment: Alignment.center,
                //   icon: const Icon(
                //     Icons.menu,
                //     color: Colors.black,
                //   ),
                //   iconSize: 24.w,
                //   padding: EdgeInsets.zero,
                // )
              ],
            ),
          ),
        ));
  }

  Widget _subTabItems(List<TabItemsModel> tabItems) {
    return Theme(
        data: ThemeData(
          highlightColor: Colors.red,
          splashColor: Colors.red,
        ),
        child: ExpandTabBar(
          onTap: changeTab,
          isScrollable: true,
          // automaticIndicatorColorAdjustment: true,
          indicatorWeight: 0,
          // padding: const EdgeInsets.all(5),
          // indicatorPadding: const EdgeInsets.all(5),
          labelPadding: const EdgeInsets.only(left: 10, right: 10),
          // indicatorColor: Colors.red,
          labelColor: XColors.tabtextChooseColor,
          unselectedLabelColor: XColors.black,
          bgColor: XColors.tabbgunChooseColor,
          unselectedBgColor: Colors.white,
          borderColor: XColors.tabbgunChooseColor,
          unselectedBorderColor: XColors.cardBackgroundColor,
          // dividerColor: Colors.red,
          // splashBorderRadius: BorderRadius.circular(10.w),
          // splashFactory: NoSplash.splashFactory,
          // overlayColor: MaterialStateProperty.resolveWith<Color?>(
          //   (Set<MaterialState> states) {
          //     LogUtil.d('>>>>>>======>$states');
          //     return states.contains(MaterialState.pressed)
          //         ? Colors.red
          //         : Colors.blue;
          //   },
          // ),
          labelStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
          unselectedLabelStyle: TextStyle(fontSize: 14),
          indicator: const UnderlineTabIndicator(),
          // indicator: UnderlineTabIndicator(
          //   borderRadius: BorderRadius.circular(4.w),
          //   borderSide: const BorderSide(width: 2.0, color: Colors.black),
          //   insets: const EdgeInsets.all(5),
          // ),
          tabs: tabItems.map((TabItemsModel tab) {
            return Tab(text: tab.title);
          }).toList(),
        ));
  }

  ///页签内容
  Widget _content(TabItemsModel item) {
    return _hasSubTabPage(item) ? _subTabContent(item) : _tabContent(item);
  }

  Widget _subTabContent(TabItemsModel item) {
    return TabWidget(item);
  }

  Widget _tabContent(TabItemsModel item) {
    return __tabContent(item) ?? _tabDefContent(item);
  }

  Widget? __tabContent(TabItemsModel item) {
    return null != item.content
        ? Container(
            child: Column(
              children: [
                const NetworkTipWidget(),
                Expanded(child: item.content!)
              ],
            ),
          )
        : null;
  }

  Widget _tabDefContent(TabItemsModel item) {
    return Center(child: Text(item.title));
  }
}
