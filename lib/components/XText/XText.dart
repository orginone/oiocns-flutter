import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XText/components/TextTag.dart';
import 'package:orginone/dart/extension/ex_list.dart';
import 'package:orginone/dart/extension/ex_widget.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../../config/theme/space.dart';
import '../../config/theme/unified_style.dart';

class XText extends StatelessWidget {
  /// 文字字符串
  final String? text;

  /// 样式
  final TextStyle? style;

  /// 颜色
  final Color? color;

  /// 必填字段
  final bool requiredField;

  /// 突出显示
  final bool highlight;

  /// 大小
  final double? size;

  /// 重量
  final FontWeight? weight;

  /// 行数
  final int? maxLines;

  /// 自动换行
  final bool? softWrap;

  /// 溢出
  final TextOverflow? overflow;

  /// 对齐方式
  final TextAlign? textAlign;

  const XText({
    Key? key,
    this.text,
    this.style,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  }) : super(key: key);

  /// 页面标题
  const XText.headerTitle(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.w600,
        ),
        super(key: key);

  /// 属性、分类头标题
  static Widget boxTitle(String title) {
    return <Widget>[
      Container()
          .backgroundColor(XColors.blue)
          .width(4)
          .paddingLeft(10)
          .height(16)
          .paddingRight(5),
      // .paddingTop(4),
      XText.classTitle(title).paddingBottom(2),
    ]
        .toRow(
          crossAxisAlignment: CrossAxisAlignment.center,
        )
        .backgroundColor(XColors.white)
        .height(30);
  }

  /// 归类标题
  const XText.classTitle(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
        super(key: key);

  /// 提示文本-目前用于分享entity二维码下方的提示信息
  const XText.tip(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          fontSize: 12,
          color: XColors.gray_66,
        ),
        super(key: key);

  /// 标签文本
  static Widget tag(String text, {highlight = false}) {
    return TextTag(
      text,
      bgColor: Colors.white,
      padding: const EdgeInsets.symmetric(vertical: 1, horizontal: 3),
      textStyle: TextStyle(
        color: highlight ? XColors.fontErrorColor : XColors.designBlue,
        fontSize: 14.sp,
      ),
      borderColor: highlight ? XColors.fontErrorColor : XColors.tinyBlue,
    );
  }

  /// 弹窗标题
  const XText.dialogTitle(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
        super(key: key);

  /// 弹窗标题
  const XText.dialogContent(
    this.text, {
    Key? key,
    this.maxLines = 20,
    this.softWrap = true,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color = XColors.gray_66,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
        super(key: key);

  /// 列表标题
  const XText.listTitle(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(fontSize: 16.5, overflow: TextOverflow.ellipsis
            // fontWeight: FontWeight.w600,
            ),
        super(key: key);

  /// 列表副标题
  const XText.listSubTitle(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.ellipsis,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          color: XColors.chatHintColors,
          fontWeight: FontWeight.w400,
          overflow: TextOverflow.ellipsis,
        ),
        super(key: key);

//   /// 文字 - 按钮
//   TextWidget.button({
//     Key? key,
//     required this.text,
//     this.maxLines = 1,
//     this.softWrap = false,
//     this.overflow = TextOverflow.clip,
//     Color? color,
//     this.size,
//     this.weight,
//     this.textAlign,
//   })  : color = color ?? XColors.secondary,
//         style = TextStyle(
//           fontSize: 14,
//           fontWeight: FontWeight.w500,
//           color: color,
//         ),
//         super(key: key);

  /// 按钮空心
  const XText.buttonHollow(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = true,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          fontSize: 16,
          fontFamily: 'PingFang SC',
          fontWeight: FontWeight.w600,
        ),
        super(key: key);

  /// 按钮实心
  const XText.buttonSolid(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          color: Colors.white,
          fontSize: 16,
          fontFamily: 'PingFang SC',
          fontWeight: FontWeight.w600,
        ),
        super(key: key);

  /// 表单字段名称
  const XText.fieldName(
    this.text, {
    Key? key,
    this.maxLines = 1,
    this.softWrap = false,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.clip,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.w500,
        ),
        super(key: key);

  /// 表单字段值
  const XText.fieldValue(
    this.text, {
    Key? key,
    this.maxLines = 2,
    this.softWrap = true,
    this.requiredField = false,
    this.highlight = false,
    this.overflow = TextOverflow.ellipsis,
    this.color,
    this.size,
    this.weight,
    this.textAlign,
  })  : style = const TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.w500,
        ),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return null == text || "" == text
        ? const SizedBox()
        : requiredField
            ? <Widget>[
                const Text(
                  '*',
                  style: TextStyle(color: Colors.red),
                ).paddingRight(AppSpace.iconTextSmail),
                _buildText(context)
              ].toRow()
            : _buildText(context);
  }

  Widget _buildText(BuildContext context) {
    final highlightColor = Theme.of(context).primaryColor;
    XLogUtil.dd('>>>>>> text $text $size');
    return Text(
      text!,
      style: style?.copyWith(
            color: color ?? (highlight ? highlightColor : Colors.black),
            fontSize: size,
            fontWeight: weight,
          ) ??
          TextStyle(
            color: color ?? (highlight ? highlightColor : Colors.black),
            fontSize: size,
            fontWeight: weight,
          ),
      overflow: overflow,
      maxLines: maxLines,
      softWrap: softWrap,
      textAlign: textAlign,
    );
  }
}
