import 'package:flutter/material.dart';
import 'package:orginone/dart/base/common/commands.dart';
import 'package:orginone/utils/log/log_util.dart';
import 'package:provider/provider.dart';

///命令动态组件
class CommandWidget<T> extends StatefulWidget {
  /// 模块
  String? type;

  /// 命令
  String? cmd;

  /// 标记
  String? flag;
  Widget Function(BuildContext, T?)? builder;
  List<Widget>? children;

  CommandWidget(
      {super.key, this.type, this.cmd, this.flag, this.builder, this.children});

  @override
  State<StatefulWidget> createState() => _CommandState<T>();
}

class _CommandState<T> extends State<CommandWidget<T>> {
  String? get type => widget.type;
  String? get cmd => widget.cmd;
  String? get flag => widget.flag;
  Widget Function(BuildContext, T?)? get builder => widget.builder;
  List<Widget>? get children => widget.children;
  CommandModel<T>? comandModel;

  _CommandState() {
    comandModel = null;
  }

  @override
  void initState() {
    super.initState();
    comandModel = CommandModel<T>(type, cmd, flag);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => comandModel,
        builder: (context, child) => _buildWatch(context));
  }

  Widget _buildWatch(BuildContext context) {
    CommandModel<T> cm = context.watch<CommandModel<T>>();
    return null != children
        ? Stack(
            children: [
              if (null != builder) builder!.call(context, cm.data),
              ...?children
            ],
          )
        : builder?.call(context, cm.data) ?? Container();
  }

  // Widget _buildConsumer(BuildContext context) {
  //   return Consumer<CommandModel<T>>(
  //     builder: (context, data, child) {
  //       return null != children
  //           ? Stack(
  //               children: [
  //                 if (null != builder) builder!.call(context, data.data),
  //                 ...?children
  //               ],
  //             )
  //           : builder?.call(context, data.data) ?? Container();
  //     },
  //   );
  // }

  @override
  void dispose() {
    super.dispose();
    comandModel?.unsubscribe();
  }
}

class CommandModel<T> extends ChangeNotifier {
  String? key;
  String? keyFlag;
  late T? data;
  CommandModel(String? type, String? cmd, String? flag) {
    data = null;
    key = null;
    keyFlag = null;
    if (null != cmd) {
      key = command.subscribe((t, c, args) {
        XLogUtil.dd(">>>>>>command $hasListeners ${args is T} $t $c $args");
        if (t == type && c == cmd) {
          if (args is T) {
            data = args;
          }
          if (hasListeners) {
            XLogUtil.dd(">>>>>>command $t $c $args");
            notifyListeners();
          }
        }
        return;
      });
    }
    if (null != flag) {
      keyFlag = command.subscribeByFlag(flag, ([args]) {
        XLogUtil.dd(
            ">>>>>>command flag $hasListeners ${args is T} old:$data new:$args");
        if (null != args && args is T) {
          data = args;
        }
        if (hasListeners) {
          notifyListeners();
        }
      });
    }
  }

  void unsubscribe() {
    if (null != key) {
      command.unsubscribe(key);
    }
    if (null != keyFlag) {
      command.unsubscribeByFlag(keyFlag!);
    }
  }
}
