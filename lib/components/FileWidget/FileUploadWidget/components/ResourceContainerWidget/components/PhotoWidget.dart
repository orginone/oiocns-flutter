import 'package:flutter/material.dart';
import 'package:orginone/routers/pages.dart';
import 'package:photo_view/photo_view_gallery.dart';

class PhotoWidget extends StatelessWidget {
  final ImageProvider imageProvider;

  const PhotoWidget({
    Key? key,
    required this.imageProvider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        RoutePages.back(context);
      },
      child: PhotoViewGallery.builder(
        scrollPhysics: const BouncingScrollPhysics(),
        builder: (BuildContext context, int index) {
          return PhotoViewGalleryPageOptions(
            imageProvider: imageProvider,
          );
        },
        itemCount: 1,
      ),
    );
  }
}
