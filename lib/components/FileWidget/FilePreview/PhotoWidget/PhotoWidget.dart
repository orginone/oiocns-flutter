import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:orginone/components/KeepAliveWidget/KeepAliveWidget.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import 'package:orginone/routers/pages.dart';
import 'package:orginone/utils/file_utils.dart';
import 'package:photo_manager/photo_manager.dart';

class PhotoWidget extends StatefulWidget {
  const PhotoWidget({super.key});

  @override
  State<PhotoWidget> createState() => _PhotoState();
}

class _PhotoState extends State<PhotoWidget> {
  late List<dynamic> images;
  late ExtendedPageController pageController;
  var currentIndex = 0;

  @override
  void initState() {
    Map<String, dynamic>? args = RoutePages.routeData.currPageData.data;
    images = args?["images"] ?? [];
    int index = args?["index"] ?? 0;
    currentIndex = index;
    pageController = ExtendedPageController(initialPage: index);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return XScaffold(
      appBarColor: Colors.transparent,
      titleWidget: Text(
        "${currentIndex + 1}/${images.length}",
        style: const TextStyle(color: Colors.white),
      ),
      backColor: Colors.white,
      backgroundColor: Colors.black,
      body: InkWell(
        onTap: () {
          RoutePages.back(context);
        },
        child: Stack(
          children: <Widget>[
            ExtendedImageGesturePageView.builder(
              itemCount: images.length,
              itemBuilder: (context, index) {
                var image = images[index];
                ImageProvider provider;
                if (image is AssetEntity) {
                  provider = AssetEntityImageProvider(image);
                } else {
                  if (FileUtils.isAssetsImg(image)) {
                    provider = AssetImage(image);
                  } else if (FileUtils.isNetworkImg(image)) {
                    provider = CachedNetworkImageProvider(image);
                  } else {
                    provider = ExtendedResizeImage(
                      FileImage(File(image)),
                      width: 375,
                    );
                  }
                }
                return KeepAliveWidget(
                  child: ExtendedImage(
                    image: provider,
                    fit: BoxFit.contain,
                    mode: ExtendedImageMode.gesture,
                    clearMemoryCacheWhenDispose: true,
                    initGestureConfigHandler: (state) {
                      return GestureConfig(
                        inPageView: true,
                        minScale: 0.5,
                      );
                    },
                  ),
                );
              },
              controller: pageController,
              onPageChanged: (index) {
                onPageChanged(index);
              },
            ),
          ],
        ),
      ),
    );
  }

  void onPageChanged(int index) {
    currentIndex = index;
    setState(() {});
  }
}
