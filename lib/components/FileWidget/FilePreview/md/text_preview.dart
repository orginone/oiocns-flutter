import 'package:flutter/material.dart';

import '../../../XStatefulWidget/XStatefulWidget.dart';

class TextPreview extends XStatelessWidget<String> {
  TextPreview({super.key});

  @override
  Widget buildWidget(BuildContext context, String data) {
    return Container(
      margin: const EdgeInsets.only(top: 1),
      padding: const EdgeInsets.all(10),
      color: Colors.white,
      child: SingleChildScrollView(
        child: Text(data),
      ),
    );
  }
}
