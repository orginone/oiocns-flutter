import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../XImage/XImage.dart';
import '../XText/XText.dart';

/// 单行文本字段
class XTextField extends StatelessWidget {
  final TextEditingController? controller;
  final ValueChanged<String>? onSubmitted;
  final ValueChanged<String>? onChanged;
  final String? icon;
  final String? title;
  final String hint;
  final bool showLine;
  final Color? backgroundColor;
  final Color? searchColor;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final bool autofocus;

  /// 字段最右边操作按钮
  final Widget? action;
  final List<TextInputFormatter>? inputFormatters;
  final bool obscureText;
  final bool required;
  final int? maxLines;
  final bool? enabled;
  final String? content;
  const XTextField(
      {super.key,
      this.controller,
      this.icon,
      this.onChanged,
      this.onSubmitted,
      this.title,
      this.hint = "",
      this.showLine = false,
      this.backgroundColor,
      this.searchColor,
      this.margin,
      this.padding,
      this.autofocus = false,
      this.action,
      this.inputFormatters,
      this.obscureText = false,
      this.required = false,
      this.enabled,
      this.content,
      this.maxLines = 1});

  //搜索框
  const XTextField.search({
    super.key,
    this.controller,
    this.onSubmitted,
    this.onChanged,
    this.hint = "搜索",
    this.showLine = false,
    this.backgroundColor,
    this.margin,
    this.padding,
    this.autofocus = false,
  })  : action = null,
        searchColor = null, //const Color(0xFFF3F4F5),
        icon = XImage.search,
        inputFormatters = null,
        obscureText = false,
        title = null,
        required = false,
        maxLines = null,
        content = null,
        enabled = null;

  /// 输入框
  XTextField.input(
      {super.key,
      this.title,
      this.icon,
      this.hint = "",
      this.controller,
      this.content,
      this.action,
      this.required = false,
      this.onChanged,
      this.enabled,
      this.showLine = false,
      this.obscureText = false,
      this.maxLines = 1})
      : inputFormatters = [
          FilteringTextInputFormatter.singleLineFormatter,
        ],
        autofocus = false,
        backgroundColor = null,
        margin = null,
        onSubmitted = null,
        padding = null,
        searchColor = null;

  @override
  Widget build(BuildContext context) {
    var searchC = searchColor ??
        Theme.of(context).primaryColor; //Colors.blue.withOpacity(0.1);
    return Container(
      // width: 343,
      height: 45,
      margin: const EdgeInsets.all(5),
      padding: const EdgeInsets.symmetric(horizontal: 12),
      clipBehavior: Clip.antiAlias,
      decoration: ShapeDecoration(
        // color: searchC,
        shape: RoundedRectangleBorder(
          side: const BorderSide(width: 1, color: Color(0xFFE7E8EB)),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: SizedBox(
              height: 30,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (null != icon) XImage.entityIcon(icon, color: searchC),
                  // Expanded(
                  //     flex: 1,
                  //     child: Container(
                  //       height: 22,
                  //       decoration: BoxDecoration(
                  //         image: DecorationImage(
                  //           image: AssetImage(icon!),
                  //           fit: BoxFit.contain,
                  //         ),
                  //       ),
                  //     )),
                  if (null != title)
                    XText.fieldName(
                      title!, requiredField: required,
                      // style: TextStyle(fontSize: 20.sp),
                    ),
                  Expanded(
                      // flex: 8,
                      child: Container(
                          padding: const EdgeInsets.fromLTRB(7, 0, 0, 0),
                          child: TextField(
                            maxLines: maxLines,
                            enabled: enabled,
                            controller: controller ??
                                TextEditingController(text: content),
                            onSubmitted: onSubmitted,
                            onChanged: onChanged,
                            autofocus: autofocus,
                            obscureText: obscureText,
                            inputFormatters: inputFormatters,
                            decoration: InputDecoration(
                                isCollapsed: true,
                                hintText: hint,
                                contentPadding: const EdgeInsets.all(4),
                                hintStyle: TextStyle(
                                    color: Colors.grey.shade400,
                                    fontSize: 22.sp),
                                border: InputBorder.none),
                          ))),
                  if (null != action) action ?? Container(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
