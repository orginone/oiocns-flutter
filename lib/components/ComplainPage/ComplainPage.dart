import 'package:flutter/material.dart';
import 'package:orginone/components/XButton/XButton.dart';
import 'package:orginone/config/theme/UIConfig.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import 'package:orginone/components/Tip/ToastUtils.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/routers/pages.dart';

///举报页面
class ComplainPage extends StatefulWidget {
  const ComplainPage({Key? key}) : super(key: key);

  @override
  State<ComplainPage> createState() => _ComplainPageState();
}

class _ComplainPageState extends State<ComplainPage> {
  List<dynamic> reasonList = [
    {"index": 100, "value": "垃圾广告"},
    {"index": 101, "value": "虚假不实消息"},
    {"index": 102, "value": "人身攻击"},
    {"index": 103, "value": "侵犯隐私"},
    {"index": 104, "value": "违法违规"},
    {"index": 105, "value": "低俗"}
  ];
  int selectReason = 0;

  @override
  Widget build(BuildContext context) {
    return XScaffold(
      titleName: '举报',
      actions: [
        XButton.link(
          '提交',
          onPressed: () async {
            if (selectReason == 0) {
              await ToastUtils.showMsg(msg: "请选择举报理由");
            }
            Future.delayed(const Duration(milliseconds: 400), () {
              ToastUtils.showMsg(msg: "举报成功");
              RoutePages.back(context);
            });
          },
          // margin: const EdgeInsets.all(10),
          // borderRadius: 4,
          // color: XColors.white,
          // fontWeight: FontWeight.w600,
          // height: 24,
          // textSize: 16,
          // padding: const EdgeInsets.all(0),
          // textColor: XColors.primary
        )
      ],
      body: Column(
        children: [
          Container(
            width: UIConfig.screenWidth,
            padding: const EdgeInsets.fromLTRB(16, 18, 16, 18),
            child: const Text('请选择你的举报理由*',
                style: TextStyle(
                  color: Colors.black,
                  fontFamily: "PingFang SC",
                  fontWeight: FontWeight.w400,
                  fontSize: 18,
                )),
          ),
          Container(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
              child: buildGrid()),
        ],
      ),
    );
  }

  Widget buildGrid() {
    List<Widget> tiles = [];
    Widget content;
    for (var item in reasonList) {
      tiles.add(InkWell(
        highlightColor: Colors.transparent,
        radius: 0,
        onTap: () {
          setState(() {
            if (selectReason == item['index']) {
              selectReason = 0;
            } else {
              selectReason = item['index'];
            }
          });
        },
        child: Container(
          // decoration: BoxDecoration(
          //   border: Border(
          //     bottom: BorderSide(
          //       width: 0.5, //宽度
          //       color: Colors.green, //边框颜色
          //     ),
          //   ),
          // ),
          padding: const EdgeInsets.only(top: 18, bottom: 18),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.check_circle_outline,
                color: selectReason == item['index']
                    ? XColors.primary
                    : XColors.gray_99,
                size: 24,
              ),
              const SizedBox(width: 8),
              Text(
                item['value'],
                style: TextStyle(
                    color: selectReason == item['index']
                        ? XColors.primary
                        : XColors.black,
                    fontFamily: "PingFang SC",
                    // fontWeight: FontWeightStyle.regular,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ],
          ),
        ),
      ));
    }
    content = Column(children: tiles);
    return content;
  }
}
