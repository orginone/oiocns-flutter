import 'package:flutter/material.dart';
import 'package:orginone/components/ChatSessionWidget/ChatSessionWidget.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';
import 'package:orginone/components/ActivityWidget/ActivityListWidget/ActivityListWidget.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/dart/core/target/base/target.dart';
import 'package:orginone/routers/index.dart';
import '../../CommandWidget/index.dart';
import '../../EntityWidget/EntitySettingWidget/EntitySettingWidget.dart';
import '../../FileWidget/FileListWidget/FileListWidget.dart';
import '../../MemberListWidget/MemberListWidget.dart';
import '../../TabContainerWidget/TabContainerWidget.dart';
import '../../TabContainerWidget/types.dart';

/// 群组关系
class RelationCohortWidget extends XStatefulWidget<dynamic> {

  TabContainerModel? relationModel;
  List<ISession>? datas;

  RelationCohortWidget({super.key, List<ISession>? datas}) {
    relationModel = null;
    dynamic params = RoutePages.getParentRouteParam();
    this.datas = datas ?? (params is List<ISession> ? params : null);
  }

  @override
  State<RelationCohortWidget> createState() => _RelationCohortPageState();
}

class _RelationCohortPageState extends XStatefulState<RelationCohortWidget, dynamic> {
  // late TabContainerModel? relationModel;
  final ScrollController scrollController = ScrollController();

  TabContainerModel? get relationModel => widget.relationModel;
  set relationModel(TabContainerModel? value) {
    widget.relationModel = value;
  }

  @override
  Widget buildWidget(BuildContext context, data) {
    if (null == relationModel) {
      load();
    }
    return TabContainerWidget(
      relationModel!,
    );
  }

  void load() {
    relationModel = TabContainerModel(
        title: RoutePages.getRouteTitle() ??
            data.name ??
            data.typeName ??
            TargetType.cohort.label,
        activeTabTitle: getActiveTabTitle(),
        tabItems: [
          // createTabItemsModel(title: "好友"),
          TabItemsModel(
            title: "沟通",
            icon: XImage.chatOutline,
            // content: buildChats(),
            content: CommandWidget(
                type: 'chat',
                cmd: 'new',
                builder: (context, args) {
                  return buildChats();
                }),
          ),
          TabItemsModel(
            title: "动态",
            icon: XImage.dynamicOutline,
            content: CommandWidget(
                type: 'activity',
                cmd: 'refresh',
                builder: (context, args) {
                  return buildActivity();
                }),),
          TabItemsModel(
              title: "文件", icon: XImage.fileOutline, content: buildFiles()),
          TabItemsModel(
              title: "群成员", icon: XImage.memberOutline, content: buildPersons()),
          TabItemsModel(
              title: "设置",
              icon: XImage.settingOutline,
              content: buildSetting()),
        ]);
  }

  /// 获得激活页签
  getActiveTabTitle() {
    return RoutePages.getRouteDefaultActiveTab()?.first;
  }

  Widget buildActivity() {
    ISession? chat;
    if (data is ISession) {
      chat = data;
    } else if (data is ITarget) {
      chat = data.session;
    } else {
      //TODO新建会话
    }
    return ActivityListWidget(scrollController: scrollController, activity: chat?.activity/*, isFromChat: true*/);
  }

  Widget buildChats() {
    return ChatSessionWidget(data: data);
  }

  Widget buildFiles() {
    return FileListWidget(data: data);
  }

  Widget buildPersons() {
    return MemberListWidget(data: data);
  }

  Widget buildSetting() {
    return EntitySettingWidget(data: data);
  }
}
