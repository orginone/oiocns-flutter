import 'package:flutter/material.dart';
import 'package:orginone/components/ChatSessionWidget/ChatSessionWidget.dart';
import 'package:orginone/components/XImage/components/icon.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/components/ListWidget/ListWidget.dart';
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';
import 'package:orginone/components/ActivityWidget/ActivityListWidget/ActivityListWidget.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/index.dart';
import 'package:orginone/utils/log/log_util.dart';
import '../../CommandWidget/index.dart';
import '../../EntityWidget/EntitySettingWidget/EntitySettingWidget.dart';
import '../../MemberListWidget/MemberListWidget.dart';
import '../../TabContainerWidget/TabContainerWidget.dart';
import '../../TabContainerWidget/types.dart';

class RelationFriendWidget extends XStatelessWidget {
  late TabContainerModel? relationModel;
  RelationFriendWidget({super.key, super.data}) {
    relationModel = null;
  }

  @override
  Widget buildWidget(BuildContext context, dynamic data) {
    if (null == relationModel) {
      load();
    }

    return TabContainerWidget(relationModel!);
  }

  void load() {
    relationModel = TabContainerModel(
        title: RoutePages.getRouteTitle() ?? entity.name ?? "好友",
        activeTabTitle: getActiveTabTitle(),
        tabItems: [
          // createTabItemsModel(title: "好友"),
          TabItemsModel(
              title: "沟通", icon: XImage.chatOutline,
              // content: buildChats(),
              content: CommandWidget(
              type: 'chat',
              cmd: 'new',
              builder: (context, args) {
                return buildChats();
              }),
          ),
          TabItemsModel(
              title: "动态",
              icon: XImage.dynamicOutline,
              content: CommandWidget(
                  type: 'activity',
                  cmd: 'refresh',
                  builder: (context, args) {
                    return buildActivity();
                  }),),
          if (isSelf)
            TabItemsModel(
                title: "成员",
                icon: XImage.memberOutline,
                content: buildPersons()),
          TabItemsModel(
              title: "设置",
              icon: XImage.settingOutline,
              content: buildSetting()),
          // TabItemsModel(title: "成员", content: const Text("文件")),
          // TabItemsModel(title: "文件", content: const Text("文件")),
        ]);
  }

  bool get isSelf {
    return entity.id == relationCtrl.user?.id;
  }

  /// 获得激活页签
  getActiveTabTitle() {
    return RoutePages.getRouteDefaultActiveTab()?.first;
  }

  Widget buildActivity() {
    ISession? chat;
    if (data is ISession) {
      chat = data;
    } else if (isSelf) {
      chat = relationCtrl.user?.session;
    } else {
      chat = relationCtrl.user?.findMemberChat(data.id);
    }
    return ActivityListWidget(activity: chat?.activity, isFromChat: true);
  }

  TabItemsModel createTabItemsModel({
    required String title,
  }) {
    return TabItemsModel(
        title: title,
        content: ListWidget<XTarget>(
          getDatas: ([dynamic data]) {
            return getFriends();
          },
          getAction: (dynamic data) {
            return GestureDetector(
              onTap: () {
                XLogUtil.d('>>>>>>======点击了感叹号');
              },
              child: const IconWidget(
                color: XColors.chatHintColors,
                iconData: Icons.info_outlined,
              ),
            );
          },
          onTap: (dynamic data, List children) {
            XLogUtil.d('>>>>>>======点击了列表项 ${data.name}');
          },
        ));
  }

  Widget buildChats() {
    var session = data;
    if (isSelf) {
      session = relationCtrl.user?.session;
    } else if (session is XTarget) {
      session = relationCtrl.user?.findMemberChat(session.id);
    }
    return ChatSessionWidget(data: session);
  }

  List<XTarget> getFriends() {
    return relationCtrl.user?.members ?? [];
  }

  Widget buildPersons() {
    return MemberListWidget(data: relationCtrl.user);
  }

  Widget buildSetting() {
    var session = data;
    if (session is XTarget) {
      session = relationCtrl.user?.findMemberChat(session.id);
    }
    return EntitySettingWidget(data: session);
  }
}
