import 'package:flutter/widgets.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'EmptyWidget.dart';

class EmptyWork extends StatelessWidget {
  const EmptyWork({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EmptyWidget(title: "暂无符合条件的数据", iconPath: XImage.emptyWork);
  }
}
