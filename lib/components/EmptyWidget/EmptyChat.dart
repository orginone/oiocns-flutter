import 'package:flutter/widgets.dart';
import 'package:orginone/components/XImage/XImage.dart';

import 'EmptyWidget.dart';

class EmptyChat extends StatelessWidget {
  const EmptyChat({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EmptyWidget(title: "暂无聊天记录", iconPath: XImage.emptyChat);
  }
}
