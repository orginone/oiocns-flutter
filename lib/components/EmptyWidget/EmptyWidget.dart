import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orginone/components/EmptyWidget/EmptyWork.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/core/public/entity.dart';

import '../../dart/core/chat/activity.dart';
import '../../dart/core/chat/session.dart';
import '../../dart/core/thing/directory.dart';
import '../../dart/core/thing/fileinfo.dart';
import 'EmptyActivity.dart';
import 'EmptyChat.dart';
import 'EmptyFile.dart';

/// 空白页
class EmptyWidget extends StatelessWidget {
  String? title;
  String? iconPath;
  double? heightva;


  EmptyWidget({Key? key, this.title, this.iconPath, this.heightva}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: heightva ?? Get.height,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            XImage.localImage(iconPath ?? XImage.empty, width: 150),
            const SizedBox(
              width: double.infinity,
              height: 15,
            ),
            Text(
              title ?? "暂无数据",
              style: const TextStyle(color: XColors.blueGrey),
            ),
            const SizedBox(
              width: double.infinity,
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}

/// 空页面
mixin EmptyMixin<T> {
  Widget buildEmptyWidget(
      [bool? isEmpty = true, Widget? child, Widget? defEmpty]) {
    Widget defEmptyWidget = defEmpty ?? EmptyWidget();
    Widget content = isEmpty! ? defEmptyWidget : child ?? Container();
    if (isEmpty) {
      switch (T) {
        case ISession:
          content = const EmptyChat();
          break;
        case IFileInfo:
        case IDirectory:
          content = const EmptyFile();
          break;
        case IActivity:
          content = const EmptyActivity();
          break;
        // case IEntity:
        //   content = const EmptyWork();
        //   break;
      }
    }
    return content;
  }
}
