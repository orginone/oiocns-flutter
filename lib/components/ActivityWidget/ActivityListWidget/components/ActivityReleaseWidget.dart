import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';
import 'package:orginone/components/Tip/ToastUtils.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/dart/core/thing/systemfile.dart';
import 'package:orginone/routers/pages.dart';

import '../../../FileWidget/FileUploadWidget/FileUploadWidget.dart';
import 'ActivityMessageWidget/components/TargetActivityWidget/components/TargetActivityListWidget/components/ActivityCommentBoxWidget.dart';

///发布动态
class ActivityReleaseWidget extends XStatefulWidget<IActivity> {
  ActivityReleaseWidget({super.key});
  @override
  State<StatefulWidget> createState() => _ActivityReleaseState();
}

class _ActivityReleaseState
    extends XStatefulState<ActivityReleaseWidget, IActivity> {
  late TextEditingController _inputController;
  late TextEditingController _inputLinkController;
  late List<FileItemShare> _resources;

  @override
  void initState() {
    super.initState();
    _inputController = TextEditingController();
    _inputLinkController = TextEditingController();
    _resources = [];
  }

  @override
  Widget buildWidget(BuildContext context, IActivity data) {
    return Scrollbar(
        child: SingleChildScrollView(
            child: Container(
      padding: const EdgeInsets.only(left: 10, right: 10),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildInputBox(context, data),
          const Divider(),
          _buildInputLinkBox(context, data),
          const Divider(),
          _buildResource(context, data),
        ],
      ),
    )));
  }

  ///构建文本输入框
  Widget _buildInputBox(BuildContext context, IActivity data) {
    return TextField(
      key: GlobalKey(),
      controller: _inputController,
      scrollPhysics: const ScrollPhysics(),
      minLines: 5,
      maxLines: 10,
      decoration: const InputDecoration(
        border: InputBorder.none,
        hintText: '分享让世界更美好...', // 这里设置默认的提示文本
      ),
    );
  }

  Widget _buildInputLinkBox(BuildContext context, IActivity data) {
    return TextField(
      key: GlobalKey(),
      controller: _inputLinkController,
      scrollPhysics: const ScrollPhysics(),
      maxLines: 3,
      decoration: const InputDecoration(
        border: InputBorder.none,
        hintText: '请输入外部引用链接地址...',
      ),
    );
  }

  ///上传资源
  Widget _buildResource(BuildContext context, IActivity data) {
    return FileUploadWidget(
      allowMultiple: true,
      onSuccess: (ISysFileInfo file) {
        _resources.add(file.shareInfo());
      },
      onRemove: (sysFile) {
        _resources.remove(sysFile.shareInfo());
      },
    );
    // return GestureDetector(
    //   onTap: () {
    //     ToastUtils.showMsg(msg: "敬请期待");
    //   },
    //   child: Container(
    //       decoration:
    //           BoxDecoration(border: Border.all(color: Colors.grey, width: 1)),
    //       child: XImage.localImage(XImage.add, width: 80)),
    // );
  }

  @override
  List<Widget>? buildButtons(BuildContext context, IActivity data) {
    return [
      ElevatedButton(
        onPressed: () async {
          if (_inputController.text.isEmpty &&
              _inputLinkController.text.isEmpty &&
              _resources.isEmpty) {
            return ToastUtils.showMsg(msg: "请添加发布内容");
          }
          // LoadingDialog.showLoading(context, msg: "动态发表中");
          if (await _sendActivity(data)) {
            // LoadingDialog.dismiss(context);
            RoutePages.back(context);
          }
        },
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.blueAccent),
          minimumSize: MaterialStateProperty.all(Size(10.w, boxDefaultHeight)),
        ),
        child: const Text("发布"),
      ),
      // GestureDetector(
      //   onTap: () async {
      //     LoadingDialog.showLoading(context, msg: "评论发表中");
      //     if (await _sendActivity(data)) {
      //       LoadingDialog.dismiss(context);
      //       Get.back();
      //     }
      //   },
      //   child: Text(
      //     "发表",
      //     style: TextStyle(color: XColors.themeColor, fontSize: 20.sp),
      //   ),
      // )
    ];
  }

  Future<bool> _sendActivity(IActivity data) async {
    return await data.send(_inputController.text, MessageType.text, _resources,
        [], _inputLinkController.text);
  }
}
