import 'dart:convert';
import 'package:flutter/material.dart' hide ImageProvider;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:get/get.dart';
import 'package:orginone/components/XButton/XButton.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/components/XDialogs/dialog_utils.dart';
import 'package:orginone/components/FileWidget/FileUploadWidget/components/ResourceContainerWidget/ResourceContainerWidget.dart';
import 'package:orginone/config/theme/UIConfig.dart';
import 'package:orginone/components/TabContainerWidget/types.dart';
import 'package:orginone/components/MessageForwardWidget/MessageForwardWidget.dart';
import 'package:orginone/components/ChatSessionWidget/components/MessageListWidget/components/DetailItemWidget/DetailItemWidget.dart';
import 'package:orginone/components/EntityWidget/EntitySettingWidget/components/DissolutionWidget.dart';
import 'package:orginone/components/ActivityWidget/ActivityListWidget/components/ActivityMessageWidget/components/ListItemMetaWidget.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/model.dart' as model;
import 'package:orginone/dart/base/regex/regex_utils.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/dart/core/work/rules/lib/tools.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/pages.dart';
import 'package:orginone/routers/router_const.dart';
import '../../../../../dart/base/common/commands.dart';
import '../../../../ChatSessionWidget/components/MessageListWidget/components/DetailItemWidget/components/detail/TextDetailWidget.dart';
import '../../../ActivityMessageFromChatWidget/components/ActivityCommentWidget.dart';
import 'components/TargetActivityWidget/components/TargetActivityListWidget/components/ActivityCommentBoxWidget.dart';

//渲染动态信息
class ActivityMessageWidget extends StatefulWidget {
  //动态消息
  late Rx<IActivityMessage> item;
  late int currIndex;
  RxBool isDelete = false.obs;
  //动态
  IActivity activity;
  //动态元数据
  model.ActivityType? metadata;
  //隐藏点赞信息和回复信息，只显示统计数量
  bool hideResource;
  ActivityMessageWidget(
      {super.key,
      IActivityMessage? item,
      this.currIndex = -1,
      required this.activity,
      this.hideResource = false}) {
    if (currIndex >= 0 && activity.activityList.length > currIndex) {
      this.item = activity.activityList[currIndex].obs;
    } else if (null != item) {
      this.item = item.obs;
    }
    metadata = this.item.value.metadata;
    //订阅数据变更
    // this.item.value.unsubscribe();
    // this.item.value.subscribe((key, args) {
    //   this.item.refresh();
    // }, false);
  }
  @override
  State<StatefulWidget> createState() => _ActivityMessageState();
}

class _ActivityMessageState extends State<ActivityMessageWidget>
    with ImageProvider, UrlSpan {
  Rx<IActivityMessage> get item => widget.item;
  bool get hideResource => widget.hideResource;
  RxBool get isDelete => widget.isDelete;
  IActivity get activity => widget.activity;
  int get currIndex => widget.currIndex;
  //动态元数据
  model.ActivityType? get metadata => widget.metadata;

  late String _key;
  @override
  void initState() {
    super.initState();
    //订阅数据变更
    item.value.unsubscribe();
    _key = item.value.subscribe((key, args) {
      if (mounted) {
        setState(() {});
      }
    }, false);
  }

  @override
  void dispose() {
    super.dispose();
    item.value.unsubscribe(_key);
  }

  @override
  Widget build(BuildContext context) {
    Widget content = Container(
      // margin: EdgeInsets.only(top: hideResource ? 6.h : 0, left: 0, right: 0),
      color: Colors.white,
      padding: EdgeInsets.only(top: 12.h, bottom: 12.h),
      child: Offstage(
        offstage: isDelete.value,
        child: ListItemMetaWidget(
          title: title(),
          subTitle: subTitle(),
          avatar: avatar(),
          description: description(context),
          onTap: hideResource
              ? () {
                  activity.currIndex = currIndex;
                  // RoutePages.jumpActivityInfo(activity);
                  RoutePages.to(
                      context: context,
                      path: Routers.targetActivity,
                      data: activity);
                }
              : null,
          moreAction: hideResource ? Container() : moreActionWidget(),
        ),
      ),
    );
    // if (!hideResource) {
    //   content = EasyRefresh(
    //       // controller: state.refreshController,
    //       onRefresh: _onRefresh,
    //       onLoad: _onLoadMore,
    //       header: const MaterialHeader(),
    //       footer: const MaterialFooter(),
    //       child: RefreshIndicator(onRefresh: _onRefresh, child: content));
    // }
    if (!hideResource) {
      content = NotificationListener<ScrollNotification>(
        onNotification: (notification) {
          if (notification is ScrollStartNotification) {
            // 开始滚动
          } else if (notification is ScrollUpdateNotification) {
            // 正在滚动。。。总滚动距离：${notification.metrics.maxScrollExtent}"
          } else if (notification is ScrollEndNotification) {
            // activity.load();
            // "停止滚动"
          }
          return true;
        },
        child: content,
      );
    }
    return content;
  }

  showBottomModal(BuildContext context) {
    return AlertDialogUtils.buildBottomModal(
        context,
        [
          ActionModel(
              title: '举报',
              onTap: () {
                // Get.toNamed(Routers.complainPage)?.then((value) {
                //   Get.back();
                // });
                RoutePages.to(context: context, path: Routers.complainPage);
              }),
          if (item.value.canDelete)
            ActionModel(
                title: '删除动态',
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) => Center(
                          child: Material(
                              color: Colors.transparent,
                              child: ConfirmDialog(
                                  title: "确认删除动态",
                                  content: "删除后将无法恢复",
                                  confirmText: "删除",
                                  confirmFun: () async {
                                    await item.value.delete();
                                    isDelete.value = true;
                                    // await activity.load();
                                    // activity.activityList.remove(item);
                                    // command.emitter("activity", "activityrefresh", item);
                                    command.emitter('activity', 'refresh');
                                    activity.changeCallback();
                                    RoutePages.back(context);
                                  }))),
                      barrierColor: Colors.black.withOpacity(0.6));
                }),
        ],
        isNeedPop: false);
  }

  Widget moreActionWidget() {
    return GestureDetector(
      onTap: () {
        showBottomModal(context);
      },
      child: Container(
          width: 60.w,
          height: 40,
          alignment: Alignment.centerRight,
          decoration: BoxDecoration(
            color: XColors.white,
            borderRadius: BorderRadius.circular(6),
          ),
          child: XImage.localImage(XImage.moreAction,
              width: 26.w, color: XColors.black)),
    );
  }

  //渲染标题
  Widget title() {
    return Container(
      child: Row(
        children: [
          Text(item.value.activity.metadata.name,
              style: XFonts.activityListTitle),
          Padding(padding: EdgeInsets.only(left: 10.h)),
          if (metadata?.tags.isNotEmpty ?? false)
            ...metadata!.tags
                .map((e) => OutlinedButton(onPressed: () {}, child: Text(e)))
                .toList()
        ],
      ),
    );
  }

  Widget subTitle() {
    XEntity? entity = null != relationCtrl.user
        ? relationCtrl.user!
            .findMetadata<XEntity>(item.value.metadata.createUser!)
        : null;
    return Row(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          // padding: EdgeInsets.only(left: 5.w),
          child: Row(
            children: [
              Text(
                  "${showChatTime(item.value.metadata.createTime!)}·${entity?.name}",
                  style: XFonts.activityListSubTitle),
            ],
          ),
        )
      ],
    );
  }

  //渲染内容
  Widget? renderContent() {
    switch (MessageType.getType(metadata!.typeName)) {
      case MessageType.text:
        if (hideResource) {
          return Text(metadata!.content,
              style: XFonts.activitListContent,
              maxLines: 3,
              overflow: TextOverflow.ellipsis);
        } else {
          return getUrlSpan(metadata!.content) ??
              Text(
                metadata!.content,
                style: const TextStyle(height: 1.5)
                    .merge(XFonts.activitListContent),
              );
        }
      case MessageType.html:
        if (hideResource) {
          return (Offstage(
            offstage: !hideResource,
            child: Text(parseHtmlToText(metadata!.content),
                style: const TextStyle(height: 1.5)
                    .merge(XFonts.activitListContent),
                maxLines: 3,
                overflow: TextOverflow.ellipsis),
          ));
        } else {
          return HtmlWidget(
            metadata!.content,
            textStyle: TextStyle(fontSize: 24.sp, height: 1.8),
            onTapUrl: (url) {
              RoutePages.jumpWeb(url: url);
              return true;
            },
            onTapImage: (url) {
              print(">>>>>>>>>$url");
            },
          );
        }
      default:
    }
    return null;
  }

  //渲染头像
  Widget avatar() {
    // return CircleAvatar(
    //   backgroundImage: AssetImage(activity.typeName),
    // );
    return XImage.entityIcon(item.value.activity.metadata,
        width: 35, radius: 4);
  }

  Widget renderLink() {
    return Container(
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.1),
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        padding: const EdgeInsets.all(12),
        child: PreViewUrl(
          url: metadata!.linkInfo,
          width: UIConfig.screenWidth - 32,
        ));
  }

  //渲染描述
  Widget description(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(top: 10.h, bottom: 12.h),
            child: renderContent() ?? Container(),
          ),
          Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(top: 10.h, bottom: 12.h),
              child: metadata?.linkInfo != null &&
                      RegexUtils.isURL(metadata!.linkInfo)
                  ? renderLink()
                  : Container()),
          if (null != metadata && metadata!.resource.isNotEmpty)
            Container(
              margin: const EdgeInsets.only(bottom: 16),
              child: ResourceContainerWidget(metadata!.resource, 100,
                  hideResource: hideResource),
            ),
          RenderCtxMore(
            activity: activity,
            item: item,
            hideResource: hideResource,
            isDelete: isDelete,
          )
        ],
      ),
    );
  }
}

//渲染动态属性信息
class RenderCtxMore extends StatelessWidget {
  Rx<IActivityMessage> item;
  IActivity activity;
  RxBool isDelete;
  bool hideResource;
  late XEntity? replyTo;
  bool commenting = false;

  RenderCtxMore(
      {super.key,
      required this.activity,
      required this.item,
      required this.hideResource,
      required this.isDelete});

  @override
  Widget build(BuildContext context) {
    if (hideResource) {
      return renderTags();
    }
    return Column(children: [
      renderOperate(context),
      Offstage(
        offstage: item.value.metadata.likes.isEmpty &&
            item.value.metadata.comments.isEmpty,
        child: _buildLikeBoxWidget(),
      ),
      Padding(padding: EdgeInsets.only(left: 5.w)),
      _buildCommentBoxWidget(context)
    ]);
  }

  //判断是否有回复
  Future<void> handleReply(BuildContext context, [String userId = '']) async {
    replyTo = null;
    if (userId.isNotEmpty) {
      var user = await relationCtrl.user?.findEntityAsync(userId);
      replyTo = user;
    }
    // ignore: use_build_context_synchronously
    Future.delayed(Duration.zero, () {
      ShowCommentBoxNotification((text) async {
        return await item.value.comment(text, replyTo: replyTo?.id);
      },
          getTipInfo: replyTo != null
              ? () {
            return "回复${replyTo?.name}：";
          }
              : null)
          .dispatch(context);
    });
  }

  //渲染操作
  Widget renderOperate(BuildContext context) {
    return Container(child: Obx(() {
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              if (item.value.metadata.likes.contains(relationCtrl.user?.id))
                XButton.iconText(
                  text: '取消',
                  onPressed: () async {
                    await item.value.like();
                  },
                  icon: XImage.localImage(XImage.likeFill,
                      width: 22/*, color: Colors.red*/),
                  // textColor: XColors.black3,
                ),
              if (!item.value.metadata.likes.contains(relationCtrl.user?.id))
                XButton.iconText(
                  onPressed: () async {
                    await item.value.like();
                  },
                  // const ImageWidget(
                  //   AssetsImages.iconLike,
                  //   size: 18,
                  // ),
                  icon: XImage.localImage(XImage.likeOutline, width: 22),
                  text: '赞',
                  // textColor: XColors.black3,
                ),
              // Padding(padding: EdgeInsets.only(left: 5.w)),
              XButton.iconText(
                onPressed: () async {
                  handleReply(context);
                },
                // const ImageWidget(
                //   AssetsImages.iconMsg,
                //   size: 18,
                // ),
                icon: XImage.localImage(XImage.commentOutline, width: 22),
                text: '评论',
                // textColor: XColors.black3,
              ),
              XButton.iconText(
                onPressed: () async {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return MessageForwardWidget(
                          msgBody: jsonEncode(item.value.metadata.toJson()),
                          msgType: MessageType.activity.label,
                          onSuccess: () {
                            Navigator.pop(context);
                          },
                        );
                      },
                      isScrollControlled: true,
                      isDismissible: false,
                      useSafeArea: true,
                      barrierColor: Colors.white);
                },
                // const ImageWidget(
                //   AssetsImages.iconMsg,
                //   size: 18,
                // ),
                icon: XImage.localImage(XImage.forward, width: 22),
                text: '转发',
                // textColor: XColors.black3,
              ),
              XButton.iconText(
                onPressed: () async {
                  bool res =
                      await item.value.collection(relationCtrl.user!.userId);
                  if (res) {
                    updateCollectionsList();
                  }
                },
                icon: XImage.localImage(XImage.coll,
                    color: item.value.metadata.collections != null &&
                            item.value.metadata.collections!
                                .contains(relationCtrl.user?.userId)
                        ? XColors.red
                        : XColors.black3,
                    width: 22),
                text: item.value.metadata.collections != null &&
                        item.value.metadata.collections!.isNotEmpty
                    ? '${item.value.metadata.collections!.length}'
                    : '收藏',
                // textColor: XColors.black3,
              )
            ],
          )
        ],
      );
    }));
  }

  updateCollectionsList() async {
    List<model.ActivityType> list = relationCtrl.user!.collectionsList;
    model.ActivityType? result = list
        .firstWhereOrNull((element) => element.id == item.value.metadata.id);
    if (result != null) {
      list.removeWhere((element) => element.id == item.value.metadata.id);
    } else {
      list.add(item.value.metadata);
    }
    await relationCtrl.user!.updateCollections(list);
  }

  ///渲染点赞信息
  Widget _buildLikeBoxWidget() {
    return Container(
      alignment: Alignment.centerLeft,
      color: XColors.bgListItem1,
      padding: EdgeInsets.all(5.w),
      child: Wrap(
        direction: Axis.horizontal,
        crossAxisAlignment: WrapCrossAlignment.center,
        runSpacing: 5,
        spacing: 1,
        children: [
          // const ImageWidget(AssetsImages.iconLike,
          //     size: 18, color: Colors.red),
          XImage.localImage(XImage.likeFill, width: 24.w/*, color: Colors.red*/),
          for (var e in item.value.metadata.likes.toSet().toList())
            ...getUserAvatar(e)
        ],
      ),
    );
  }

  ///渲染评论信息
  Widget _buildCommentBoxWidget(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      color: XColors.white,
      margin: EdgeInsets.only(top: 5.w),
      padding: EdgeInsets.all(5.w),
      child: Wrap(
        direction: Axis.horizontal,
        runSpacing: 5,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 5.h, bottom: 5.h),
            child: Text("全部评论 ${item.value.metadata.comments.length}",
                style: const TextStyle(
                  fontSize: 16,
                  color: XColors.black,
                  fontWeight: FontWeight.bold,
                )),
          ),
          // if (item.value.metadata.comments.isEmpty) EmptyComments(title: "暂无评论"),
          if (item.value.metadata.comments.isNotEmpty)
            ...item.value.metadata.comments.map((e) => ActivityCommentWidget(
                comment: e,
                item: item,
                onTap: (comment) => handleReply(context, comment.userId)))
        ],
      ),
    );
  }

  //渲染发布者信息
  Widget renderTags() {
    XEntity? entity = null != relationCtrl.user
        ? relationCtrl.user!
            .findMetadata<XEntity>(item.value.metadata.createUser!)
        : null;
    var showLikes = item.value.metadata.likes.isEmpty &&
        item.value.metadata.comments.isEmpty;
    return Column(
      children: [
        // Padding(padding: EdgeInsets.only(top: 5.h)),
        Offstage(
          offstage: false, //showLikes,
          child: Container(
              // padding: EdgeInsets.all(5.w),
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  // const ImageWidget(AssetsImages.iconLike,
                  //     size: 18, color: Colors.red),
                  XImage.localImage(XImage.likeOutline,
                      width: 22,
                      color: (item.value.metadata.likes.isNotEmpty &&
                              item.value.metadata.likes
                                  .contains(relationCtrl.user?.id))
                          ? XColors.primary
                          : XColors.black3),
                  Container(
                      padding: EdgeInsets.only(left: 6.w),
                      child: Text(
                        "${item.value.metadata.likes.isEmpty ? '点赞' : item.value.metadata.likes.toSet().length}",
                        style: XFonts.size20Black3,
                      ))
                ],
              ),
              Row(
                children: [
                  // const ImageWidget(AssetsImages.iconMsg, size: 18),
                  XImage.localImage(XImage.commentOutline, width: 22),
                  Container(
                      padding: EdgeInsets.only(left: 6.w),
                      child: Text(
                        "${item.value.metadata.comments.isEmpty ? '评论' : item.value.metadata.comments.length}",
                        style: XFonts.size20Black3,
                      ))
                ],
              ),
              Row(
                children: [
                  // const ImageWidget(Icons.forward_5, size: 18),
                  XImage.localImage(XImage.forward, width: 22),
                  Container(
                      padding: EdgeInsets.only(left: 6.w),
                      child: Text(
                        "转发",
                        style: XFonts.size20Black3,
                      ))
                ],
              ),
              Row(
                children: [
                  XImage.localImage(XImage.coll,
                      width: 22,
                      color: item.value.metadata.collections != null &&
                              item.value.metadata.collections!
                                  .contains(relationCtrl.user?.id)
                          ? XColors.red
                          : XColors.black3),
                  Container(
                      padding: EdgeInsets.only(left: 6.w),
                      child: Text(
                        "${item.value.metadata.collections == null || item.value.metadata.collections!.isEmpty ? '收藏' : item.value.metadata.collections!.length}",
                        style: XFonts.size20Black3,
                      ))
                ],
              ),
            ],
          )),
        )
      ],
    );
  }

  List<Widget> getUserAvatar(String userId) {
    dynamic entity = relationCtrl.user?.findMetadata(userId);
    return [
      Padding(padding: EdgeInsets.only(left: 5.w)),
      // TeamAvatar(
      //   info: TeamTypeInfo(userId: userId),
      //   size: 24.w,
      // ),
      XImage.entityIcon(entity, width: 24.w),
      Padding(padding: EdgeInsets.only(left: 5.w)),
      Text(
        entity?.name ?? "",
      )
    ];
  }
}
