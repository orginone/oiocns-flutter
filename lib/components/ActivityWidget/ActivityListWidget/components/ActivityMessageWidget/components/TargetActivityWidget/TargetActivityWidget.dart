import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:getwidget/components/button/gf_icon_button.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import 'package:orginone/routers/pages.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'components/TargetActivityListWidget/components/ActivityCommentBoxWidget.dart';
import 'components/TargetActivityListWidget/TargetActivityListWidget.dart';

class TargetActivityWidget extends StatefulWidget {
  const TargetActivityWidget({super.key});

  @override
  State<TargetActivityWidget> createState() => _TargetActivityWidgetState();
}

class _TargetActivityWidgetState extends State<TargetActivityWidget> {
  late IActivity activity;
  late IActivityMessage activityMessage;

  late GlobalKey scrollKey;

  final ItemScrollController itemScrollController = ItemScrollController();
  bool showCommentBox = false;
  int activityCount = 1;
  bool isRefreshData = false;

  refresh() async {}

  @override
  void initState() {
    dynamic params = RoutePages.getRouteParams();
    if (params is ActivityMessage) {
      activityMessage = params;
      activity = activityMessage.activity;
    } else if (params is IActivity) {
      activity = params;
      activityCount = activity.activityList.length;
    }
    scrollKey = GlobalKey();
    activity.subscribe((key, args) {
      isRefreshData = !isRefreshData;
    }, false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return XScaffold(
      titleWidget: _title(),
      actions: _actions(),
      body: ActivityCommentBoxWidget(
        body: TargetActivityListWidget(),
      ),
    );
  }

  Widget _title() {
    return Column(
      children: [
        Text(activity.name ?? "", style: XFonts.size24Black3),
      ],
    );
  }

  List<Widget> _actions() {
    return <Widget>[
      GFIconButton(
        color: Colors.white.withOpacity(0),
        icon: Icon(
          Icons.more_vert,
          color: XColors.black3,
          size: 32.w,
        ),
        onPressed: () async {
          // Get.toNamed(Routers.messageSetting, arguments: state.chat);
        },
      ),
    ];
  }
}
