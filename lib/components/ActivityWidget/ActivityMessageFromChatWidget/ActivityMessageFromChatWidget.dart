import 'package:flutter/material.dart' hide ImageProvider;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import 'package:orginone/components/ActivityWidget/ActivityMessageFromChatWidget/components/ActivityCommentWidget.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/model.dart' as model;
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/dart/core/work/rules/lib/tools.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/pages.dart';

import '../../../config/theme/UIConfig.dart';
import '../../../dart/base/regex/regex_utils.dart';
import '../../ChatSessionWidget/components/MessageListWidget/components/DetailItemWidget/DetailItemWidget.dart';
import '../../ChatSessionWidget/components/MessageListWidget/components/DetailItemWidget/components/detail/TextDetailWidget.dart';
import '../../FileWidget/FileUploadWidget/components/ResourceContainerWidget/ResourceContainerWidget.dart';

//渲染动态信息
// ignore: must_be_immutable
class ActivityMessageFromChatWidget extends StatefulWidget {
  //动态元数据
  model.ActivityType metadata;
  bool isHideLikesAndComment;

  ActivityMessageFromChatWidget(
      {super.key, required this.metadata, this.isHideLikesAndComment = false}) {
    metadata = metadata;
  }
  @override
  State<StatefulWidget> createState() => _ActivityMessageFromChatState();
}

class _ActivityMessageFromChatState extends State<ActivityMessageFromChatWidget>
    with ImageProvider, UrlSpan {
  //动态元数据
  model.ActivityType get metadata => widget.metadata;
  bool get isHideLikesAndComment => widget.isHideLikesAndComment;
  dynamic entity;
  @override
  void initState() {
    super.initState();
    initData();
  }

  initData() async {
    ISession? sessionUser =
        relationCtrl.user?.findMemberChat(metadata.updateUser!);
    if (sessionUser != null) {
      IActivity iActivity = Activity(sessionUser.metadata, sessionUser);
      await iActivity.load();
    }

    entity = null != relationCtrl.user
        ? relationCtrl.user!.findMetadata(metadata.shareId!)
        : null;

    // if (entity == null) {
    //   List<XTarget> list =
    //       await relationCtrl.user!.searchTargets(metadata.shareId!, [
    //     TargetType.person.label,
    //     TargetType.cohort.label,
    //     TargetType.group.label,
    //     TargetType.company.label
    //   ]);
    //   if (list.isNotEmpty) {
    //     entity = list[0];
    //   }
    // }
    entity ??= null != relationCtrl.user
        ? relationCtrl.user!.findMetadata(metadata.createUser!)
        : null;
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget content = XScaffold(
        backgroundColor: Colors.white,
        titleWidget: Text("动态详情", style: XFonts.size24Black3),
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.only(top: 12.w, bottom: 12.h),
          child: isHideLikesAndComment
              ? Container(
                  padding: EdgeInsets.only(left: 20.w, right: 20.w),
                  color: Colors.white,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          avatar(),
                          Padding(
                              padding: EdgeInsets.only(left: 10.w),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [title(), subTitle()]))
                        ],
                      ),
                      description(context),
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    color: Colors.white,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            avatar(),
                            Padding(
                                padding: EdgeInsets.only(left: 10.w),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [title(), subTitle()]))
                          ],
                        ),
                        description(context),
                      ],
                    ),
                  ),
                ),
        ));
    return content;
  }

  //渲染标题
  Widget title() {
    return Container(
      child: Row(
        children: [
          Text(entity?.name ?? "", style: XFonts.activityListTitle),
          Padding(padding: EdgeInsets.only(left: 10.h)),
          if (metadata.tags.isNotEmpty)
            ...metadata.tags
                .map((e) => OutlinedButton(onPressed: () {}, child: Text(e)))
                .toList()
        ],
      ),
    );
  }

  Widget subTitle() {
    return Row(
      children: [
        // TeamAvatar(
        //   info: TeamTypeInfo(userId: item.value.metadata.createUser!),
        //   size: 24.w,
        // ),
        Container(
          alignment: Alignment.centerLeft,
          // padding: EdgeInsets.only(left: 5.w),
          child: Row(
            children: [
              Text("${showChatTime(metadata.createTime!)}·${entity?.name}",
                  style: XFonts.activityListSubTitle),
            ],
          ),
        )
      ],
    );
  }

  //渲染内容
  Widget? renderContent() {
    switch (MessageType.getType(metadata.typeName)) {
      case MessageType.text:
        if (isHideLikesAndComment) {
          return Text(metadata.content,
              style: XFonts.activitListContent,
              maxLines: 3,
              overflow: TextOverflow.ellipsis);
        } else {
          return getUrlSpan(metadata.content) ??
              Text(
                metadata.content,
                style: const TextStyle(height: 1.5)
                    .merge(XFonts.activitListContent),
              );
        }

      case MessageType.html:
        if (isHideLikesAndComment) {
          return (Offstage(
            offstage: !isHideLikesAndComment,
            child: Text(parseHtmlToText(metadata.content),
                style: const TextStyle(height: 1.5)
                    .merge(XFonts.activitListContent),
                maxLines: 3,
                overflow: TextOverflow.ellipsis),
          ));
        } else {
          return HtmlWidget(
            metadata.content,
            textStyle: TextStyle(fontSize: 24.sp, height: 1.8),
            onTapUrl: (url) {
              RoutePages.jumpWeb(url: url);
              return true;
            },
            onTapImage: (url) {
              print(">>>>>>>>>$url");
            },
          );
        }

      default:
    }
    return null;
  }

  //渲染头像
  Widget avatar() {
    return XImage.entityIcon(entity, size: Size(60.w, 60.w), radius: 5.w);
    // return TeamAvatar(
    //   info: TeamTypeInfo(userId: metadata.createUser!),
    //   size: 35,
    //   radius: 4,
    // );
  }

  //渲染描述
  Widget description(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(top: 10.h, bottom: 12.h),
            child: renderContent() ?? Container(),
          ),
          Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(top: 10.h, bottom: 12.h),
              child: metadata?.linkInfo != null &&
                  RegexUtils.isURL(metadata!.linkInfo)
                  ? renderLink()
                  : Container()),
          if (null != metadata && metadata!.resource.isNotEmpty)
            Container(
              margin: const EdgeInsets.only(bottom: 16),
              child: ResourceContainerWidget(metadata!.resource, 100,
                  hideResource: false),
            ),
        ],
      ),
    );
  }

  Widget renderLink() {
    return Container(
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.1),
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        padding: const EdgeInsets.all(12),
        child: PreViewUrl(
          url: metadata!.linkInfo,
          width: UIConfig.screenWidth - 32,
        ));
  }
}

//渲染动态属性信息
// ignore: must_be_immutable
class RenderCtxMore extends StatelessWidget {
  model.ActivityType metadata;
  late XEntity? replyTo;

  RenderCtxMore({super.key, required this.metadata});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      // renderOperate(context),
      Offstage(
        offstage: metadata.likes.isEmpty,
        child: _buildLikeBoxWidget(),
      ),
      Padding(padding: EdgeInsets.only(left: 5.w)),
      Offstage(
        offstage: metadata.comments.isEmpty,
        child: _buildCommentBoxWidget(context),
      )
    ]);
  }

  //判断是否有回复
  Future<void> handleReply(BuildContext context, [String userId = '']) async {
    replyTo = null;
    if (userId.isNotEmpty) {
      var user = await relationCtrl.user?.findEntityAsync(userId);
      replyTo = user;
    }
  }

  ///渲染点赞信息
  Widget _buildLikeBoxWidget() {
    return Container(
      alignment: Alignment.centerLeft,
      color: XColors.bgListItem1,
      padding: EdgeInsets.all(5.w),
      key: UniqueKey(),
      child: Wrap(
        key: UniqueKey(),
        direction: Axis.horizontal,
        crossAxisAlignment: WrapCrossAlignment.center,
        runSpacing: 5,
        spacing: 1,
        children: [
          // const ImageWidget(AssetsImages.iconLike,
          //     size: 18, color: Colors.red),
          XImage.localImage(XImage.likeFill, width: 24.w, color: Colors.red),
          for (var e in metadata.likes.toSet().toList())
            ...getUserAvatar(e)
        ],
      ),
    );
  }

  ///渲染评论信息
  Widget _buildCommentBoxWidget(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      color: XColors.white,
      margin: EdgeInsets.only(top: 5.w),
      padding: EdgeInsets.all(5.w),
      child: Wrap(
        direction: Axis.horizontal,
        runSpacing: 5,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 5.h, bottom: 5.h),
            child: Text("全部评论 ${metadata.comments.length}",
                style: const TextStyle(
                  fontSize: 16,
                  color: XColors.black,
                  fontWeight: FontWeight.bold,
                )),
          ),
          ...metadata.comments.map((e) => ActivityCommentWidget(
              comment: e,
              onTap: (comment) => handleReply(context, comment.userId)))
        ],
      ),
    );
  }

  List<Widget> getUserAvatar(String userId) {
    XEntity? entity = relationCtrl.user?.findMetadata<XEntity>(userId);
    return [
      Padding(padding: EdgeInsets.only(left: 5.w)),
      // TeamAvatar(
      //   info: TeamTypeInfo(userId: userId),
      //   size: 24.w,
      // ),
      XImage.entityIcon(entity, width: 24.w),
      Padding(padding: EdgeInsets.only(left: 5.w)),
      Text(
        entity?.name ?? "",
      )
    ];
  }
}
