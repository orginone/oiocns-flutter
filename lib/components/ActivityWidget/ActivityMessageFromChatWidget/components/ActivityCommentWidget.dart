import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import 'package:orginone/dart/core/work/rules/lib/tools.dart';
import 'package:orginone/main.dart';
import 'package:flutter/src/widgets/basic.dart' as basic;
import 'package:orginone/routers/pages.dart';
import '../../../XButton/XButton.dart';
import '../../../XDialogs/XDialog.dart';

//动态评论
class ActivityCommentWidget extends StatelessWidget {
  late CommentType comment;
  late Rx<IActivityMessage>? item;
  late bool isShowOperationForComment;

  late void Function(CommentType comment)? onTap;

  ActivityCommentWidget(
      {required this.comment,
      this.item,
      this.onTap,
      this.isShowOperationForComment = true,
      super.key});

  @override
  Widget build(BuildContext context) {
    List<Widget> replyTo = [];
    if (comment.replyTo != null && comment.replyTo!.isNotEmpty) {
      replyTo
        ..add(const Text("回复 "))
        ..addAll(getUserMsgForReply(comment.replyTo!))
        ..add(const Text(" : "));
    }
    //getUserAvatar(comment.replyTo!)
    return GestureDetector(
        onTap: () => onTap?.call(comment),
        child: Container(
            alignment: Alignment.centerLeft,
            // padding: const EdgeInsets.only(top: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                getUserAvatar(context, comment.userId),
                Expanded(
                  child: basic.Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(children: [
                          getUserName(comment.userId),
                          const SizedBox(width: 5),
                          // getCreateTime(comment.userId),
                        ]),
                        SizedBox(
                          height: 4.h,
                        ),
                        Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            ...replyTo,
                            const SizedBox(height: 4),
                            Text(comment.label,
                                textAlign: TextAlign.start,
                                style: XFonts.activitListContent)
                          ],
                        ),
                        // Row(children: [
                        //   ...replyTo,
                        // ]),
                        // const SizedBox(height: 4),
                        // Text(
                        //   comment.label,
                        //   textAlign: TextAlign.start,
                        //   style: const TextStyle(
                        //       fontSize: 17,
                        //       color: XColors.black,
                        //       fontWeight: FontWeight.normal),
                        // )
                        if (isShowOperationForComment)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              getCreateTime(comment.userId),
                              Row(
                                children: [
                                  if (comment.userId ==
                                      relationCtrl.user!.userId)
                                    XButton.iconText(
                                      onPressed: () async {
                                        XDialog.showConfirmDialog(
                                            title: "确认删除评论",
                                            content: "删除后将无法恢复",
                                            confirmText: "删除",
                                            confirmFun: () async {
                                              await item?.value
                                                  .removeComment(comment);
                                            });
                                        // showDialog(
                                        //     context: context,
                                        //     builder: (context) => Center(
                                        //         child: Material(
                                        //             color: Colors.transparent,
                                        //             child: ConfirmDialog(
                                        //                 title: "确认删除评论",
                                        //                 content: "删除后将无法恢复",
                                        //                 confirmText: "删除",
                                        //                 confirmFun: () async {
                                        //                   await item?.value
                                        //                       .removeComment(
                                        //                           comment);
                                        //                 }))),
                                        //     barrierColor:
                                        //         Colors.black.withOpacity(0.6));
                                      },
                                      icon: XImage.localImage(
                                          XImage.deleteOutline,
                                          width: 18),
                                      text: '删除',
                                      // textColor: XColors.black3,
                                    ),
                                  const SizedBox(width: 16),
                                  XButton.iconText(
                                    onPressed: () async {
                                      if (comment.likes != null) {
                                        if (comment.likes!.contains(
                                            relationCtrl.user?.userId)) {
                                          comment.likes!.remove(
                                              relationCtrl.user?.userId);
                                        } else {
                                          comment.likes!
                                              .add(relationCtrl.user!.userId);
                                        }
                                      } else {
                                        comment.likes ??= [
                                          relationCtrl.user!.userId
                                        ];
                                      }
                                      await item?.value.likeOneComment(comment);
                                    },
                                    icon: XImage.localImage(XImage.likeOutline,
                                        width: 18,
                                        color: comment.likes != null &&
                                                comment.likes!.isNotEmpty &&
                                                comment.likes!.contains(
                                                    relationCtrl.user?.userId)
                                            ? XColors.red
                                            : XColors.black3),
                                    text:
                                        "${comment.likes == null || comment.likes!.isEmpty ? '点赞' : comment.likes!.length}",
                                    // textColor: XColors.black3,
                                  ),
                                ],
                              )
                            ],
                          )
                      ]),
                )
              ],
            )));
  }

  Widget getUserAvatar(BuildContext context, String userId) {
    XEntity? entity = relationCtrl.user?.findMetadata<XEntity>(userId);
    return GestureDetector(
      onTap: () {
        if (entity != null) RoutePages.jumpEneityInfo(context, data: entity);
      },
      child: Padding(
        padding: const EdgeInsets.only(right: 10),
        child: XImage.entityIcon(entity,
            width: 40
                .w) /* TeamAvatar(
          info: TeamTypeInfo(userId: userId),
          size: 40.w,
        ) */
        ,
      ),
    );
  }

  Widget getUserName(String userId) {
    XEntity? entity = relationCtrl.user?.findMetadata<XEntity>(userId);

    return Text(entity?.name ?? "",
        style: const TextStyle(
          color: XColors.black3,
          fontSize: 14,
          fontWeight: FontWeight.normal,
        ));
  }

  Widget getCreateTime(String userId) {
    XEntity? entity = relationCtrl.user?.findMetadata<XEntity>(userId);
    return Text(showChatTime(comment.time),
        style: const TextStyle(
          color: XColors.black3,
          fontSize: 14,
          fontWeight: FontWeight.normal,
        ));
  }

  List<Widget> getUserMsgForReply(String userId) {
    XEntity? entity = relationCtrl.user?.findMetadata<XEntity>(userId);
    return [
      Padding(padding: EdgeInsets.only(left: 5.w)),
      // TeamAvatar(
      //   info: TeamTypeInfo(userId: userId),
      //   size: 24.w,
      // ),
      XImage.entityIcon(entity, width: 24.w),
      Padding(padding: EdgeInsets.only(left: 5.w)),
      Text(entity?.name ?? "",
          style: const TextStyle(
            fontWeight: FontWeight.bold,
          ))
    ];
  }
}
