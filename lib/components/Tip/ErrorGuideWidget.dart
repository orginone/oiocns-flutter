import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/dart/extension/index.dart';

import '../../config/theme/unified_style.dart';
import '../../main.dart';
import '../../routers/pages.dart';
import '../../routers/router_const.dart';
import '../BeautifulBGWidget/BeautifulBGWidget.dart';
import '../TextKeyWidget/TextKeyWidget.dart';
import '../XText/XText.dart';

///异常引导页面
class ErrorGuideWidget extends BeautifulBGStatelessWidget {
  ErrorGuideWidget({super.key});

  @override
  Widget buildWidget(BuildContext context) {
    return topAndBottomLayout(context,
        showBack: false,
        top: kernel.isOnline
            ? _buildLoadingError(context)
            : _buildNetworkError(context),
        bottom: Container());
        // bottom: _buildRetry(context));
  }

  Widget _buildLoadingError(BuildContext context) {
    return Center(
      child: <Widget>[
        const SizedBox(
          height: 200,
        ),
        const XText.listTitle('应用加载异常，请重试')
      ].toColumn().alignCenter(),
    );
  }

  Widget _buildNetworkError(BuildContext context) {
    return Center(
      child: <Widget>[
        const SizedBox(
          height: 200,
        ),
        TextKeyWidget(
          data: "网络异常，加载失败请重试",
          keys: const ['重试'],
          style: TextStyle(color: Colors.black, fontSize: 20.sp),
          keyStyle: TextStyle(color: XColors.themeColor, fontSize: 20.sp, decoration: TextDecoration.underline,),
          onTapCallback: (String key) {
            if (key == '重试') {
                relationCtrl.appStartController.changeCallback();
                RoutePages.to(context: context, path: Routers.logintrans);
            }
          },
        ),
        // const XText.listTitle('网络异常，加载失败请')
      ].toColumn().alignCenter(),
    );
  }

  // Widget _buildRetry(BuildContext context) {
  //   return Column(
  //     children: [
  //       XButton.solidMin('重试', padding: 100, onPressed: () {
  //         relationCtrl.appStartController.changeCallback();
  //         RoutePages.to(context: context, path: Routers.logintrans);
  //       }),
  //       const SizedBox(
  //         height: 30,
  //       )
  //     ],
  //   );
  // }
}
