import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/CommandWidget/index.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/main.dart';

///网络提醒
class NetworkTipWidget extends StatefulWidget {
  const NetworkTipWidget({super.key});

  @override
  State<StatefulWidget> createState() => _NetworkTipState();
}

class _NetworkTipState extends State<NetworkTipWidget> {
  bool showNetworkTip = false;

  @override
  void initState() {
    super.initState();
    // relationCtrl.appStartController.subscribe((key, args) {
    //   if (mounted) {
    //     setState(() {});
    //   } else {
    //     showNetworkTip = !relationCtrl.appStartController.isNetworkConnected;
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return CommandWidget(
      type: 'network',
      cmd: 'refresh',
      builder: (context, args) {
        showNetworkTip = !relationCtrl.appStartController.isNetworkConnected;
        return Offstage(
          offstage: !showNetworkTip,
          child: Container(
            alignment: Alignment.center,
            color: XColors.bgErrorColor,
            padding: EdgeInsets.all(8.w),
            height: 30,
            child: const Row(
              children: [
                Icon(Icons.error, size: 18, color: XColors.fontErrorColor),
                SizedBox(width: 18),
                Text("当前无法连接网络，可检查网络设置是否正常。",
                    style: TextStyle(color: XColors.black666))
              ],
            ),
          ),
        );
      },
    );
  }
}
