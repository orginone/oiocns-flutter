import 'package:flutter/material.dart';
import 'package:orginone/components/Tip/ToastUtils.dart';
import 'package:orginone/routers/pages.dart';

/// 退出APP提示
class QuitAppTipWidget extends StatelessWidget {
  Widget child;
  DateTime? lastCloseApp;

  QuitAppTipWidget({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (!RoutePages.isHomePage()) {
            RoutePages.back(context);
            return false;
          } else if ((lastCloseApp == null ||
              DateTime.now().difference(lastCloseApp!) >
                  const Duration(seconds: 1))) {
            lastCloseApp = DateTime.now();
            ToastUtils.showMsg(msg: '再按一次退出');
            return false;
          }
          return true;
        },
        child: child);
  }
}
