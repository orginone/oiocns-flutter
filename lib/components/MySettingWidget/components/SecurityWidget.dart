import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import 'package:orginone/dart/core/target/team/company.dart';
import 'package:orginone/main.dart';

class SecurityWidget extends StatefulWidget {
  const SecurityWidget({Key? key}) : super(key: key);

  @override
  State<SecurityWidget> createState() => _SecurityWidgetState();
}

class _SecurityWidgetState extends State<SecurityWidget>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return XScaffold(
        backgroundColor: Colors.white,
        titleName: '安全',
        body: SafeArea(
          child: _buildView(),
        ));
  }

  // 主视图
  Widget _buildView() {
    int cont = 0;
    for (ICompany company in relationCtrl.provider.user?.companys ?? []) {
      cont += company.chats.length;
    }
    // List<Widget> widgetList = [
    //   Row(
    //     children: [
    //       Text("单位数据：成功加载${relationCtrl.provider.user?.companys.length}个"),
    //     ],
    //   ),
    //   Row(
    //     children: [
    //       Text("群数据：成功加载${relationCtrl.provider.user?.cohorts.length}个"),
    //     ],
    //   ),
    //   Row(
    //     children: [
    //       Text("群会话数据：成功加载${relationCtrl.provider.user?.cohortChats.length}个")
    //     ],
    //   ),
    //   Row(
    //     children: [Text("全部消息列表：成功加载${relationCtrl.chats.length}个")],
    //   ),
    //   Row(
    //     children: [
    //       Text("人员消息列表：成功加载${relationCtrl.provider.user?.chats.length}个")
    //     ],
    //   ),
    //   Row(
    //     children: [Text("单位消息列表：成功加载$cont个")],
    //   )
    // ];
    if (relationCtrl.provider.errInfo != "") {
      TextEditingController controller = TextEditingController();
      controller.text = relationCtrl.provider.errInfo;
      return Container(
          child: Column(children: [
        Scrollbar(
            child: SingleChildScrollView(
          child: TextField(maxLines: null, controller: controller),
        )),
      ])); //_buildView(),
    } else {
      return Center(
        child: Column(children: [
          Image.asset(
            XImage.empty,
            width: 280.w,
            height: 280.w,
          ),
          const Text.rich(TextSpan(text: '暂无内容'))
        ]),
      );
    }
  }
}
