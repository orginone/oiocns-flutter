import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orginone/components/EmptyWidget/EmptyWidget.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import 'package:orginone/components/EmptyWidget/EmptyActivity.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import 'package:orginone/dart/core/provider/session.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/pages.dart';

import '../../../routers/router_const.dart';
import '../../ActivityWidget/ActivityMessageFromChatWidget/ActivityMessageFromChatWidget.dart';
import '../../XLazy/XLazy.dart';

class MyCollectionWidget extends StatefulWidget {
  const MyCollectionWidget({Key? key}) : super(key: key);

  @override
  State<MyCollectionWidget> createState() => _MyCollectionWidgetState();
}

class _MyCollectionWidgetState extends State<MyCollectionWidget> {
  List<ActivityType> listColl = [];
  final ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() async {
    listColl = relationCtrl.user?.collectionsList ?? [];
    listColl = listColl.reversed.toList();
  }

  @override
  Widget build(BuildContext context) {
    return XScaffold(
        titleName: '收藏',
        body: Container(
          decoration: const BoxDecoration(
              color: XColors.white,
              border: Border(top: BorderSide(color: XColors.dividerLineColor))),
          child: Container(
            color: XColors.white,
            child: XLazy<ActivityType>(
              initDatas: listColl,
              onInitLoad:  () async {
                return listColl;
              },
              onLoad: () async {
                loadData();
                return listColl;
              },
              scrollController: _scrollController,
              builder: (context, datas) {
                return (listColl.isEmpty)
                    ? EmptyWidget()
                    : ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return buildActivityItem(listColl[index]);
                  },
                  itemCount: listColl.length,
                );
              },
            ),
          ),
        ));
  }

  jumpToActivityMsg(ActivityType item) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ActivityMessageFromChatWidget(
        metadata: item,
      );
    }));
  }

  buildActivityItem(ActivityType item) {
    return InkWell(
      highlightColor: Colors.transparent,
      radius: 0,
      onTap: () {
        List<ISession> chatsAll = [];
        if (relationCtrl.user?.chats != null) {
          chatsAll.addAll(relationCtrl.user!.chats);
        }
        IChatProvider? chatProvider = relationCtrl.provider.chatProvider;
        if (chatProvider?.chats != null) {
          chatsAll.addAll(chatProvider!.chats);
        }
        ISession? iss =
            chatsAll.firstWhereOrNull((element) => element.id == item.shareId);
        if (iss != null) {
          IActivityMessage? activityMessage = iss.activity.activityList
              .firstWhereOrNull((element) => element.metadata.id == item.id);
          if (activityMessage != null) {
            int index = iss.activity.activityList.indexOf(activityMessage);
            iss.activity.currIndex = index < 0 ? 0 : index;
            // RoutePages.to(context: context, data: iss.activity);
            RoutePages.to(
                context: context,
                path: Routers.targetActivity,
                data: iss.activity);
          } else {
            jumpToActivityMsg(item);
            // RoutePages.to(
            //     context: context,
            //     path: Routers.targetActivity,
            //     data: iss.activity);
          }
        } else {
          jumpToActivityMsg(item);
          // RoutePages.to(
          //     context: context,
          //     path: Routers.targetActivity,
          //     data: iss?.activity);
        }
      },
      child: Container(
          decoration: const BoxDecoration(
              border:
                  Border(bottom: BorderSide(color: XColors.dividerLineColor))),
          child: ActivityMessageFromChatWidget(
            metadata: item,
            isHideLikesAndComment: true,
          )),
    );
  }
}
