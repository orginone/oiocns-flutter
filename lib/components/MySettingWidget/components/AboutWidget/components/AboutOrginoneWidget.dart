import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';

class AboutOrginoneWidget extends StatefulWidget {
  const AboutOrginoneWidget({super.key});

  @override
  State<AboutOrginoneWidget> createState() => _AboutOrginoneWidgetState();
}

class _AboutOrginoneWidgetState extends State<AboutOrginoneWidget> {
  var mk = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return XScaffold(
        titleName: '关于奥集能',
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Expanded(
                flex: 0,
                child: Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Image.asset(fit: BoxFit.cover,
                      "assets/markdown/orginone-logo-tou.png", height: 60),
                    /*child: XImage.localImage(
                        fit: BoxFit.contain,
                        "assets/markdown/orginone-logo-tou.png",
                        *//*height: 60*//*)*/)),
            Expanded(
              child: FutureBuilder<String>(
                future: loadAssests(),
                builder: (BuildContext context, AsyncSnapshot<String> shot) {
                  if (shot.hasData &&
                      shot.connectionState == ConnectionState.done) {
                    return Markdown(
                      imageBuilder: (uri, title, alt) =>
                          Image(image: AssetImage(uri.toString())),
                      controller: ScrollController(),
                      selectable: false,
                      data: mk,
                    );
                  }
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                },
              ),
            ),
          ],
        ));
  }

  Future<String> loadAssests() async {
    String filePath = 'assets/markdown/originone.md';
    dynamic result = await rootBundle.loadString(filePath);
    if (result != null) {
      mk = result.toString();
      // LogUtil.d(state.mk.value);
    }
    return mk;
  }

  void backToHome() {
    Navigator.pop(context);
  }
}
