import 'package:flutter/material.dart';
import 'package:orginone/components/XDialogs/XDialog.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import '../../../../../../config/constant.dart';
import '../../../../../../dart/base/api/http_util.dart';
import '../../../../../AppUpdate/AppUpdate.dart';
import 'components/VersionItemWidget.dart';

class VersionListWidget extends StatefulWidget {
  const VersionListWidget({super.key});

  @override
  State<VersionListWidget> createState() => _VersionListWidgetState();
}

class _VersionListWidgetState extends State<VersionListWidget> {
  late List<UpdateModel> loadHistoryVersionInfo = <UpdateModel>[];
  static String jsonUrl =
      '${Constant.host}/orginone/kernel/load/tgv1ucnbugv3dqnbuhe1uoobygm5uc0afy2ib2m28torvyezuenfyge63imv1xi33pmj5gonjrowyx5mlum6vue6lpnw1hvl1wgmzuimr1gi1donjrga1din1vgi2s86ufoj1xs33of1whg33o';

  @override
  void initState() {
    getVersionList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return XScaffold(
        titleName: '版本介绍',
        backgroundColor: Colors.white,
        body: ListView.builder(
          itemCount: loadHistoryVersionInfo.length,
          itemBuilder: (context, int index) {
            var item = loadHistoryVersionInfo[index];
            var versionItem = VersionItemWidget(
              title: item.content ?? '',
              version: item.version ?? '',
              date: item.updateTime/*?.dateTimeFormat*/ ?? '',
              content: item.content ?? '',
            );
            return GestureDetector(
              onTap: () {
                // showDetail(item);
                showDiaLog(context, versionItem);
              },
              child: versionItem,
            );
          },
        ));
  }

  void backToHome() {
    Navigator.pop(context);
  }

  void showDiaLog(BuildContext context, VersionItemWidget item) {
    XDialog.showVersionItemDetail(context, item);
  }

  getVersionList() async {
    // List<UpdateModel> loadHistoryVersionInfo =
    //     await UpdateRequest.loadHistoryVersionInfo();
    // if (loadHistoryVersionInfo.isNotEmpty) {
    //   loadHistoryVersionInfo.sort((a1, a2) {
    //     if (a2.updateTime!.isEmpty || a1.updateTime!.isEmpty) {
    //       return 0;
    //     }
    //     return DateTime.parse(a2.updateTime ?? '')
    //         .compareTo(DateTime.parse(a1.updateTime ?? ''));
    //   });
    // }
    final remoteData = await HttpUtil().post(jsonUrl);

    if (remoteData['status'] != 2000) return false;
    UpdateModel _updateModel = UpdateModel.fromJson((remoteData['data']));
    loadHistoryVersionInfo.clear();
    loadHistoryVersionInfo.add(_updateModel);
    if(_updateModel.records!.isNotEmpty) {
      for(UpdateRecordModel updateRecordModel in _updateModel.records!) {
        UpdateModel updateModel = new UpdateModel(url: updateRecordModel.url, version: updateRecordModel.version);
        updateModel.title = updateRecordModel.title;
        updateModel.content = updateRecordModel.content;
        updateModel.version = updateRecordModel.version;
        updateModel.force = updateRecordModel.force;
        updateModel.updateTime = updateRecordModel.updateTime;
        updateModel.update = updateRecordModel.update;
        loadHistoryVersionInfo.add(updateModel);
      }
    }
    // loadHistoryVersionInfo.addAll(_updateModel.records as List<UpdateModel>);

    // ///remoteVersion 服务器版本号
    // String remoteVersion = _updateModel?.version ?? '1.0.0';
    //
    // ///locVersion 本地版本号
    // String locVersion = _packageInfo?.version ?? '1.0.0';
    //
    // if (remoteVersion.isEmpty || locVersion.isEmpty) return false;
    // int newVersionInt, oldVersion;
    // var newList = remoteVersion.split('.');
    // var oldList = locVersion.split('.');
    // if (newList.isEmpty || oldList.isEmpty) {
    //   return false;
    // }
    // for (int i = 0; i < newList.length; i++) {
    //   newVersionInt = int.parse(newList[i]);
    //   oldVersion = int.parse(oldList[i]);
    //   if (newVersionInt > oldVersion) {
    //     return true;
    //   } else if (newVersionInt < oldVersion) {
    //     return false;
    //   }
    // }

    loadHistoryVersionInfo = loadHistoryVersionInfo;
    setState(() {});
  }
}
