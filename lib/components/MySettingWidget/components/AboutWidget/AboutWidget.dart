import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/Tip/ToastUtils.dart';
import 'package:orginone/components/AppUpdate/AppUpdate.dart';
import 'package:orginone/routers/pages.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../../../config/theme/unified_style.dart';
import '../../../BeautifulBGWidget/BeautifulBGWidget.dart';
import '../../../TextKeyWidget/TextKeyWidget.dart';
import '../../../XScaffold/XScaffold.dart';
import 'components/AboutOrginoneWidget.dart';
import 'components/VersionListWidget/VersionListWidget.dart';

class AboutWidget extends BeautifulBGStatefulWidget {
  const AboutWidget({super.key});

  @override
  State<AboutWidget> createState() => _AboutPageState();
}

class _AboutPageState extends BeautifulBGStatefulState<AboutWidget> {
  var _version = '';

  @override
  void initState() {
    super.initState();
    _getAppVersion();
  }

  // 获取应用版本信息
  Future<void> _getAppVersion() async {
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      _version = 'Version ${packageInfo.version}';
    });
  }

  @override
  Widget buildWidget(BuildContext context) {
    return topAndBottomLayout(
      context,
      top: Column(
        children: [
          const SizedBox(
            height: 100,
          ),
          buildLogoColumn(),
          const SizedBox(
            height: 40,
          ),
          SizedBox(
            height: 20,
            child: Text(
              _version,
              style: TextStyle(
                fontSize: 18,
                color: Colors.grey.shade600,
                fontFamily: 'PingFang SC',
              ),
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          GestureDetector(
            onTap: () {
              RoutePages.to(
                  context: context, child: const AboutOrginoneWidget());
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15),
              margin: EdgeInsets.only(left: 30, right: 30),
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: 0.5, color: Colors.grey),
                  // bottom: BorderSide(width: 0.5, color: Colors.grey),
                ),
              ),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '关于奥集能',
                    style: TextStyle(fontSize: 16),
                  ),
                  Icon(Icons.chevron_right, color: Colors.black54)
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              RoutePages.jumpAssectMarkDown(
                  context: context,
                  title: "服务条款",
                  path: "assets/markdown/term_service.md");
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15),
              margin: EdgeInsets.only(left: 30, right: 30),
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: 0.5, color: Colors.grey),
                  // bottom: BorderSide(width: 0.5, color: Colors.grey),
                ),
              ),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '服务条款',
                    style: TextStyle(fontSize: 16),
                  ),
                  Icon(Icons.chevron_right, color: Colors.black54)
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              RoutePages.jumpAssectMarkDown(
                  title: "隐私条款", path: "assets/markdown/privacy_clause.md");
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15),
              margin: EdgeInsets.only(left: 30, right: 30),
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: 0.5, color: Colors.grey),
                  bottom: BorderSide(width: 0.5, color: Colors.grey),
                ),
              ),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '隐私条款',
                    style: TextStyle(fontSize: 16),
                  ),
                  Icon(Icons.chevron_right, color: Colors.black54)
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              RoutePages.to(
                  context: context, child: const VersionListWidget());
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15),
              margin: EdgeInsets.only(left: 30, right: 30),
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 0.5, color: Colors.grey),
                ),
              ),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '版本信息',
                    style: TextStyle(fontSize: 16),
                  ),
                  Icon(Icons.chevron_right,
                      color: Color.fromRGBO(0, 0, 0, 0.541))
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () async {
              bool update = await AppUpdate.checkUpdate();
              if (!update) {
                ToastUtils.showMsg(msg: "已是最新版本");
              } else {
                AppUpdate.instance.update();
              }
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15),
              margin: EdgeInsets.only(left: 30, right: 30),
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 0.5, color: Colors.grey),
                ),
              ),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '版本更新',
                    style: TextStyle(fontSize: 16),
                  ),
                  Icon(Icons.chevron_right, color: Colors.black54)
                ],
              ),
            ),
          )
        ],
      ),
      bottom: Center(
        child: Column(
          children: [
            // Text(
            //   'Powered by Orginone',
            //   style: TextStyle(
            //     fontSize: 16,
            //     color: Colors.grey.shade600,
            //     fontFamily: 'PingFang SC',
            //   ),
            // ),
            const SizedBox(
              height: 5,
            ),
            TextKeyWidget(
              data: "ICP备案：浙ICP备2024055135号-3A",
              keys: const ['ICP备案：浙ICP备2024055135号-3A'],
              style: TextStyle(color: Colors.black, fontSize: 20.sp),
              keyStyle: TextStyle(color: XColors.themeColor, fontSize: 20.sp),
              onTapCallback: (String key) {
                if (key == 'ICP备案：浙ICP备2024055135号-3A') {
                  RoutePages.jumpWeb(
                      url: 'https://beian.miit.gov.cn/#/Integrated/index');
                }
              },
            ),
            // Text(
            //   'ICP备案：浙ICP备2024055135号-3A',
            //   style: TextStyle(
            //     fontSize: 15,
            //     color: Colors.grey.shade600,
            //     fontFamily: 'PingFang SC',
            //   ),
            // ),
            const SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }
}
