import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:orginone/utils/system/PermissionUtil.dart';
import 'package:permission_handler/permission_handler.dart';

typedef SelectCallBack = void Function(dynamic value);
typedef UploadCallBack = void Function(dynamic value);

enum ImageUploadType { avatar }

class UploudImage {
  final ImagePicker picker = ImagePicker();

  Future<dynamic> camera(BuildContext context) async {
    dynamic result;
    await PermissionUtil.showPermissionDialog(context, Permission.camera,
        callback: () async {
      var camera = ImageSource.camera;
      try {
        XFile? pickedImage = await picker.pickImage(source: camera);
        if (pickedImage != null) {
          result = pickedImage;
        }
      } on PlatformException catch (error) {
        if (error.code == "camera_access_denied") {
          PermissionUtil.showPermissionDialog(context, Permission.camera);
        }
      } catch (error) {
        error.printError();
        Fluttertoast.showToast(msg: "打开相机时发生异常!");
      }
    });
    return result;
  }

  Future<dynamic> chooseImage(BuildContext context) async {
    dynamic result;
    await PermissionUtil.showPermissionDialog(context, Permission.photos,
        callback: () async {
      var gallery = ImageSource.gallery;
      XFile? pickedImage = await picker.pickImage(source: gallery);
      if (pickedImage != null) {
        result = pickedImage;
      }
    });
    return result;
  }
}
