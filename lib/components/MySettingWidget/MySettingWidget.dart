import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/controller/index.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/pages.dart';
import 'package:orginone/routers/router_const.dart';

/// 我的设置
class MySettingWidget extends StatelessWidget {
  const MySettingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding:
            EdgeInsets.fromLTRB(20, ScreenUtil().statusBarHeight + 20, 20, 40),
        child: Column(
          children: [
            settingMenuHeaderWidget(context),
            Padding(
              padding: EdgeInsets.only(top: 32.h, bottom: 32.h),
              child: Divider(
                height: 1.h,
                color: XColors.dividerLineColor,
              ),
            ),
            ...SettingEnum.values
                .sublist(0, SettingEnum.values.length - 1)
                .map(
                  (item) => settingMenuItemWidget(context, item),
                )
                .toList(),
            const Expanded(child: SizedBox()),
            settingMenuItemWidget(context, SettingEnum.exitLogin,
                isShowRightIcon: false)
          ],
        ));
  }

  ///设置弹框的item
  settingMenuItemWidget(BuildContext context, SettingEnum item,
      {bool isShowRightIcon = true}) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        // relationCtrl.functionMenuController.hideMenu();

        // Future.delayed(Duration.zero, () {
        RoutePages.jumpSetting(context, item);
        // });
      },
      child: SizedBox(
        height: 56,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
                width: 24.w,
                height: 24.w,
                child: XImage.localImage(item.icon, width: 24.w)),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 10.w),
                child: Text(
                  item.label,
                  style: TextStyle(
                    color: const Color(0xff15181d),
                    fontSize: 22.sp,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'PingFang SC',
                  ),
                ),
              ),
            ),
            if (isShowRightIcon)
              Icon(
                Icons.arrow_forward_ios,
                size: 24.sp,
              )
          ],
        ),
      ),
    );
  }

  ///设置菜单头部 用户信息
  settingMenuHeaderWidget(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Future.delayed(const Duration(milliseconds: 200), () {
        //   Get.toNamed(Routers.userInfo);
        // });

        RoutePages.jumpRelationInfo(
            data: relationCtrl.user!, defaultActiveTabs: ['设置']);
      },
      child: SizedBox(
        height: 58.w,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _createImgAvatar(
                EdgeInsets.only(
                  right: 14.w,
                ),
                size: 56,
                circular: false,
                radius: 7),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    relationCtrl.provider.user?.metadata.name ?? "",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: 24.sp,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'PingFang SC',
                      color: const Color(0xff15181d),
                    ),
                  ),
                  // TextWidget(
                  //   size: 18.sp,
                  //   text: relationCtrl.provider.user?.metadata.name ?? "",
                  //   style: AppTextStyles.titleSmall
                  //       ?.copyWith(fontWeight: FontWeight.w900),
                  // ),
                  Text(
                    relationCtrl.provider.user?.metadata.remark ?? "",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                      color: const Color(0xff424a57),
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'PingFang SC',
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                Future.delayed(const Duration(milliseconds: 200), () {
                  RoutePages.to(
                    context: context,
                    path: Routers.shareQrCode,
                    data: /*{"entity": relationCtrl.user?.metadata}*/
                        relationCtrl.user?.metadata,
                  );
                });
              },
              child: SizedBox(
                  // color: Colors.green,
                  width: 24.w,
                  height: 24.w,
                  child: XImage.localImage(XImage.qrcode, width: 24.w)),
            )
          ],
        ),
      ),
    );
  }

  ///头像
  Widget _createImgAvatar(EdgeInsets insets,
      {BoxFit fit = BoxFit.cover,
      bool circular = true,
      double size = 44,
      double? radius}) {
    dynamic avatar = XImage.entityIcon(
      relationCtrl.provider.user,
      width: size.w,
      circular: circular,
      radius: radius,
      fit: fit,
    );

    return Container(margin: insets, child: avatar);
  }
}
