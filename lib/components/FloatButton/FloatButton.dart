import 'package:flutter/material.dart';

class FloatButton extends StatelessWidget {
  final Widget child;
  final String tooltip;
  final Widget icon;
  final void Function()? onPressed;

  const FloatButton(
      {super.key,
      required this.child,
      required this.tooltip,
      required this.icon,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return _floatingActionButtonWidget(context);
  }

  _floatingActionButtonWidget(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: const BoxDecoration(color: Colors.white),
      child: Stack(
        children: [
          child,
          Positioned(
            bottom: 16.0,
            right: 16.0,
            child: FloatingActionButton(
              onPressed: onPressed,
              mini: true,
              tooltip: tooltip,
              child: icon,
            ),
          )
        ],
      ),
    );
  }
}
