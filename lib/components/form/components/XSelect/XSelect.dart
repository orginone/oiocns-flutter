import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class XSelect extends StatelessWidget {
  final String title;
  final String content;
  final bool showLine;
  final String? hint;
  final bool required;
  final VoidCallback? onTap;
  final TextStyle? textStyle;

  const XSelect(
      {super.key,
      required this.title,
      required this.content,
      this.showLine = false,
      this.hint,
      this.required = false,
      this.onTap,
      this.textStyle});

  //选择
  const XSelect.dialog(this.title, this.content,
      {super.key,
      this.showLine = false,
      this.hint,
      this.required = false,
      this.onTap,
      this.textStyle});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Colors.white,
          width: double.infinity,
          padding: EdgeInsets.symmetric(vertical: 10.w, horizontal: 16.h),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: showLine
                  ? Border(
                      bottom:
                          BorderSide(color: Colors.grey.shade200, width: 0.5))
                  : null,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: textStyle?.fontSize ?? 18.sp),
                ),
                GestureDetector(
                  onTap: onTap,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.h),
                    child: Row(
                      children: [
                        Expanded(
                          child: content.isEmpty
                              ? Text(
                                  hint ?? "请选择",
                                  style: TextStyle(
                                      color: Colors.grey.shade300,
                                      fontSize: textStyle?.fontSize ?? 18.sp),
                                )
                              : Text(
                                  content,
                                  style: textStyle ??
                                      TextStyle(
                                          color: Colors.black, fontSize: 18.sp),
                                ),
                        ),
                        Icon(
                          Icons.chevron_right,
                          size: 32.w,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
          top: 5.h,
          left: 10.w,
          child: required
              ? const Text(
                  "*",
                  style: TextStyle(color: Colors.red),
                )
              : Container(),
        )
      ],
    );
  }
}
