import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../config/theme/unified_style.dart';

class XChoice<T> extends StatelessWidget {
  final  ValueChanged<bool>? changed;
  final double? iconSize;
  final bool isSelected;


  const XChoice(
      {super.key,
      this.changed,
      this.iconSize,
      this.isSelected = false});

  //  选中  取消选中按钮
  const XChoice.text(this.changed, this.iconSize,
        this.isSelected);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: isSelected
          ? Container(
        width: iconSize ?? 32.w,
        height: iconSize ?? 32.w,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: XColors.themeColor,
        ),
        child: Icon(
          Icons.done,
          size: 20.w,
          color: Colors.white,
        ),
      )
          : Icon(
        Icons.radio_button_off,
        size: iconSize ?? 32.w,
      ),
      onTap: () {
        if (changed != null) {
          changed!(!isSelected);
        }
      },
    );
  }
}
