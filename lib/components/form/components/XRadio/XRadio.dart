import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../XText/text_high_light.dart';

class XRadio<T> extends StatelessWidget {
  final String name;
  final T value;
  final T? groupValue;
  final ValueChanged<T?>? onChanged;
  final String? keyWord;
  final bool showLine;
  final EdgeInsets? padding;

  const XRadio(
      {super.key,
      required this.name,
      required this.value,
      this.groupValue,
      this.onChanged,
      this.keyWord,
      this.showLine = false,
      this.padding});

  //单选按钮
  const XRadio.text(this.name, this.value,
      {super.key, this.groupValue, this.onChanged, this.padding})
      : keyWord = null,
        showLine = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: showLine
              ? Border(bottom: BorderSide(color: Colors.grey.shade300))
              : null,
          color: Colors.white),
      child: Container(
        padding:
            padding ?? EdgeInsets.symmetric(vertical: 5.h, horizontal: 16.w),
        color: Colors.white,
        child: Row(
          children: [
            Radio<T>(
              value: value,
              groupValue: groupValue,
              onChanged: onChanged,
            ),
            TextHighlight(
              content: name,
              keyWord: keyWord,
              normalStyle: TextStyle(fontSize: 16.sp, color: Colors.black87),
              highlightStyle: TextStyle(fontSize: 18.sp, color: Colors.black),
            )
          ],
        ),
      ),
    );
  }
}
