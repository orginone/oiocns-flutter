import 'package:get/get.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/routers/pages.dart';

class FormController extends GetxController {
  FormController();
  String title = '';
  List<XForm> mainForm = [];
  List<XForm> subForm = [];
  _initData() {
    mainForm = RoutePages.routeData.currPageData.data['mainForm'].cast<XForm>();
    subForm = RoutePages.routeData.currPageData.data['subForm'].cast<XForm>();
    title = RoutePages.routeData.currPageData.data['title'];
    update(["form"]);
  }

  void onTap() {}

  // @override
  // void onInit() {
  //   super.onInit();
  // }

  @override
  void onReady() {
    super.onReady();
    _initData();
  }

  // @override
  // void onClose() {
  //   super.onClose();
  // }
}
