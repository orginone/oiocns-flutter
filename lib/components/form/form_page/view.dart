import 'package:flutter/material.dart' hide Form;
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';
import 'package:orginone/components/form/form_widget/view.dart';

class FormPage extends XStatefulWidget {
  FormPage({super.key, super.data});

  @override
  State<FormPage> createState() => _FormPageState();
}

class _FormPageState extends XStatefulState<FormPage, dynamic> {
  @override
  Widget buildWidget(BuildContext context, dynamic data) {
    return FormWidget(
      form: data,
      mainForm: data.metadata == null ? [] : [data.metadata],
      subForm: const [],
    );
  }
}

// class FormPage1 extends GetView<FormController> {
//   const FormPage1({Key? key}) : super(key: key);

//   // 主视图
//   Widget _buildView() {
//     return FormWidget(
//       form: data,
//       mainForm: controller.mainForm,
//       subForm: controller.subForm,
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GetBuilder<FormController>(
//       init: FormController(),
//       id: "form",
//       builder: (_) {
//         return GyScaffold(
//           // appBar: AppBar(title: const Text("form")),
//           titleName: controller.title,
//           body: SafeArea(
//             child: _buildView(),
//           ),
//         );
//       },
//     );
//   }
// }
