import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orginone/dart/extension/ex_list.dart';
import 'package:orginone/dart/extension/ex_widget.dart';
import 'package:orginone/config/theme/space.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/schema.dart';

import '../../XText/XText.dart';

class FormAttributesView extends StatelessWidget {
  const FormAttributesView({
    super.key,
    required this.xform,
    // this.form,
  });
  final XForm xform;
  // final Form? form;
  @override
  Widget build(BuildContext context) {
    return _buildView(xform);
  }

  Widget _buildView(XForm xform) {
    if (xform == null) {
      return Container();
    }
    return <Widget>[
      XText.boxTitle('表单字段列表'),
      _attributesListView(xform),
    ].toColumn();
  }

  Widget _attributesListView(XForm xform) {
    if (xform.attributes == null || xform.attributes!.isEmpty) {
      return Container();
    }
    return ListView.separated(
      itemCount: xform.attributes!.length,
      separatorBuilder: (context, index) => Divider(
        height: 2,
        color: XColors.backgroundColor,
      ),
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        XAttribute attribute = xform.attributes![index];

        return AttributeItem(attribute: attribute);
      },
    );
  }
}

class AttributeItem extends StatelessWidget {
  const AttributeItem({
    super.key,
    required this.attribute,
  });

  final XAttribute attribute;

  @override
  Widget build(BuildContext context) {
    return <Widget>[
      Row(
        children: [
          const XText.fieldName('名称：'),
          XText.fieldValue(attribute.name),
        ],
      ),
      Row(
        children: [
          const XText.fieldName('代码：'),
          XText.fieldValue(attribute.code),
        ],
      ),
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const XText.fieldName('描述：'),
          XText.fieldValue(
            attribute.remark,
            overflow: TextOverflow.clip,
          ).width(Get.width - 80),
        ],
      ),
      Row(
        children: [
          const XText.fieldName('创建时间：'),
          XText.fieldValue(attribute.createTime),
        ],
      ),
    ]
        .toColumn(crossAxisAlignment: CrossAxisAlignment.start)
        .padding(all: AppSpace.page)
        .backgroundColor(XColors.white);
  }
}
