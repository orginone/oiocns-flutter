import 'package:get/get.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/core/thing/standard/form.dart';
import 'package:orginone/routers/pages.dart';
import 'package:orginone/routers/router_const.dart';

class MainFormController extends GetxController {
  MainFormController();

  _initData() {
    // update(["main_form"]);
  }

  void onTap() {}

  // @override
  // void onInit() {
  //   super.onInit();
  // }

  @override
  void onReady() {
    super.onReady();
    _initData();
  }

//跳转到详情
  toDetail(List<FieldModel> fileModels, Form? form) {
    form?.fields = fileModels;
    RoutePages.to(path: Routers.formDetailPreview, data: form);
  }
  // @override
  // void onClose() {
  //   super.onClose();
  // }
}
