import 'package:flutter/material.dart' hide Form;
import 'package:get/get.dart';
import 'package:orginone/dart/extension/index.dart';
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';
import 'package:orginone/components/form/form_widget/form_tool.dart';
import 'package:orginone/components/form/form_widget/main_form/view.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/thing/standard/form.dart';

import '../../../XText/XText.dart';

// ignore: must_be_immutable

//办事使用的需要操作的表单详情
class FormDetailPage extends XStatefulWidget<dynamic> {
  FormDetailPage({super.key, super.data});

  @override
  State<FormDetailPage> createState() => _FormDetailPageState();
}

class _FormDetailPageState extends XStatefulState<FormDetailPage, dynamic> {
  // 主视图
  Widget _buildView(XForm xForm, int infoIndex) {
    if (xForm == null) {
      return Container();
    }
    return SingleChildScrollView(
        child: MainFormPage(
      [xForm],
      infoIndex: infoIndex,
    ));
  }

  @override
  Widget buildWidget(BuildContext context, dynamic data) {
    return _buildView(data['form'], data['infoIndex']);
  }
}

class FormDetailPreviewPage extends XStatefulWidget<Form> {
  FormDetailPreviewPage({super.key, super.data});

  @override
  State<FormDetailPreviewPage> createState() => _FormDetailPreviewPageState();
}

class _FormDetailPreviewPageState
    extends XStatefulState<FormDetailPreviewPage, Form> {
  // 主视图
  Widget _buildView(
    List<FieldModel> fileModels,
  ) {
    if (fileModels.isEmpty) {
      return Container();
    }
    return SingleChildScrollView(
      child: ListView.separated(
        itemCount: fileModels.length,
        separatorBuilder: (context, index) => Divider(
          height: 2,
          color: XColors.backgroundColor,
        ),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          FieldModel model = fileModels[index];

          return FormDataDetailItem(data: model);
        },
      ),
    );
  }

  @override
  Widget buildWidget(BuildContext context, Form data) {
    return _buildView(
      data.fields,
    );
  }
}

class FormDataDetailItem extends StatelessWidget {
  const FormDataDetailItem({
    super.key,
    required this.data,
  });

  final FieldModel data;

  @override
  Widget build(BuildContext context) {
    return _buildView();
  }

  _buildView() {
    return _buildField().backgroundColor(XColors.white);
  }

  Widget _buildField() {
    var w = FutureBuilder<String>(
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done &&
            !snapshot.hasData) {
          return Container();
        }
        var content = snapshot.data??"";
        Widget row = <Widget>[
          Text('${data.name}:  ',
              style: const TextStyle(color: XColors.gray_99)),
          Expanded(
            child: Wrap(
              alignment: WrapAlignment.start,
              children: [
                XText.fieldValue(
                  "null" == content ? "" : content,
                ),
              ],
            ),
          ),
        ]
            .toRow(crossAxisAlignment: CrossAxisAlignment.start)
            .paddingSymmetric(horizontal: 15, vertical: 8);
        return row;
      },
      future: FormTool.buildFieldValue(data),
    );
    return w;
  }
}
