import 'package:flutter/material.dart' hide Form;
import 'package:get/get.dart';
import 'package:orginone/components/form/form_widget/sub_form/view.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/thing/standard/form.dart';

import 'index.dart';
import 'main_form/view.dart';

class FormWidget extends StatefulWidget {
  const FormWidget(
      {Key? key, required this.mainForm, required this.subForm, this.form})
      : super(key: key);
  final Form? form;
  final List<XForm> mainForm;
  final List<XForm> subForm;
  @override
  State<FormWidget> createState() =>
      _FormWidgetState(form: form, mainForm: mainForm, subForm: subForm);
}

class _FormWidgetState extends State<FormWidget> {
  _FormWidgetState({this.form, required this.mainForm, required this.subForm});

  final Form? form;
  final List<XForm> mainForm;
  final List<XForm> subForm;

  @override
  Widget build(BuildContext context) {
    return _FormWidgetViewGetX(
      form: widget.form,
      mainForm: widget.mainForm,
      subForm: widget.subForm,
    );
  }
}

class _FormWidgetViewGetX extends GetView<FormWidgetController> {
  const _FormWidgetViewGetX({
    Key? key,
    this.form,
    required this.mainForm,
    required this.subForm,
  }) : super(key: key);
  final Form? form;
  final List<XForm> mainForm;
  final List<XForm> subForm;
  // 主视图
  Widget _buildView() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          MainFormPage(
            mainForm,
            form: form,
          ),
          SubFormPage(subForm),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FormWidgetController>(
      init: FormWidgetController(),
      id: "form_widget",
      builder: (_) {
        return _buildView();
      },
    );
  }
}
