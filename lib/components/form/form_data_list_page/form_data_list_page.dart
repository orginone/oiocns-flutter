import 'package:flutter/material.dart' hide Form;
import 'package:orginone/components/form/widgets/form_datas_view.dart';
import 'package:orginone/dart/core/thing/standard/form.dart';
import 'package:orginone/dart/base/storages/storage.dart';
import '../../XStatefulWidget/XStatefulWidget.dart';

class FormDataListPage extends XStatefulWidget<Form> {
  FormDataListPage({super.key, super.data});

  @override
  State<FormDataListPage> createState() => _FormDataListPageState();
}

class _FormDataListPageState extends XStatefulState<FormDataListPage, Form> {
  @override
  Widget buildWidget(BuildContext context, Form form) {
    return FormDataView(
      form: form,
      xForm: form.metadata,
      showType: "all",
      type: true == Storage.getBool('formlisttype') ? 0 : 1,
    );
  }
}
