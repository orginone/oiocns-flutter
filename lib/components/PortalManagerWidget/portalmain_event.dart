//Bus初始化
import 'package:event_bus/event_bus.dart';

import '../../dart/core/thing/fileinfo.dart';

EventBus portalMainEventBus = EventBus();

class PortalMainEvent {
  List<IFileInfo>? portalTopdata;
  PortalMainEvent(List<IFileInfo>? portalTopdata) {
    this.portalTopdata = portalTopdata;
  }
}