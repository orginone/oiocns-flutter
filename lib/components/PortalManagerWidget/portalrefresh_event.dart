//Bus初始化
import 'package:event_bus/event_bus.dart';

EventBus portalRefreshEventBus = EventBus();

class PortalRefreshEvent {
  int index = 0;
  PortalRefreshEvent(int index) {
    this.index = index;
  }
}