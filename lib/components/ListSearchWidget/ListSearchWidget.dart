import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/CommandWidget/index.dart';
import 'package:orginone/components/EmptyWidget/EmptyWork.dart';
import 'package:orginone/components/XDialogs/XDialog.dart';
import 'package:orginone/utils/string_util.dart';

import '../../config/theme/unified_style.dart';
import '../../dart/base/common/commands.dart';
import '../../dart/base/ui.dart';
import '../EmptyWidget/EmptyChat.dart';
import '../ListWidget/ListWidget.dart';
import '../XButton/XButton.dart';
import '../XImage/XImage.dart';
import '../XTextField/XTextField.dart';

/// 列表搜索组件
class ListSearchWidget<T extends IEntityUI> extends StatelessWidget {
  /// 初始化数据
  final List<T> initDatas;

  /// 初始化加载
  final FutureOr<List<T>> Function([String? searchText])? onInitLoad;

  /// 加载更多数据
  final FutureOr<List<T>> Function([String? searchText])? onLoad;

  /// 滚动条控制器
  final ScrollController? scrollController;

  /// 获得头像气泡值
  final int? Function(T data)? getBadge;

  /// 最右边的的操作
  final Widget? Function(T data)? getAction;

  /// 点击列表项事件
  final void Function(T, List)? onTap;

  const ListSearchWidget(
      {super.key,
      this.initDatas = const [],
      this.onInitLoad,
      this.onLoad,
      this.getAction,
      this.getBadge,
      this.onTap,
      this.scrollController});

  @override
  Widget build(BuildContext context) {
    TextEditingController searchController = TextEditingController();
    String searchText = "";
    return Column(
      children: [
        // Row(
        //   children: [
        //     XButton.iconCustomText(
        //       text: '所有时间',
        //       iconLeft: false,
        //       bgcolor: XColors.transparent,
        //       textColor: XColors.black,
        //       icon: XImage.localImage(XImage.iconArrawdown, width: 8),
        //       onPressed: () {
        //         XDialog.showListBottomSheet(title: '时间范围', children: [Text('所有时间'), Text('最近24小时'), Text('最近7天'), Text('最近30天')]);
        //       },
        //     ),
        //     XButton.iconCustomText(
        //       text: '全部状态',
        //       iconLeft: false,
        //       bgcolor: XColors.transparent,
        //       textColor: XColors.black,
        //       icon: XImage.localImage(XImage.iconArrawdown, width: 8),
        //       onPressed: () {
        //         XDialog.showListBottomSheet(title: '状态', children: [Text('全部状态'), Text('审批中'), Text('已通过'), Text('已拒绝'), Text('已终止')]);
        //       },
        //     )
        //   ],
        // ),
        XTextField.search(
          controller: searchController,
          onChanged: (value) {
            StringUtil.debounce(() {
              if (searchText != value) {
                command.emitterFlag('search', value);
                searchText = value;
              }
            }, 500);
          },
        ),
        Expanded(
          child: Container(
              margin: EdgeInsets.symmetric(vertical: 1.h),
              decoration: const BoxDecoration(color: Colors.white),
              child: CommandWidget<String>(
                flag: 'search',
                builder: _buildSearchList,
              )),
        )
      ],
    );
  }

  Widget _buildSearchList(BuildContext context, String? searchText) {
    // if (null != onLoad) {
    //   return _buildLazyList(context, searchText);
    // } else {
      return _buildList(context, searchText);
    // }
  }

  /// 构建懒加载列表
  Widget _buildLazyList(BuildContext context, String? searchText) {
    //实现上啦加载下来刷新
    return Container();
  }

  /// 构建列表
  Widget _buildList(BuildContext context, String? searchText) {
    return ListWidget<T>(
      initDatas: search(searchText),
      getDatas: null == onInitLoad
          ? null
          : ([data]) async {
              return onInitLoad?.call(searchText) ?? Future(() => []);
            },
      onInitLoad: null == onInitLoad
          ? null
          : ([data]) async {
        return onInitLoad?.call(searchText) ?? Future(() => []);
      },
      onLoad: onLoad,
      getBadge: getBadge,
      getAction: getAction,
      scrollController: scrollController,
      onTap: onTap,
    );
  }

  List<T> search(String? searchText) {
    List<T> resultList;
    if (null == searchText || searchText.isEmpty) {
      resultList = initDatas;
    } else {
      resultList =
          initDatas.where((element) => element.search(searchText)).toList();
    }
    return resultList;
  }
}
