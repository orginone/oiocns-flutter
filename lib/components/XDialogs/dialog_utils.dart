import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/TabContainerWidget/types.dart';
import 'package:orginone/components/XDialogs/XDialog.dart';
import 'package:orginone/components/XTextField/XTextField.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';
import 'package:orginone/routers/router_const.dart';
import '../../config/constant.dart';
import '../../routers/pages.dart';
import '../../utils/system/notify/notification_util.dart';
import '../XButton/XButton.dart';
import 'package:orginone/dart/extension/index.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/space.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/dart/core/target/authority/authority.dart';
import 'package:orginone/dart/core/target/base/target.dart';
import 'package:orginone/dart/core/thing/standard/species.dart';
import 'package:orginone/main.dart';
import '../XText/XText.dart';
import '../TextKeyWidget/TextKeyWidget.dart';

typedef SelectCallback<T> = void Function(T model);
typedef ClickCallback = void Function(int index, String title);
typedef ClickWorkCallback = void Function(int index, XWorkTask worktask);

typedef CreateOrganizationChangeCallBack = Future<bool> Function(
    String name,
    String code,
    String nickName,
    String identify,
    String remark,
    TargetType type);

typedef CreateIdentityCallBack = Function(
    String name, String code, String authID, String remark);

typedef CreateAttributeCallBack = Function(
  String name,
  String code,
  String valueType,
  String info,
  String remark, [
  String? unit,
  ISpecies? dict,
]);

typedef CreateAttrCallBack = Function(String name, String code, String remark,
    XProperty property, IAuthority authority, bool public);

typedef CreateWorkCallBack = Function(String name, String code, String remark,
    bool allowAdd, bool allowEdit, bool allowSelect, ITarget share);

typedef CreateAuthCallBack = Function(
    String name, String code, ITarget target, bool isPublic, String remark);

typedef CreateClassCriteriaCallBack = Function(
    String name, String code, String specie, String remark,
    [String? resource]);

typedef CreateSpeciesCallBack = Function(
    String name, String info, String remark);

@Deprecated("待优化")
class PickerUtils {
  static void showVideoMethodsPicker(BuildContext context,
      {required List<String> videoMethods,
      required SelectCallback<String> callback}) {
    showCupertinoPicker(
      context,
      titles: videoMethods,
      callback: (index, title) {
        callback(videoMethods[index]);
      },
    );
  }

  static void showDynamicPicker(BuildContext context,
      {required List<dynamic> models,
      required SelectCallback<dynamic> callback}) {
    showCupertinoPicker(
      context,
      titles: models.map((e) => e.name.toString()).toList(),
      callback: (index, title) {
        callback(models[index]);
      },
    );
  }

  static void showMapPicker(
    BuildContext context, {
    required Map<String, String> map,
    required SelectCallback<String> callback,
    int initIndex = 0,
  }) {
    showCupertinoPicker(
      context,
      titles: map.values.toList(),
      initIndex: initIndex,
      callback: (index, title) {
        callback(map.keys.toList()[index]);
      },
    );
  }

  static void showListStringPicker(BuildContext context,
      {required List<String> titles,
      required SelectCallback<String> callback,
      String doneTitle = "确定"}) {
    showCupertinoPicker(context, titles: titles, callback: (index, title) {
      callback(title);
    }, doneTitle: doneTitle);
  }

  static void showTestPicker(BuildContext context) {
    showCupertinoPicker(
      context,
      titles: [
        "男",
        "女",
        "保密",
      ],
      callback: (index, title) {},
    );
  }

  static showWorktaskPicker(BuildContext context,
      {required List<XWorkTask> tasks,
        required ClickWorkCallback workcallback,
        String cancelTitle = "取消",
        String doneTitle = "提交",
        int initIndex = 0}) {
    int index = 0;
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 250,
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CupertinoButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        cancelTitle,
                        style: const TextStyle(
                          fontSize: 15,
                          color: Colors.black38,
                        ),
                      ),
                    ),
                    CupertinoButton(
                      onPressed: () {
                        Navigator.pop(context);
                        workcallback(index, tasks[index]);
                      },
                      child: Text(
                        doneTitle,
                        style: TextStyle(
                          fontSize: 15,
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: CupertinoPicker(
                    itemExtent: 40,
                    scrollController:
                    FixedExtentScrollController(initialItem: initIndex),
                    onSelectedItemChanged: (value) {
                      index = value;
                    },
                    children: tasks
                        .map((e) => Container(
                      height: 40,
                      alignment: Alignment.center,
                      child: Text(
                        e.title!.substring(0, e.title!.lastIndexOf(']') + 1).replaceAll('[驳回重批]-', ''),
                        style: const TextStyle(
                          fontSize: 15,
                        ),
                      ),
                    ))
                        .toList(),
                  ),
                ),
              ],
            ),
          );
        });
  }

  static showCupertinoPicker(BuildContext context,
      {required List<String> titles,
      required ClickCallback callback,
      String cancelTitle = "取消",
      String doneTitle = "同意",
      int initIndex = 0}) {
    int index = 0;
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 250,
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CupertinoButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        cancelTitle,
                        style: const TextStyle(
                          fontSize: 15,
                          color: Colors.black38,
                        ),
                      ),
                    ),
                    CupertinoButton(
                      onPressed: () {
                        Navigator.pop(context);
                        callback(index, titles[index]);
                      },
                      child: Text(
                        doneTitle,
                        style: TextStyle(
                          fontSize: 15,
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: CupertinoPicker(
                    itemExtent: 40,
                    scrollController:
                        FixedExtentScrollController(initialItem: initIndex),
                    onSelectedItemChanged: (value) {
                      index = value;
                    },
                    children: titles
                        .map((e) => Container(
                              height: 40,
                              alignment: Alignment.center,
                              child: Text(
                                e,
                                style: const TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                            ))
                        .toList(),
                  ),
                ),
              ],
            ),
          );
        });
  }
}

@Deprecated("待优化")
class AlertDialogUtils {
  static buildBottomModal(BuildContext context, List<ActionModel> listAction,
      {bool isNeedPop = true}) {
    return showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.only(bottom: 40.h),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: listAction
                .map((itemObj) => GestureDetector(
                    onTap: () {
                      itemObj.onTap();
                      if (isNeedPop) Navigator.pop(context);
                    },
                    child: Container(
                      height: 80.h,
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            width: 1,
                            color: XColors.dividerLineColor,
                          ),
                        ),
                      ),
                      child: Text(
                        itemObj.title,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 24.sp,
                        ),
                      ),
                    )))
                .toList(),
          ),
        );
      },
    );
  }

  static showPrivateKey(BuildContext context, String privateKey) {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
              backgroundColor: Colors.white,
              shadowColor: Colors.grey.shade50,
              alignment: Alignment.center,
              child: SingleChildScrollView(
                child: StatefulBuilder(
                  builder: (context, setState) {
                    return Container(
                      padding: const EdgeInsets.all(20),
                      alignment: Alignment.centerLeft,
                      child: Column(children: [
                        Container(
                          padding: const EdgeInsets.only(left: 0),
                          alignment: Alignment.centerLeft,
                          child: const Text(
                            '账户私钥',
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 18),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                  padding: const EdgeInsets.only(left: 10),
                                  alignment: Alignment.centerLeft,
                                  height: 45,
                                  clipBehavior: Clip.antiAlias,
                                  decoration: ShapeDecoration(
                                    color: Colors.amber.shade50,
                                    shape: RoundedRectangleBorder(
                                      side: const BorderSide(
                                          width: 1, color: Color(0xFFE7E8EB)),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                  child: const Text(
                                    '请妥善保管私钥，请勿告诉他人',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                        fontSize: 16),
                                  )),
                              Container(
                                  padding: const EdgeInsets.only(left: 10),
                                  alignment: Alignment.centerLeft,
                                  height: 45,
                                  clipBehavior: Clip.antiAlias,
                                  decoration: ShapeDecoration(
                                    color: Colors.amber.shade50,
                                    shape: RoundedRectangleBorder(
                                      side: const BorderSide(
                                          width: 1, color: Color(0xFFE7E8EB)),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                  child: const Text(
                                    '该私钥可以为你重置密码，加解密数据',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                        fontSize: 16),
                                  )),
                              Container(
                                  padding: const EdgeInsets.only(left: 10),
                                  alignment: Alignment.centerLeft,
                                  height: 45,
                                  clipBehavior: Clip.antiAlias,
                                  decoration: ShapeDecoration(
                                    color: Colors.amber.shade50,
                                    shape: RoundedRectangleBorder(
                                      side: const BorderSide(
                                          width: 1, color: Color(0xFFE7E8EB)),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                  child: const Text(
                                    '可拍照截图保存',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w200,
                                        fontSize: 16),
                                  )),
                              Container(
                                  padding: const EdgeInsets.only(left: 10),
                                  alignment: Alignment.centerLeft,
                                  height: 45,
                                  clipBehavior: Clip.antiAlias,
                                  decoration: ShapeDecoration(
                                    color: Colors.amber.shade50,
                                    shape: RoundedRectangleBorder(
                                      side: const BorderSide(
                                          width: 1, color: Color(0xFFE7E8EB)),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                  child: Text(
                                    privateKey,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 19),
                                  )),
                              const SizedBox(
                                height: 20,
                              ),
                              Container(
                                alignment: Alignment.center,
                                height: 45,
                                clipBehavior: Clip.antiAlias,
                                decoration: ShapeDecoration(
                                  color: XColors.themeColor,
                                  shape: RoundedRectangleBorder(
                                    side: const BorderSide(
                                        width: 1, color: Color(0xFFE7E8EB)),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                                child: XButton.solidMin(
                                    // bgColor: XColors.themeColor,
                                    // height: 45,
                                    '我知道了，去登录',
                                    // textColor: XColors.white,
                                    // textSize: 18,
                                    onPressed: () {
                                  RoutePages.to(
                                      context: context, path: Routers.login);
                                }),
                              ),
                            ],
                          ),
                        ),
                      ]),
                    );
                  },
                ),
              ));
        });
  }

  ///服务条款和隐私条款弹框
  static showPrivacyDialog(BuildContext context) async {
    final pref = await SharedPreferences.getInstance();
    bool? isAgree = pref.getBool(Constant.privaPolicycheck);
    if (isAgree == null || !isAgree) {
      String data =
          "\n     为了保障您的权利，在使用我们的服务前，请您务必审慎阅读、充分理解此内容中的“服务协议”和“隐私政策”各条款，了解我们对于个人信息的使用情况与您所享有的相关权利。我们将严格按照以上文件内容为您提供服务，并以专业的技术为您的信息安全保驾护航。"
          "\n     您可以查看完整版的《服务条款》与《隐私条款》了解详细信息，若您同意，则请点击“同意”开始接受我们的服务。";
      showGeneralDialog(
          context: context,
          barrierDismissible: false,
          barrierLabel: '',
          transitionDuration: const Duration(milliseconds: 500),
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return WillPopScope(
                onWillPop: () async => false, // 返回 false 禁用返回键退出
                child: Center(
                  child: Material(
                    child: Container(
                      height: MediaQuery.of(context).size.height * .6,
                      width: MediaQuery.of(context).size.width * .8,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(24.w)),
                      ),
                      child: Column(
                        children: [
                          Container(
                            height: 45,
                            alignment: Alignment.center,
                            child: const Text(
                              '服务协议和隐私政策',
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          const Divider(
                            height: 1,
                          ),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SingleChildScrollView(
                              child: TextKeyWidget(
                                data: data,
                                keys: const ['《服务条款》', '《隐私条款》'],
                                onTapCallback: (String key) {
                                  if (key == '《服务条款》') {
                                    RoutePages.jumpAssectMarkDown(
                                        title: "服务条款",
                                        path:
                                            "assets/markdown/term_service.md");
                                  } else if (key == '《隐私条款》') {
                                    RoutePages.jumpAssectMarkDown(
                                        title: "隐私条款",
                                        path:
                                            "assets/markdown/privacy_clause.md");
                                  }
                                },
                              ),
                            ),
                          )),
                          const Divider(
                            height: 1,
                          ),
                          SizedBox(
                            height: 45,
                            child: Row(
                              children: [
                                Expanded(
                                    child: GestureDetector(
                                  child: Container(
                                      alignment: Alignment.center,
                                      child: const Text(
                                        '暂不同意',
                                        style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 14),
                                      )),
                                  onTap: () async {
                                    final pref =
                                        await SharedPreferences.getInstance();
                                    bool isCheckOk = await pref.setBool(
                                        Constant.privaPolicycheck, false);
                                    if (isCheckOk) {
                                      exit(0);
                                    }
                                  },
                                )),
                                const VerticalDivider(
                                  width: 1,
                                ),
                                Expanded(
                                    child: GestureDetector(
                                  child: Container(
                                      alignment: Alignment.center,
                                      color: Theme.of(context).primaryColor,
                                      child: const Text(
                                        '同意',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 14),
                                      )),
                                  onTap: () async {
                                    final pref =
                                        await SharedPreferences.getInstance();
                                    bool isCheckOk = await pref.setBool(
                                        Constant.privaPolicycheck, true);
                                    if (isCheckOk) {
                                      Navigator.pop(context);
                                      // 初始化通知服务
                                      await NotificationUtil.initializeService();
                                    }
                                  },
                                )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ));
          });
    } else {
      //
    }
  }
}

// Future<void> showCreateIdentityDialog(
//     BuildContext context, List<IAuthority> authority,
//     {CreateIdentityCallBack? onCreate, IIdentity? identity}) async {
//   List<IAuthority> allAuth = config.getAllAuthority(authority);

//   TextEditingController name =
//       TextEditingController(text: identity?.metadata.name);
//   TextEditingController code =
//       TextEditingController(text: identity?.metadata.code);
//   TextEditingController remark =
//       TextEditingController(text: identity?.metadata.remark);

//   IAuthority? selected;
//   return showDialog(
//     context: context,
//     builder: (context) {
//       return Dialog(
//           alignment: Alignment.center,
//           child: SingleChildScrollView(
//             child: StatefulBuilder(builder: (context, state) {
//               return SizedBox(
//                 child: Column(
//                   mainAxisSize: MainAxisSize.min,
//                   children: [
//                     CommonWidget.commonHeadInfoWidget(
//                         identity != null ? "编辑" : "新增"),
//                     CommonWidget.commonTextTile("角色名称", '',
//                         controller: name,
//                         showLine: true,
//                         required: true,
//                         hint: "请输入",
//                         textStyle: XFonts.size22Black0),
//                     CommonWidget.commonTextTile("角色编号", '',
//                         controller: code,
//                         showLine: true,
//                         required: true,
//                         hint: "请输入",
//                         textStyle: XFonts.size22Black0),
//                     identity != null
//                         ? const SizedBox()
//                         : CommonWidget.commonChoiceTile(
//                             "设置权限", selected?.metadata.name ?? "",
//                             showLine: true, required: true, onTap: () {
//                             PickerUtils.showListStringPicker(navigatorKey.currentState!.context,
//                                 titles: allAuth
//                                     .map((e) => e.metadata.name ?? "")
//                                     .toList(), callback: (str) {
//                               state(() {
//                                 try {
//                                   selected = allAuth.firstWhere((element) =>
//                                       element.metadata.name == str);
//                                   // ignore: empty_catches
//                                 } catch (e) {}
//                               });
//                             });
//                           }, hint: "请选择", textStyle: XFonts.size22Black0),
//                     CommonWidget.commonTextTile("角色简介", '',
//                         controller: remark,
//                         showLine: true,
//                         maxLine: 4,
//                         hint: "请输入",
//                         textStyle: XFonts.size22Black0),
//                     CommonWidget.commonMultipleSubmitWidget(onTap1: () {
//                       Navigator.pop(context);
//                     }, onTap2: () {
//                       if (name.text.isEmpty) {
//                         ToastUtils.showMsg(msg: "请输入角色名称");
//                       } else if (code.text.isEmpty) {
//                         ToastUtils.showMsg(msg: "请输入角色编号");
//                       } else if (selected == null && identity == null) {
//                         ToastUtils.showMsg(msg: "请设置权限");
//                       } else {
//                         if (onCreate != null) {
//                           onCreate(name.text, code.text,
//                               selected?.metadata.id ?? "", remark.text);
//                         }
//                         Navigator.pop(context);
//                       }
//                     }),
//                   ],
//                 ),
//               );
//             }),
//           ));
//     },
//   );
// }

@Deprecated("待统一优化")
//showCreateOrganizationBottomSheet
Future<void> showSearchBottomSheet(BuildContext context, TargetType targetType,
    {String title = '',
    String hint = '',
    ValueChanged<List<XTarget>>? onSelected}) async {
  TextEditingController searchController = TextEditingController();
  List<XTarget> data = [];
  List<XTarget> selected = [];
  Widget itemWidget(XTarget item, {VoidCallback? onTap}) {
    if (selected.isEmpty) {
      selected.add(item);
    }
    bool isSelected = selected.contains(item);

    List<Widget> widgets = [];

    widgets = [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          XImage.entityIcon(item, width: 40),
          SizedBox(
            width: 10.h,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              XText.dialogTitle(item.name),
              SizedBox(
                height: 10.h,
              ),
              XText.dialogContent(
                item.remark,
                // color: XColors.gray_66,
                // softWrap: true,
                maxLines: 6,
                overflow: TextOverflow.ellipsis,
              ).width(Get.width - 110),
            ],
          )
        ],
      )
    ];
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(
            bottom: 10.h, left: AppSpace.page, right: AppSpace.page),
        width: Get.width - AppSpace.page * 2,
        decoration: BoxDecoration(
            color: isSelected
                ? XColors.formBackgroundColor.withOpacity(0.2)
                : Colors.white,
            borderRadius: BorderRadius.circular(4.w),
            border: Border.all(
                color: isSelected ? XColors.themeColor : Colors.grey.shade400,
                width: 0.5)),
        padding: EdgeInsets.symmetric(vertical: 15.w, horizontal: 20.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widgets,
        ),
      ),
    );
  }

  Future<List<XTarget>> search(String code) async {
    List<XTarget>? targets;
    if (null != relationCtrl.user) {
      switch (targetType) {
        case TargetType.group:
          targets = await relationCtrl.user!
              .searchTargets(code, [TargetType.group.label]);
          break;
        case TargetType.cohort:
          targets = await relationCtrl.user!
              .searchTargets(code, [TargetType.cohort.label]);
          break;
        case TargetType.person:
          targets = await relationCtrl.user!
              .searchTargets(code, [TargetType.person.label]);
          break;
        case TargetType.company:
        case TargetType.hospital:
        case TargetType.university:
          targets = await relationCtrl.user!.searchTargets(code, [
            TargetType.company.label,
            TargetType.hospital.label,
            TargetType.university.label
          ]);
          break;
        default:
      }
    }
    // if (targets == null || targets.isEmpty) {
    //   ToastUtils.showMsg(msg: '未查询到相关数据请核对');
    // }
    return targets ?? [];
  }

  return showModalBottomSheet(
    isScrollControlled: true,
    context: context,
    backgroundColor: Colors.white,
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10), topRight: Radius.circular(10))),
    builder: (context) {
      return SingleChildScrollView(
        child: StatefulBuilder(builder: (context, state) {
          return AnimatedPadding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context)
                    .viewInsets
                    .bottom), // 我们可以根据这个获取需要的padding
            duration: const Duration(milliseconds: 100),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                XDialog.headInfoWidget(title),
                XTextField.search(
                    controller: searchController,
                    // autofocus: true,
                    // searchColor: XColors.bgColor,
                    onChanged: (code) async {
                      search(code).then((value) {
                        state(() {
                          selected.clear();
                          data = value;
                        });
                      });
                    },
                    hint: hint),
                data.isEmpty && searchController.text.isNotEmpty
                    ? SizedBox(
                        height: 100.h,
                        child: const Center(
                          child: Text('抱歉，没有查询到相关结果'),
                        )
                            .height(88)
                            .width(Get.width - AppSpace.page * 2)
                            .border(all: 1, color: XColors.bgColor),
                      )
                    : SizedBox(
                        child: SingleChildScrollView(
                          child: Column(
                            children: data.map((e) {
                              return itemWidget(e, onTap: () {
                                state(() {
                                  if (selected.contains(e)) {
                                    selected.remove(e);
                                  } else {
                                    selected.add(e);
                                  }
                                });
                              });
                            }).toList(),
                          ),
                        ),
                      ),
                commonMultipleSubmitWidget(onTap1: () {
                  Navigator.pop(context);
                }, onTap2: () {
                  if (onSelected != null) {
                    onSelected(selected);
                  }
                  Navigator.pop(context);
                }),
              ],
            ),
          );
        }),
      );
    },
  );
}

//两个按钮
Widget commonMultipleSubmitWidget(
    {String str1 = "取消",
    String str2 = "确定",
    VoidCallback? onTap1,
    VoidCallback? onTap2}) {
  return Container(
    width: double.infinity,
    padding: EdgeInsets.symmetric(vertical: 15.h),
    color: Colors.white,
    alignment: Alignment.center,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        GestureDetector(
          onTap: onTap1,
          child: Container(
            width: 200.w,
            height: 50.h,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4.w),
                border: Border.all(color: XColors.themeColor)),
            alignment: Alignment.center,
            child: Text(
              str1,
              style: TextStyle(color: XColors.themeColor, fontSize: 16.sp),
            ),
          ),
        ),
        GestureDetector(
          onTap: onTap2,
          child: Container(
            width: 200.w,
            height: 50.h,
            decoration: BoxDecoration(
              color: Colors.blueAccent,
              borderRadius: BorderRadius.circular(4.w),
            ),
            alignment: Alignment.center,
            child: Text(
              str2,
              style: TextStyle(color: Colors.white, fontSize: 16.sp),
            ),
          ),
        ),
      ],
    ),
  );
}
