import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/config/theme/UIConfig.dart';
import 'package:orginone/dart/extension/ex_string.dart';
import 'package:orginone/main.dart';

import '../../config/theme/unified_style.dart';
import '../../dart/base/schema.dart';
import '../../dart/core/public/enums.dart';
import '../../dart/core/target/authority/authority.dart';
import '../../dart/core/target/base/target.dart';
import '../../dart/core/thing/standard/species.dart';
import '../EntityWidget/EntitySettingWidget/components/DissolutionWidget.dart';
import '../MySettingWidget/components/AboutWidget/components/VersionListWidget/components/VersionItemWidget.dart';
import '../Tip/ToastUtils.dart';
import '../XButton/XButton.dart';
import '../XImage/components/icon.dart';
import '../XTextField/XTextField.dart';
import '../form/components/XSelect/XSelect.dart';
import 'dialog_utils.dart';

typedef CreateDictChangeCallBack = Function(
    String name, String code, String remark);
typedef CreateFormCallBack = Function(String name, String code, bool public);

class XDialog {
  static void showConfirmDialog(
      {BuildContext? context,
      required String title,
      required String content,
      String? confirmText,
      Function? confirmFun}) {
    showDialog(
        context: context ?? navigatorKey.currentContext!,
        builder: (context) => Center(
            child: Material(
                color: Colors.transparent,
                child: ConfirmDialog(
                    title: title,
                    content: content,
                    confirmText: confirmText ?? "确认",
                    confirmFun: confirmFun ?? () {}))),
        barrierColor: Colors.black.withOpacity(0.6));
  }

  static Future<void> showCreateDictItemDialog(BuildContext context,
      {CreateDictChangeCallBack? onCreate,
      String name = '',
      String code = '',
      String remark = '',
      bool isEdit = false}) async {
    TextEditingController nameCtr = TextEditingController(text: name);
    TextEditingController codeCtr = TextEditingController(text: code);
    TextEditingController remarkCtr = TextEditingController(text: remark);

    return XDialog.showFormDialog(
        context: context,
        title: "${isEdit ? "编辑" : "新增"}字典项",
        children: [
          XTextField.input(
            title: "名称",
            controller: nameCtr,
            showLine: true,
            required: true,
            hint: "请输入",
            // textStyle: XFonts.size22Black0
          ),
          XTextField.input(
            title: "值",
            controller: codeCtr,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "备注",
            controller: remarkCtr,
            showLine: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () {
          if (nameCtr.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入名称");
          } else if (codeCtr.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入值");
          } else {
            if (onCreate != null) {
              onCreate(nameCtr.text, codeCtr.text, remarkCtr.text);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showCreateDictDialog(BuildContext context,
      {CreateDictChangeCallBack? onCreate,
      bool isEdit = false,
      String name = '',
      String code = '',
      String remark = ''}) async {
    TextEditingController nameCtr = TextEditingController(text: name);
    TextEditingController codeCtr = TextEditingController(text: code);
    TextEditingController remarkCtr = TextEditingController(text: remark);

    return XDialog.showFormDialog(
        context: context,
        title: isEdit ? "编辑" : "新增",
        children: [
          XTextField.input(
            title: "字典名称",
            controller: nameCtr,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "字典代码",
            controller: codeCtr,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "备注",
            controller: remarkCtr,
            showLine: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () {
          if (nameCtr.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入名称");
          } else if (codeCtr.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入字典代码");
          } else {
            if (onCreate != null) {
              onCreate(nameCtr.text, codeCtr.text, remarkCtr.text);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showCreateFormDialog(BuildContext context,
      {CreateFormCallBack? onCreate,
      String name = '',
      String code = '',
      bool public = true,
      bool isEdit = false}) async {
    TextEditingController nameCrl = TextEditingController(text: name);
    TextEditingController codeCrl = TextEditingController(text: code);

    bool isPublic = public;
    return XDialog.showFormDialog(
        context: context,
        title: isEdit ? "编辑" : "新增",
        children: [
          XTextField.input(
            title: "字典名称",
            controller: nameCrl,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "字典代码",
            controller: codeCrl,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XSelect.dialog("向下组织公开", public ? "公开" : '不公开',
              showLine: true, required: true, onTap: () {
            PickerUtils.showListStringPicker(navigatorKey.currentState!.context,
                titles: ['公开', "不公开"], callback: (str) {
              // state(() {
              public = str == "公开";
              // });
            });
          }, hint: "请选择", textStyle: XFonts.size22Black0)
        ],
        onOk: () {
          if (nameCrl.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入名称");
          } else if (codeCrl.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入字典代码");
          } else {
            if (onCreate != null) {
              onCreate(nameCrl.text, codeCrl.text, isPublic);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showCreateOrganizationBottomSheet(
      BuildContext context, List<TargetType> targetType,
      {String? headerTitle,
      String name = '',
      String code = '',
      String nickName = '',
      String identify = '',
      String remark = '',
      TargetType? type,
      CreateOrganizationChangeCallBack? callBack,
      bool isEdit = false}) async {
    TextEditingController nameController = TextEditingController(text: name);
    TextEditingController codeController = TextEditingController(text: code);
    TextEditingController nickNameController =
        TextEditingController(text: nickName);
    TextEditingController identifyController =
        TextEditingController(text: identify);
    TextEditingController remarkController =
        TextEditingController(text: remark);

    TargetType selectedTarget = type ?? targetType.first;

    return XDialog.showFormBottomSheet(
        context: context,
        title: headerTitle,
        isEdit: isEdit,
        children: [
          XTextField.input(
            title: "名称",
            controller: nameController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "代码",
            controller: codeController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          // XTextField.input(title:"简称",
          //     controller: nickNameController,
          //     showLine: true,
          //     required: true,
          //     hint: "请输入",
          //     ),
          XSelect.dialog(
            "选择制定组织",
            selectedTarget.label,
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: targetType.map((e) => e.label).toList(),
                  callback: (str) {
                // state(() {
                try {
                  selectedTarget =
                      targetType.firstWhere((element) => element.label == str);
                  // ignore: empty_catches
                } catch (e) {}
                // });
              });
            },
            hint: "请选择",
          ),
          XTextField.input(
            title: "标识",
            controller: identifyController,
            showLine: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "备注",
            controller: remarkController,
            showLine: true,
            required: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () async {
          if (nameController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入名称");
          } else if (codeController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入代码");
          } /*else if (nickNameController.text.isEmpty) {
                    ToastUtils.showMsg(msg: "请输入简称");
                  }*/
          else if (remarkController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入备注");
          } else {
            bool res = true;
            if (callBack != null) {
              res = await callBack(
                  nameController.text,
                  codeController.text,
                  nickNameController.text,
                  identifyController.text,
                  remarkController.text,
                  selectedTarget);
            }
            if (res) {
              Navigator.pop(context);
            }
          }
        });
    // showModalBottomSheet(
    //   isScrollControlled: true,
    //   context: context,
    //   backgroundColor: Colors.white,
    //   shape: const RoundedRectangleBorder(
    //       borderRadius: BorderRadius.only(
    //           topLeft: Radius.circular(10), topRight: Radius.circular(10))),
    //   builder: (context) {
    //     return SingleChildScrollView(
    //       child: StatefulBuilder(builder: (context, state) {
    //         return AnimatedPadding(
    //           padding: EdgeInsets.only(
    //               bottom: MediaQuery.of(context)
    //                   .viewInsets
    //                   .bottom), // 我们可以根据这个获取需要的padding
    //           duration: const Duration(milliseconds: 100),
    //           child: Column(
    //             mainAxisSize: MainAxisSize.min,
    //             children: [
    //               SingleChildScrollView(
    //                 child: Column(
    //                   children: [
    //                     const SizedBox(height: 10),
    //                     CommonWidget.commonHeadInfoWidget(
    //                       headerTitle ?? (isEdit ? "编辑" : "新建"),
    //                       color: Colors.white,
    //                     ),
    //                     CommonWidget.commonTextTile("名称", '',
    //                         controller: nameController,
    //                         showLine: true,
    //                         required: true,
    //                         hint: "请输入",
    //                         textStyle: XFonts.size22Black0),
    //                     CommonWidget.commonTextTile("代码", '',
    //                         controller: codeController,
    //                         showLine: true,
    //                         required: true,
    //                         hint: "请输入",
    //                         textStyle: XFonts.size22Black0),
    //                     // CommonWidget.commonTextTile("简称", '',
    //                     //     controller: nickNameController,
    //                     //     showLine: true,
    //                     //     required: true,
    //                     //     hint: "请输入",
    //                     //     textStyle: XFonts.size22Black0),
    //                     CommonWidget.commonChoiceTile(
    //                         "选择制定组织", selectedTarget.label,
    //                         showLine: true, required: true, onTap: () {
    //                       PickerUtils.showListStringPicker(
    //                           navigatorKey.currentState!.context,
    //                           titles: targetType.map((e) => e.label).toList(),
    //                           callback: (str) {
    //                         state(() {
    //                           try {
    //                             selectedTarget = targetType.firstWhere(
    //                                 (element) => element.label == str);
    //                             // ignore: empty_catches
    //                           } catch (e) {}
    //                         });
    //                       });
    //                     }, hint: "请选择", textStyle: XFonts.size22Black0),
    //                     CommonWidget.commonTextTile("标识", '',
    //                         controller: identifyController,
    //                         showLine: true,
    //                         hint: "请输入",
    //                         textStyle: XFonts.size22Black0),
    //                     CommonWidget.commonTextTile("备注", '',
    //                         controller: remarkController,
    //                         showLine: true,
    //                         required: true,
    //                         maxLine: 4,
    //                         hint: "请输入",
    //                         textStyle: XFonts.size22Black0),
    //                   ],
    //                 ),
    //               ),
    //               CommonWidget.commonMultipleSubmitWidget(onTap1: () {
    //                 Navigator.pop(context);
    //               }, onTap2: () async {
    //                 if (nameController.text.isEmpty) {
    //                   ToastUtils.showMsg(msg: "请输入名称");
    //                 } else if (codeController.text.isEmpty) {
    //                   ToastUtils.showMsg(msg: "请输入代码");
    //                 } /*else if (nickNameController.text.isEmpty) {
    //                   ToastUtils.showMsg(msg: "请输入简称");
    //                 }*/
    //                 else if (remarkController.text.isEmpty) {
    //                   ToastUtils.showMsg(msg: "请输入备注");
    //                 } else {
    //                   bool res = true;
    //                   if (callBack != null) {
    //                     res = await callBack(
    //                         nameController.text,
    //                         codeController.text,
    //                         nickNameController.text,
    //                         identifyController.text,
    //                         remarkController.text,
    //                         selectedTarget);
    //                   }
    //                   if (res) {
    //                     Navigator.pop(context);
    //                   }
    //                 }
    //               }),
    //             ],
    //           ),
    //         );
    //       }),
    //     );
    //   },
    // );
  }

// static Future<void> showCreateAttributeDialog(BuildContext context,
//     {CreateAttributeCallBack? onCreate,
//     List<ISpecies> dictList = const [],
//     bool isEdit = false,
//     String? name,
//     String? code,
//     String? info,
//     String? remark,
//     String? valueType,
//     String? unit,
//     String? dictId}) async {
//   TextEditingController nameCtr = TextEditingController(text: name);
//   TextEditingController codeCtr = TextEditingController(text: code);
//   TextEditingController unitCtr = TextEditingController(text: unit);
//   TextEditingController infoCtr = TextEditingController(text: info);
//   TextEditingController remarkCtr = TextEditingController(text: remark);

//   String? type = valueType;
//   ISpecies? dictValue = dictId != null
//       ? dictList.firstWhere((element) => element.id == dictId)
//       : null;

//   return showDialog(
//     context: context,
//     builder: (context) {
//       return Dialog(
//           alignment: Alignment.center,
//           child: SingleChildScrollView(
//             child: StatefulBuilder(builder: (context, state) {
//               return SizedBox(
//                 child: Column(
//                   mainAxisSize: MainAxisSize.min,
//                   children: [
//                     CommonWidget.commonHeadInfoWidget(isEdit ? "编辑" : "新增"),
//                     CommonWidget.commonTextTile("属性名称", '',
//                         controller: nameCtr,
//                         showLine: true,
//                         required: true,
//                         hint: "请输入",
//                         textStyle: XFonts.size22Black0),
//                     CommonWidget.commonTextTile("属性代码", '',
//                         controller: codeCtr,
//                         showLine: true,
//                         required: true,
//                         hint: "请输入",
//                         textStyle: XFonts.size22Black0),
//                     CommonWidget.commonChoiceTile("属性类型", type ?? "",
//                         showLine: true, required: true, onTap: () {
//                       PickerUtils.showListStringPicker(navigatorKey.currentState!.context,
//                           titles: config.ValueType, callback: (str) {
//                         state(() {
//                           type = str;
//                         });
//                       });
//                     }, hint: "请选择", textStyle: XFonts.size22Black0),
//                     type == "选择型"
//                         ? CommonWidget.commonChoiceTile(
//                             "选择字典", dictValue?.metadata.name ?? "",
//                             showLine: true, required: true, onTap: () {
//                             PickerUtils.showListStringPicker(navigatorKey.currentState!.context,
//                                 titles: dictList
//                                     .map((e) => e.metadata.name ?? "")
//                                     .toList(), callback: (str) {
//                               state(() {
//                                 dictValue = dictList.firstWhere(
//                                     (element) => element.metadata.name == str);
//                               });
//                             });
//                           }, hint: "请选择", textStyle: XFonts.size22Black0)
//                         : const SizedBox(),
//                     type == "数值型"
//                         ? CommonWidget.commonTextTile("单位", '',
//                             controller: unitCtr,
//                             showLine: true,
//                             hint: "请输入",
//                             textStyle: XFonts.size22Black0)
//                         : const SizedBox(),
//                     CommonWidget.commonTextTile("附加信息", '',
//                         controller: infoCtr,
//                         showLine: true,
//                         required: true,
//                         maxLine: 4,
//                         hint: "请输入",
//                         textStyle: XFonts.size22Black0),
//                     CommonWidget.commonTextTile("备注信息", '',
//                         controller: remarkCtr,
//                         showLine: true,
//                         required: true,
//                         maxLine: 4,
//                         hint: "请输入",
//                         textStyle: XFonts.size22Black0),
//                     CommonWidget.commonMultipleSubmitWidget(onTap1: () {
//                       Navigator.pop(context);
//                     }, onTap2: () {
//                       if (nameCtr.text.isEmpty) {
//                         ToastUtils.showMsg(msg: "请输入名称");
//                       } else if (codeCtr.text.isEmpty) {
//                         ToastUtils.showMsg(msg: "请输入代码");
//                       } else if (type == null) {
//                         ToastUtils.showMsg(msg: "请选择属性类型");
//                       } else if (infoCtr.text.isEmpty) {
//                         ToastUtils.showMsg(msg: "请输入附加信息");
//                       } else if (remarkCtr.text.isEmpty) {
//                         ToastUtils.showMsg(msg: "请输入备注信息");
//                       } else if (type == "选择型" && dictValue == null) {
//                         ToastUtils.showMsg(msg: "请选择枚举字典");
//                       } else {
//                         if (onCreate != null) {
//                           onCreate(
//                               nameCtr.text,
//                               codeCtr.text,
//                               type!,
//                               infoCtr.text,
//                               remarkCtr.text,
//                               unitCtr.text,
//                               dictValue);
//                         }
//                         Navigator.pop(context);
//                       }
//                     }),
//                   ],
//                 ),
//               );
//             }),
//           ));
//     },
//   );
// }

  static Future<void> showCreateAttrDialog(
    BuildContext context,
    List<IAuthority> authoritys,
    List<XProperty> propertys, {
    CreateAttrCallBack? onCreate,
    String name = '',
    String code = '',
    String remark = '',
    XProperty? pro,
    String? authId,
    bool public = false,
  }) async {
    TextEditingController nameCtr = TextEditingController(text: name);
    TextEditingController codeCtr = TextEditingController(text: code);
    TextEditingController remarkCtr = TextEditingController(text: remark);

    XProperty? property = pro;
    IAuthority? authority = authId != null
        ? authoritys.firstWhere((element) => element.metadata.id == authId)
        : null;
    bool isPublic = public;
    return XDialog.showFormDialog(
        context: context,
        title: "新增特性",
        children: [
          XTextField.input(
            title: "特性名称",
            controller: nameCtr,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "特性代码",
            controller: codeCtr,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XSelect.dialog(
            "选择属性",
            property?.name ?? "",
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: propertys.map((e) => e.name ?? "").toList(),
                  callback: (str) {
                // state(() {
                property =
                    propertys.firstWhere((element) => element.name == str);
                // });
              });
            },
            hint: "请选择",
          ),
          XSelect.dialog(
            "选择管理权限",
            authority?.metadata.name ?? "",
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: authoritys.map((e) => e.metadata.name ?? "").toList(),
                  callback: (str) {
                // state(() {
                authority = authoritys
                    .firstWhere((element) => element.metadata.name == str);
                // });
              });
            },
            hint: "请选择",
          ),
          XSelect.dialog(
            "向下组织公开",
            isPublic ? "公开" : '不公开',
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: ['公开', "不公开"], callback: (str) {
                // state(() {
                isPublic = str == "公开";
                // });
              });
            },
            hint: "请选择",
          ),
          XTextField.input(
            title: "特性定义",
            controller: remarkCtr,
            showLine: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () {
          if (nameCtr.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入特性名称");
          } else if (codeCtr.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入特性代码");
          } else if (property == null) {
            ToastUtils.showMsg(msg: "请选择属性");
          } else if (authority == null) {
            ToastUtils.showMsg(msg: "请选择管理权限");
          } else {
            if (onCreate != null) {
              onCreate(nameCtr.text, codeCtr.text, remarkCtr.text, property!,
                  authority!, isPublic);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showCreateWorkDialog(
      BuildContext context, List<ISpecies> thing,
      {CreateWorkCallBack? onCreate,
      required List<ITarget> share,
      String name = '',
      String remark = '',
      String code = '',
      bool allowAdd = true,
      bool allowEdit = true,
      bool allowSelect = true,
      bool isEdit = false}) async {
    TextEditingController nameCtr = TextEditingController(text: name);
    TextEditingController codeCtr = TextEditingController(text: code);
    TextEditingController remarkCtr = TextEditingController(text: remark);

    bool add = allowAdd;
    bool edit = allowEdit;
    bool select = allowSelect;

    ITarget? selectTarget;
    return XDialog.showFormDialog(
        context: context,
        isEdit: isEdit,
        children: [
          XTextField.input(
            title: "事项名称",
            controller: nameCtr,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "事项编号",
            controller: codeCtr,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XSelect.dialog(
            "选择共享组织",
            selectTarget?.metadata.name ?? "",
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: share.map((e) => e.metadata.name).toList(),
                  callback: (str) {
                // state(() {
                selectTarget =
                    share.firstWhere((element) => element.metadata.name == str);
                // });
              });
            },
            hint: "请选择",
          ),
          XSelect.dialog(
            "允许新增实体",
            add ? "是" : '否',
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: ['是', "否"], callback: (str) {
                // state(() {
                add = str == "是";
              });
              // });
            },
            hint: "请选择",
          ),
          XSelect.dialog(
            "允许变更实体",
            edit ? "是" : '否',
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: ['是', "否"], callback: (str) {
                // state(() {
                edit = str == "是";
                // });
              });
            },
            hint: "请选择",
          ),
          XSelect.dialog(
            "允许选择实体",
            select ? "是" : '否',
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: ['是', "否"], callback: (str) {
                // state(() {
                select = str == "是";
                // });
              });
            },
            hint: "请选择",
          ),
          XTextField.input(
            title: "备注",
            controller: remarkCtr,
            showLine: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () {
          if (nameCtr.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入事项名称");
          } else if (selectTarget == null) {
            ToastUtils.showMsg(msg: "请选择共享组织");
          } else if (codeCtr.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入事项编号");
          } else {
            if (onCreate != null) {
              onCreate(nameCtr.text, codeCtr.text, remarkCtr.text, add, edit,
                  select, selectTarget!);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showCreateAuthDialog(
      BuildContext context, List<ITarget> targets,
      {String name = '',
      String code = '',
      String remark = '',
      required ITarget target,
      bool public = false,
      bool isEdit = false,
      CreateAuthCallBack? callBack}) async {
    TextEditingController nameController = TextEditingController(text: name);
    TextEditingController codeController = TextEditingController(text: code);
    TextEditingController remarkController =
        TextEditingController(text: remark);

    bool isPublic = public;
    ITarget selectedTarget = target;

    return XDialog.showFormDialog(
        context: context,
        title: '权限',
        isEdit: isEdit,
        children: [
          XTextField.input(
            title: "名称",
            controller: nameController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "代码",
            controller: codeController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XSelect.dialog(
            "选择制定组织",
            selectedTarget.metadata.name,
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: targets.map((e) => e.metadata.name).toList(),
                  callback: (str) {
                // state(() {
                try {
                  selectedTarget = targets
                      .firstWhere((element) => element.metadata.name == str);
                  // ignore: empty_catches
                } catch (e) {}
                // });
              });
            },
            hint: "请选择",
          ),
          XSelect.dialog(
            "是否公开",
            isPublic ? "公开" : '不公开',
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: ['公开', "不公开"], callback: (str) {
                // state(() {
                isPublic = str == "公开";
                // });
              });
            },
            hint: "请选择",
          ),
          XTextField.input(
            title: "备注",
            controller: remarkController,
            showLine: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () {
          if (nameController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入名称");
          } else if (codeController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入代码");
          } else if (remarkController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入简介");
          } else {
            if (callBack != null) {
              callBack(nameController.text, codeController.text, selectedTarget,
                  isPublic, remarkController.text);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showClassCriteriaDialog(
      BuildContext context, List<SpeciesType> speciesTypes,
      {bool isEdit = false,
      String? name,
      String? code,
      String? resource,
      String? remark,
      String? typeName,
      CreateClassCriteriaCallBack? callBack}) async {
    TextEditingController nameController = TextEditingController(text: name);
    TextEditingController codeController = TextEditingController(text: code);
    TextEditingController remarkController =
        TextEditingController(text: remark);
    TextEditingController resourceController =
        TextEditingController(text: resource);

    String? selectedSpecies = typeName;
    return XDialog.showFormDialog(
        context: context,
        isEdit: isEdit,
        children: [
          XTextField.input(
            title: "名称",
            controller: nameController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "代码",
            controller: codeController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XSelect.dialog(
            "选择类型",
            selectedSpecies ?? "",
            showLine: true,
            required: true,
            onTap: () {
              PickerUtils.showListStringPicker(
                  navigatorKey.currentState!.context,
                  titles: speciesTypes.map((e) => e.label).toList(),
                  callback: (str) {
                // state(() {
                try {
                  selectedSpecies = speciesTypes
                      .firstWhere((element) => element.label == str)
                      .label;
                  // ignore: empty_catches
                } catch (e) {}
                // });
              });
            },
            hint: "请选择",
          ),
          XTextField.input(
            title: "资源",
            controller: resourceController,
            showLine: true,
            maxLines: 1,
            hint: "请输入",
          ),
          XTextField.input(
            title: "备注信息",
            controller: remarkController,
            showLine: true,
            required: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () {
          if (nameController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入名称");
          } else if (codeController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入代码");
          } else if (selectedSpecies == null) {
            ToastUtils.showMsg(msg: "请选择类型");
          } else if (remarkController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入备注信息");
          } else {
            if (callBack != null) {
              callBack(
                  nameController.text,
                  codeController.text,
                  selectedSpecies!,
                  remarkController.text,
                  resourceController.text);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showCreateSpeciesDialog(BuildContext context,
      {String name = '',
      String info = '',
      String remark = '',
      bool isEdit = false,
      CreateSpeciesCallBack? callBack}) async {
    TextEditingController nameController = TextEditingController(text: name);
    TextEditingController infoController = TextEditingController(text: info);
    TextEditingController remarkController =
        TextEditingController(text: remark);

    return XDialog.showFormDialog(
        context: context,
        title: '分类项',
        isEdit: isEdit,
        children: [
          XTextField.input(
            title: "名称",
            controller: nameController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "附加信息",
            controller: infoController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "备注",
            controller: remarkController,
            showLine: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () {
          if (nameController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入名称");
          } else if (infoController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入代码");
          } else {
            if (callBack != null) {
              callBack(nameController.text, infoController.text,
                  remarkController.text);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showCreateGeneralDialog(
      BuildContext context, String title,
      {String name = '',
      String code = '',
      String remark = '',
      bool isEdit = false,
      CreateSpeciesCallBack? callBack}) async {
    TextEditingController nameController = TextEditingController(text: name);
    TextEditingController codeController = TextEditingController(text: code);
    TextEditingController remarkController =
        TextEditingController(text: remark);
    return XDialog.showFormDialog(
        context: context,
        title: title,
        isEdit: isEdit,
        children: [
          XTextField.input(
            title: "名称",
            controller: nameController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "代码",
            controller: codeController,
            showLine: true,
            required: true,
            hint: "请输入",
          ),
          XTextField.input(
            title: "备注",
            controller: remarkController,
            showLine: true,
            maxLines: 4,
            hint: "请输入",
          )
        ],
        onOk: () {
          if (nameController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入名称");
          } else if (codeController.text.isEmpty) {
            ToastUtils.showMsg(msg: "请输入代码");
          } else {
            if (callBack != null) {
              callBack(nameController.text, codeController.text,
                  remarkController.text);
            }
            Navigator.pop(context);
          }
        });
  }

  static Future<void> showVersionItemDetail(
      BuildContext context, VersionItemWidget versionItem) {
    return showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            alignment: Alignment.centerLeft,
            child: SingleChildScrollView(
              child: StatefulBuilder(builder: (context, state) {
                return Container(
                    // width: 350,
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 20),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 40,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    width: 80,
                                    child: Text(
                                      '版本号:',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Text(versionItem.version),
                                ]),
                          ),
                          SizedBox(
                            height: 40,
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    width: 80,
                                    child: Text(
                                      '更新时间:',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Text(versionItem.date/*.dateTimeFormat*/)
                                ]),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 8),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    width: 80,
                                    child: Text(
                                      '更新内容:',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      maxLines: 20,
                                      versionItem.content, // 文本内容
                                      overflow: TextOverflow.visible,
                                      style:
                                          const TextStyle(fontSize: 14), // 文本样式
                                    ),
                                  )
                                ]),
                          )
                        ]));
              }),
            ),
          );
        });
  }

  /// 表单弹框
  static Future<T?> showFormDialog<T>(
      {BuildContext? context,
      String? title,
      bool? isEdit = false,
      required List<Widget> children,
      void Function()? onOk}) {
    return null != navigatorKey.currentContext
        ? showDialog<T>(
            context: context ?? navigatorKey.currentContext!,
            builder: (context) {
              return Center(
                child: Material(
                  child: Container(
                      width: UIConfig.screenWidth - 32,
                      padding: const EdgeInsets.all(16),
                      child: SingleChildScrollView(
                        child: StatefulBuilder(builder: (context, state) {
                          return SizedBox(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                headInfoWidget(null != isEdit
                                    ? (isEdit ? "编辑$title" : "新增$title")
                                    : title ?? ''),
                                ...children,
                                _commonMultipleSubmitWidget(
                                    onTap1: () {
                                      Navigator.pop(context);
                                    },
                                    onTap2: onOk),
                              ],
                            ),
                          );
                        }),
                      )),
                ),
              );
            },
            useSafeArea: true,
          )
        : Future(() => null);
  }

  /// 从底部升起弹框
  static Future<T?> showFormBottomSheet<T>(
      {BuildContext? context,
      String? title,
      bool isEdit = false,
      required List<Widget> children,
      void Function()? onOk}) {
    return null != navigatorKey.currentContext
        ? showModalBottomSheet(
            isScrollControlled: true,
            context: context ?? navigatorKey.currentContext!,
            backgroundColor: Colors.white,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10))),
            builder: (context) {
              return SingleChildScrollView(
                child: StatefulBuilder(builder: (context, state) {
                  return AnimatedPadding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context)
                            .viewInsets
                            .bottom), // 我们可以根据这个获取需要的padding
                    duration: const Duration(milliseconds: 100),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              const SizedBox(height: 10),
                              headInfoWidget(
                                title ?? (isEdit ? "编辑" : "新建"),
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                        _commonMultipleSubmitWidget(
                          onTap1: () {
                            Navigator.pop(context);
                          },
                          onTap2: onOk,
                        ),
                      ],
                    ),
                  );
                }),
              );
            },
          )
        : Future(() => null);
  }

  /// 从底部升起弹框列表
  // static Future<T?> showListBottomSheet<T>(
  //     {BuildContext? context,
  //       String? title,
  //       required List<Widget> children,
  //       void Function()? onOk}) {
  //   return null != navigatorKey.currentContext
  //       ? showModalBottomSheet(
  //     isScrollControlled: true,
  //     context: context ?? navigatorKey.currentContext!,
  //     backgroundColor: Colors.white,
  //     shape: const RoundedRectangleBorder(
  //         borderRadius: BorderRadius.only(
  //             topLeft: Radius.circular(10),
  //             topRight: Radius.circular(10))),
  //     builder: (context) {
  //       return SingleChildScrollView(
  //         child: StatefulBuilder(builder: (context, state) {
  //           return AnimatedPadding(
  //             padding: EdgeInsets.only(
  //                 bottom: MediaQuery.of(context)
  //                     .viewInsets
  //                     .bottom), // 我们可以根据这个获取需要的padding
  //             duration: const Duration(milliseconds: 100),
  //             child: Column(
  //               mainAxisSize: MainAxisSize.min,
  //               children: [
  //                 Row(
  //                   children: [
  //                     Text('取消', style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14),),
  //                     Text(title!, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),),
  //                     Text('筛选', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),)
  //                   ],
  //                 ),
  //                 SingleChildScrollView(
  //                   child: ListView(
  //                       children: children),
  //                 ),
  //               ],
  //             ),
  //           );
  //         }),
  //       );
  //     },
  //   )
  //       : Future(() => null);
  // }

  //标题信息
  static headInfoWidget(
    String info, {
    Widget? action,
    Color? color,
  }) {
    return Container(
      padding: const EdgeInsets.only(left: 10),
      color: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: null != action
                  ? MainAxisAlignment.start
                  : MainAxisAlignment.center,
              children: [
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                          text: info,
                          style: TextStyle(
                              fontSize: 22.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.w600))
                    ],
                  ),
                ),
              ],
            ),
          ),
          action ??
              XButton.iconText(
                icon: IconWidget.icon(
                  Icons.close,
                  color: Colors.black,
                ),
                onPressed: () =>
                    Navigator.pop(navigatorKey.currentState!.context),
              ),
        ],
      ),
    );
  }

  //两个按钮
  static Widget _commonMultipleSubmitWidget(
      {String str1 = "取消",
      String str2 = "确定",
      VoidCallback? onTap1,
      VoidCallback? onTap2}) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 15.h),
      color: Colors.white,
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: onTap1,
            child: Container(
              width: 200.w,
              height: 50.h,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4.w),
                  border: Border.all(color: XColors.themeColor)),
              alignment: Alignment.center,
              child: Text(
                str1,
                style: TextStyle(color: XColors.themeColor, fontSize: 16.sp),
              ),
            ),
          ),
          GestureDetector(
            onTap: onTap2,
            child: Container(
              width: 200.w,
              height: 50.h,
              decoration: BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.circular(4.w),
              ),
              alignment: Alignment.center,
              child: Text(
                str2,
                style: TextStyle(color: Colors.white, fontSize: 16.sp),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
