import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/dart/extension/index.dart';

class XButton extends StatelessWidget {
  final String? text;

  /// 图标
  final Widget? icon;
  final VoidCallback? onPressed;
  // final VoidCallback? onLongPress;
  // final ValueChanged<bool>? onHover;
  // final ValueChanged<bool>? onFocusChange;
  final ButtonStyle? style;
  // final FocusNode? focusNode;
  final Color? color;
  // final Color? bgcolor;
  // 按钮是否高亮显示（高亮：红色）
  final bool highlight;
  final bool autofocus = false;
  final bool iconLeft;

  ///按钮高度
  final double? height;

  /// 按钮左右间距
  final double? padding;
  final Clip clipBehavior = Clip.none;
  final TextStyle? textStyle;

  const XButton(this.text,
      {super.key,
      this.icon,
      this.onPressed,
      this.color,
      this.highlight = false,
      this.iconLeft = true,
      this.style,
      this.textStyle,
      this.padding,
      this.height});

  // 文本按钮
  XButton.link(this.text,
      {Key? key,
      this.onPressed,
      this.highlight = false,
      this.iconLeft = true,
      this.textStyle =
          const TextStyle(fontSize: 18, fontWeight: FontWeight.bold)})
      : color = null,
        icon = null,
        style = ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.transparent),
          minimumSize: MaterialStateProperty.all(const Size(10, 30)),
          side: MaterialStateProperty.all(
            BorderSide.none,
          ),
          shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(6))),
        ),
        height = null,
        padding = null,
        super(key: key);

  /// 目前使用到的地方首页，动态
  XButton.iconText({super.key, this.text, this.icon, this.onPressed})
      : height = null,
        padding = null,
        highlight = false,
        iconLeft = true,
        color = Colors.black,
        style = ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.white),
            side: MaterialStateProperty.all(BorderSide.none),
            padding: MaterialStateProperty.all(const EdgeInsets.all(0))),
        textStyle = null;

  XButton.iconCustomText({super.key, this.text, this.icon, Color? bgcolor, required this.iconLeft, Color? textColor, this.onPressed})
      : height = null,
        padding = null,
        highlight = false,
        // iconLeft = isiconLeft,
        color = textColor,
        style = ButtonStyle(
            backgroundColor: MaterialStateProperty.all(bgcolor),
            side: MaterialStateProperty.all(BorderSide.none),
            padding: MaterialStateProperty.all(const EdgeInsets.all(0))),
        textStyle = null;

  /// 空心按钮（小的）
  XButton.hollowMin(this.text,
      {Key? key,
      this.icon,
      this.onPressed,
      this.highlight = false,
      this.iconLeft = true,
      this.padding = 30,
      this.textStyle = const TextStyle(fontSize: 18, color: Colors.black)})
      : color = null,
        style = ButtonStyle(
          padding: MaterialStateProperty.all(
              EdgeInsets.only(left: padding!, right: padding)),
          backgroundColor: MaterialStateProperty.all(Colors.transparent),
          minimumSize: MaterialStateProperty.all(const Size(10, 30)),
          side: MaterialStateProperty.all(
            BorderSide(
                color: textStyle?.color ?? Colors.black,
                width: 1.0), // 设置边框颜色和宽度
          ),
          shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(6))),
        ),
        height = 40,
        super(key: key);

  /// 实心按钮（小的）
  ///
  const XButton.solidMin(this.text,
      {Key? key,
      this.icon,
      this.padding,
      this.highlight = false,
      this.iconLeft = true,
      this.onPressed,
      this.textStyle = const TextStyle(fontSize: 18, color: Colors.white)})
      : color = null,
        style = null,
        height = 38,
        super(key: key);

  /// 空心按钮（小的）
  XButton.hollow(this.text,
      {Key? key,
      this.icon,
      this.padding,
      this.highlight = false,
      this.iconLeft = true,
      this.onPressed,
      this.textStyle = const TextStyle(fontSize: 18, color: Colors.black)})
      : color = null,
        style = ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.transparent),
          minimumSize: MaterialStateProperty.all(const Size(10, 30)),
          shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(6))),
        ),
        height = 48,
        super(key: key);

  /// 实心按钮（中的）
  const XButton.solid(this.text,
      {Key? key,
      this.icon,
      this.iconLeft = true,
      this.onPressed,
      this.highlight = false,
      this.padding,
      this.textStyle = const TextStyle(fontSize: 18, color: Colors.white)})
      : color = null,
        style = null,
        height = 48,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Color defColor = highlight ? Colors.red : Theme.of(context).primaryColor;
    ButtonStyle defStyle = ButtonStyle(
      padding: null == padding
          ? null
          : MaterialStateProperty.all(
              EdgeInsets.only(left: padding!, right: padding!)),
      backgroundColor: MaterialStateProperty.all(color ?? defColor),
      side: MaterialStateProperty.all(BorderSide.none),
      shape: MaterialStateProperty.all(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(6))),
    );
    var textColor = color;
    if (null == icon && null == textColor) {
      textColor = defColor;
    }
    Widget? content;
    if (null != text && null != icon) {
      if (iconLeft) {
        content = <Widget>[
          if (null != icon) icon!,
          if (null != text && text!.isNotEmpty)
            Text(
              text!,
              style: textStyle ??
                  TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.sp,
                      color: textColor ?? Colors.black87),
            )
        ].toRow();
      } else {
        content = <Widget>[
          if (null != text && text!.isNotEmpty)
            Text(
              text!,
              style: textStyle ??
                  TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.sp,
                      color: textColor ?? Colors.black87),
            ),
          if (null != icon) icon!,
        ].toRow();
      }
    } else if (null != text) {
      content = Text(
        text!,
        style: textStyle ??
            TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.sp,
                color: textColor ?? Colors.black87),
      );
    } else if (null != icon) {
      content = icon;
    }

    return SizedBox(
        height: height ?? 56.h,
        child: OutlinedButton(
          key: key,
          onPressed: onPressed,
          // onLongPress: onLongPress,
          // onHover: onHover,
          // onFocusChange: onFocusChange,
          style: style ?? defStyle,
          // focusNode: focusNode,
          autofocus: autofocus,
          clipBehavior: clipBehavior,
          child: content,
        ));
  }
}
