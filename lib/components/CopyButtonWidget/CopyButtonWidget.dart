import 'package:flutter/material.dart';

import '../../config/theme/unified_style.dart';
import '../../main.dart';
import '../../utils/system/system_utils.dart';
import '../XButton/XButton.dart';
import '../XImage/XImage.dart';
import '../XImage/components/icon.dart';
import '../XImage/components/icons.dart';

class CopyButtonWidget extends StatelessWidget {
  const CopyButtonWidget({super.key, required this.content});
  final String content;
  @override
  Widget build(BuildContext context) {
    return _buildMainView();
  }

  _buildMainView() {
    return XButton.iconText(
      icon: IconWidget.svg(
        IconsUtils.icons['x']?[XImage.copyOutline],
        height: 18,
        width: 14,
        color: XColors.primary,
      ),
      // height: 30,
      // width: 30,
      onPressed: () => SystemUtils.copyToClipboard(content,
          successMessage: '复制成功', context: navigatorKey.currentContext),
    );
  }
}
