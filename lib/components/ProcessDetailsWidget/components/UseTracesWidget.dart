import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/dart/extension/index.dart';
import 'package:orginone/components/XText/target_text.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:timeline_tile/timeline_tile.dart';
import '../../../dart/core/work/task.dart';

Map<int, Status> statusMap = {
  1: Status(Colors.blue, '待处理'),
  100: Status(Colors.green, '已同意'),
  200: Status(Colors.red, '已拒绝'),
  102: Status(Colors.green, '已发货'),
  220: Status(Colors.red, '买方取消订单'),
  221: Status(Colors.red, '买方取消订单'),
  222: Status(Colors.red, '已退货'),
  240: Status(Colors.red, '已取消'),
  230: Status(Colors.red, '已退回'),
  250: Status(Colors.red, '未定义状态'),
};

class Status {
  late Color color;
  late String text;

  Status(this.color, this.text);
}

///历史痕迹页面
class UseTracesWidget extends StatefulWidget {
  IWorkTask? todo;
  UseTracesWidget({super.key, required this.todo});

  @override
  State<UseTracesWidget> createState() => _UseTracesState();
}

class _UseTracesState extends State<UseTracesWidget> {
  // IWorkTask? todo;
  XWorkInstance? flowInstance;

  @override
  void initState() {
    // todo = RoutePages.routeData.currPageData.data['todo'];
    flowInstance = widget.todo?.instance;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _timeLine();
  }

  Widget _timeLine() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 10.w),
      height: double.infinity,
      color: XColors.backgroundColor,
      child: Builder(builder: (context) {
        int length = flowInstance?.historyTasks?.length ?? 0;
        return ListView.builder(
          itemCount: length,
          itemBuilder: (context, index) {
            return Container(
              child: _buildTimelineTile(
                index,
                flowInstance!.historyTasks![index],
              ),
            );
          },
        );
      }),
    );
  }

  Widget _buildTimelineTile(int index, XWorkTaskHistory task) {
    bool isLast =
        index == flowInstance!.historyTasks!.length - 1 ? true : false;
    // LogUtil.e("_buildTimelineTile");
    // LogUtil.e(task.toJson());
    return TimelineTile(
      isFirst: index == 0 ? true : false,
      isLast: isLast,
      indicatorStyle: IndicatorStyle(
        width: 15.w,
        height: 15.w,
        color: XColors.themeColor,
        indicatorXY: 0,
      ),
      afterLineStyle: const LineStyle(thickness: 1, color: XColors.themeColor),
      beforeLineStyle: const LineStyle(thickness: 1, color: XColors.themeColor),
      endChild: Container(
        margin: EdgeInsets.only(left: 15.h),
        child: Card(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 10.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(DateTime.tryParse(task.createTime ?? "")
                            ?.format(format: "yyyy-MM-dd HH:mm:ss") ??
                        ""),
                    Text(
                      task.status != null && task.status! > 1
                          ? '审批结果：${statusMap[task.status]!.text}'
                          : statusMap[task.status]!.text,
                      style: TextStyle(color: statusMap[task.status]!.color),
                    ),
                  ],
                ),
                task.status != null && task.status! > 1
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text('审批人：'),
                          Expanded(
                              child: TargetText(
                            userId:
                                task.records == null || task.records!.isEmpty
                                    ? task.createUser ?? ''
                                    : task.records!.first.createUser ??
                                        '', //task.createUser ?? ''
                          )),
                        ],
                      )
                    : const SizedBox(),
                Text("审核节点:${task.node?.nodeType ?? ""}"),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        child: Text(
                      "审批意见:${task.records?.last.comment ?? ""}",
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    )),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
