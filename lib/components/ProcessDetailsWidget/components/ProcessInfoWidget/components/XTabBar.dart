import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../../config/theme/unified_style.dart';

class XTabBar extends StatelessWidget {
  final TabController? tabController;
  final List<String> tabTitle;
  final ValueChanged<int>? onTap;
  final TextStyle? labelStyle;

  //没有指示器的tabbar
  const XTabBar(this.tabController, this.tabTitle,
      {super.key, this.onTap, this.labelStyle});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
              bottom: BorderSide(color: Colors.grey.shade200, width: 0.5))),
      alignment: Alignment.centerLeft,
      child: TabBar(
        controller: tabController,
        tabs: tabTitle.map((e) {
          return Tab(
            text: e,
            height: 50.h,
          );
        }).toList(),
        unselectedLabelColor: Colors.grey,
        unselectedLabelStyle: labelStyle ?? TextStyle(fontSize: 18.sp),
        labelColor: XColors.themeColor,
        labelStyle: labelStyle ?? TextStyle(fontSize: 18.sp),
        isScrollable: true,
        indicator: const BoxDecoration(),
        onTap: onTap,
      ),
    );
  }
}
