import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/main.dart';

typedef DocumentOperation = Function(dynamic type, String data);

class XDocument extends StatelessWidget {
  final List<String> title;
  final List<List<String>> content;
  final double? contentWidth;
  final bool showOperation;
  final DocumentOperation? onOperation;
  final List<PopupMenuItem>? popupMenus;

  //文档控件
  const XDocument({
    super.key,
    required this.title,
    required this.content,
    this.contentWidth,
    this.showOperation = false,
    this.onOperation,
    this.popupMenus,
  });
  @override
  Widget build(BuildContext context) {
    List<List<String>> data = [];

    for (int i = 0; i < title.length; i++) {
      List<String> key = [];
      if (key.isEmpty) {
        key.add(title[i]);
      }
      for (var value in content) {
        String data;
        if (value.length - 1 < i) {
          data = '';
        } else {
          data = value[i];
        }
        key.add(data);
      }
      data.add(key);
    }
    if (showOperation) {
      var operation = List.generate(
          data.first.length, (index) => index == 0 ? "操作" : index.toString());
      data.add(operation);
    }

    Widget titleWidget(String title) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        alignment: Alignment.center,
        constraints: BoxConstraints(
            minWidth: contentWidth ?? 60.w,
            maxWidth: title == "操作" ? 60.w : 170.w),
        decoration: BoxDecoration(
            border: Border(
                right: BorderSide(color: Colors.grey.shade200, width: 0.5))),
        height: 50.h,
        child: Text(
          title,
          style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w600,
              fontSize: 18.sp),
        ),
      );
    }

    Widget contentWidget(String content) {
      return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 10.w),
        constraints:
            BoxConstraints(minWidth: contentWidth ?? 60.w, maxWidth: 170.w),
        height: 50.h,
        decoration: BoxDecoration(
            border: Border(
                right: BorderSide(color: Colors.grey.shade200, width: 0.5))),
        child: Text(
          content,
          style: TextStyle(color: Colors.grey, fontSize: 18.sp),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      );
    }

    return Container(
      color: Colors.white,
      width: double.infinity,
      child: Scrollbar(
        // thumbVisibility: true,
        // trackVisibility: true,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: data.map((title) {
                return Column(
                  children: title.map((str) {
                    int index = title.indexOf(str);
                    if (index == 0) {
                      return titleWidget(str);
                    }
                    if (showOperation &&
                        (data.indexOf(title) == data.length - 1)) {
                      double x = 0;
                      double y = 0;
                      return GestureDetector(
                        onPanDown: (position) {
                          x = position.globalPosition.dx;
                          y = position.globalPosition.dy;
                        },
                        onTap: () async {
                          await showMenu(
                            context: navigatorKey.currentState!.context,
                            items: popupMenus ?? [],
                            position: RelativeRect.fromLTRB(
                              x,
                              y - 50,
                              MediaQuery.of(navigatorKey.currentState!.context)
                                      .size
                                      .width -
                                  x,
                              0,
                            ),
                          ).then((value) {
                            if (value != null) {
                              if (onOperation != null) {
                                onOperation?.call(value, data[0][index]);
                              }
                            }
                          });
                        },
                        child: SizedBox(
                          width: 40.w,
                          height: 50.h,
                          child: const Icon(Icons.more_horiz),
                        ),
                      );
                    }
                    return contentWidget(str);
                  }).toList(),
                );
              }).toList()),
        ),
      ),
    );
  }
}
