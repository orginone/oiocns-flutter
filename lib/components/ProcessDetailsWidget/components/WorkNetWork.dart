import 'dart:ui';
import 'package:orginone/components/Tip/ToastUtils.dart';
import 'package:orginone/dart/core/work/task.dart';

class WorkNetWork {
  static Future<void> approvalTask(
      {required IWorkTask todo,
      required int status,
      String? comment,
      String? backId,
      bool? isSkip,
      Map<String, List<String>>? gatewayData,
      VoidCallback? onSuccess}) async {
    try {
      bool success = await todo.approvalTask(status, comment: comment/*, gatewayData: gatewayData*/, backId: backId, isSkip: isSkip);
      if (success) {
        ToastUtils.showMsg(msg: "操作成功");
        await Future.delayed(const Duration(milliseconds: 100));
        if (onSuccess != null) {
          onSuccess();
        }
      }
    } catch (e) {
      ToastUtils.showMsg(msg: "审核异常");
    }
  }
}
