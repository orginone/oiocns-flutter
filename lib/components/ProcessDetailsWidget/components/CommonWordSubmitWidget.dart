import 'package:flutter/material.dart';
import 'package:orginone/components/XTextField/XTextField.dart';
import 'package:orginone/dart/extension/index.dart';
import 'package:orginone/config/theme/space.dart';
import 'package:orginone/dart/core/work/task.dart';

//审批常用语编辑提交组件
class CommonWordSubmitWidget extends StatelessWidget {
  CommonWordSubmitWidget({
    super.key,
    required this.todo,
  });

  final IWorkTask? todo;
  TextEditingController comment = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return _buildMainView(context);
  }

  _buildMainView(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SingleChildScrollView(
        child: <Widget>[
          _editApplyOption(),
          _saveCommonOptionFlag(),
        ].toColumn(crossAxisAlignment: CrossAxisAlignment.start),
      ).paddingHorizontal(AppSpace.page),
    );
  }

  Widget _editApplyOption() {
    return XTextField.input(
      title: "备注",
      required: true,
      // backgroundColor: XColors.bgColor,
      controller: comment,
      hint: "请输入审批意见",
      maxLines: 4,
    ).paddingTop(AppSpace.listItem);
  }

  Widget _saveCommonOptionFlag() {
    return XTextField.input(
      title: "备注",
      required: true,
      // backgroundColor: XColors.bgColor,
      controller: comment,
      hint: "请输入审批意见",
      maxLines: 4,
    ).paddingTop(AppSpace.listItem);
  }
}
