import 'package:flutter/material.dart';
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';

import 'StandardEntityInfoWidget/components/StandardEntityBaseInfoWidget.dart';

// ignore: must_be_immutable
class EntityInfoWidget extends XStatefulWidget {
  EntityInfoWidget({super.key, super.data});

  @override
  State<EntityInfoWidget> createState() => _EntityInfoState();
}

class _EntityInfoState extends XStatefulState<EntityInfoWidget, dynamic> {
  @override
  Widget buildWidget(BuildContext context, dynamic data) {
    return _buildView(context, data);
  }

  // 主视图
  Widget _buildView(BuildContext context, data) {
    if (data == null || data.metadata == null) {
      return const Text('数据异常');
    }

    // ignore: prefer_typing_uninitialized_variables
    var item;

    // if (data is Property) {
    //   item = data.metadata.toJson();
    //   LogUtil.d(item);
    // } else if (data is Species) {
    //   item = data.metadata.toJson();
    //   LogUtil.d(item);
    // } else {
    //   item = data.metadata.toJson();
    // }

    item = data.metadata.toJson();
    return StandardEntityBaseInfoWidget(
      item: item,
      belong: data.belong,
    );
  }
}
