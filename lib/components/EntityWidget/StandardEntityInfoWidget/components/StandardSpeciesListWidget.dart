import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orginone/dart/extension/index.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/space.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/core/public/consts.dart';
import 'package:orginone/main.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../../../XText/XText.dart';

class StandardSpeciesListWidget<T> extends StatefulWidget {
  const StandardSpeciesListWidget({
    super.key,
    required this.data,
  });
  final T data;
  @override
  State<StandardSpeciesListWidget> createState() => _StandardSpeciesListState();
}

class _StandardSpeciesListState extends State<StandardSpeciesListWidget> {
  get data => widget.data;
  get list => widget.data.speciesItems;
  @override
  Widget build(BuildContext context) {
    return _buildView();
  } //构建infoView

  _buildView() {
    XLogUtil.d('StandardSpeciesListWidget');
    XLogUtil.d(data.toJson());
    if (data == null || list == null || list.isEmpty) {
      return const SizedBox();
    }
    return <Widget>[XText.boxTitle('${data.typeName ?? ''}项'), _buildListView()]
        .toColumn();
  }

  _buildListView() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: list.length,
        itemBuilder: (_, int index) {
          var item = list[index];
          return _item(item, index);
        });
  }

  _item(dynamic item, int index) {
    return <Widget>[
      itemWidget('名称', '${item.name ?? ''}', rowCount: 2, item: item.toJson()),
      itemWidget('编号', '${item.code ?? '--'}',
          rowCount: 2, item: item.toJson()),
      itemWidget('信息', '${item.info ?? ''}', rowCount: 1, item: item.toJson()),
      itemWidget('归属', '${item.name ?? ''}',
          rowCount: 1, userId: '${item.belongId ?? ''}', item: item.toJson()),
      itemWidget('创建人', '${item.name ?? ''}',
          rowCount: 1, userId: '${item.updateUser ?? ''}', item: item.toJson()),
      itemWidget('创建时间', '${item.createTime ?? ''}',
          rowCount: 1, item: item.toJson()),
      itemWidget('备注', '${item.remark ?? ''}',
          rowCount: 1, item: item.toJson()),
    ]
        .toWrap(runSpacing: AppSpace.listItem)
        .marginSymmetric(horizontal: AppSpace.page, vertical: AppSpace.page)
        .backgroundColor(XColors.lightPrimary)
        .marginOnly(
            top: AppSpace.page, left: AppSpace.page, right: AppSpace.page)
        .borderRadius(all: 10)
        .clipRRect(all: 10, topLeft: 10);
  }

  itemWidget(String title, String value,
      {int rowCount = 2, ShareIcon? icon, Map? item, String? userId}) {
    bool hasIcon = icon != null || userId != null;
    ShareIcon share = shareIcon(userId ?? '');
    return <Widget>[
      <Widget>[
        XText.fieldName(
          '$title:',
          // style: const TextStyle(color: XColors.gray_66),
        ),
        userId == null
            ? const SizedBox()
            : XImage.entityIcon(userId, entityId: userId, height: 20)
                .paddingLeft(3),
        XText.fieldValue(
          hasIcon ? share.name : value,
          highlight: userId != null,
          overflow: TextOverflow.clip,
        ).paddingLeft(3).width((Get.width - AppSpace.page * 4) / rowCount -
            (hasIcon ? 18 + 6 : 0) -
            title.length * 18),
      ].toRow(crossAxisAlignment: CrossAxisAlignment.center),
    ]
        .toColumn(crossAxisAlignment: CrossAxisAlignment.start)
        .width((Get.width - AppSpace.page * 4) / rowCount); //
  }

  ShareIcon shareIcon(String id) {
    if (ShareIdSet.containsKey(id)) {
      return ShareIdSet[id] ?? ShareIcon(name: "--", typeName: "未知类型");
    }

    return relationCtrl.user?.findShareById(id) ??
        ShareIcon(name: "--", typeName: "未知类型");
  }
}
