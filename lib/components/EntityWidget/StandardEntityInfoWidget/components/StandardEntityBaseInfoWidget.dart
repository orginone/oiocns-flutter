import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orginone/dart/extension/index.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/space.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/public/entity.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../../../CopyButtonWidget/CopyButtonWidget.dart';
import '../../../XText/XText.dart';

class StandardEntityBaseInfoWidget extends StatefulWidget {
  const StandardEntityBaseInfoWidget(
      {super.key, @required this.item, @required this.belong});
  final dynamic item;
  final dynamic belong;
  @override
  State<StandardEntityBaseInfoWidget> createState() =>
      _StandardEntityBaseInfoState();
}

class _StandardEntityBaseInfoState extends State<StandardEntityBaseInfoWidget> {
  @override
  Widget build(BuildContext context) {
    return _buildInfoView();
  } //构建infoView

  _buildInfoView() {
    var item = widget.item;
    var belong = widget.belong;
    return <Widget>[
      widgets(item, '名称', 'name', required: true, allowCopy: true),
      widgets(item, '代码', 'code', required: true, allowCopy: true),
      widgets(item, '类型', 'valueType', required: true),
      // itemWidget('类型', item?.valueType ?? '-', required: true),
      widgets(item, '附加信息', 'info'),
      widgets(
        item,
        '归属',
        'belong',
        // icon: belong,
        userId: item['belongId'] ?? '-',
      ),

      widgets(
        item,
        '创建人',
        'createUser',
        userId: item['createUser'],
      ),
      widgets(
        item,
        '创建时间',
        'createTime',
      ),
      widgets(
        item,
        '更新人',
        'updateUser',
        userId: item['updateUser'] ?? '-',
      ),
      widgets(
        item,
        '更新时间',
        'updateTime',
      ),
      widgets(item, '备注', 'remark', rowCount: 1),
    ]
        .toWrap(runSpacing: AppSpace.paragraph)
        .paddingAll(AppSpace.page)
        .backgroundColor(XColors.white);
  }

  widgets(Map item, String title, String key,
      {bool required = false,
      bool allowCopy = false,
      int rowCount = 2,
      ShareIcon? icon,
      String? userId}) {
    //  data.belong != null && data.belong is ShareIcon
    //     ? (data.belong as ShareIcon).name
    //     : '-',
    var value = item[key] ?? '-';
    // LogUtil.d(key);
    // LogUtil.d(value);
    if (value != null && key.contains('belong')) {
      XLogUtil.d(value);
      value = widget.belong.name;
    }

    if (key.contains('User')) {
      value = ShareIdSet[item[key]] != null && ShareIdSet[item[key]] is XTarget
          ? ShareIdSet[item[key]].name
          : '-';
    }

    bool hasKey = item.containsKey(key);
    if (!hasKey) {
      return const SizedBox();
    }
    return itemWidget(title, value,
        required: required,
        allowCopy: allowCopy,
        rowCount: rowCount,
        icon: icon,
        userId: userId,
        item: item);
  }

  itemWidget(String title, String value,
      {bool required = false,
      bool allowCopy = false,
      int rowCount = 2,
      ShareIcon? icon,
      Map? item,
      String? userId}) {
    bool hasIcon = icon != null || userId != null;
    return <Widget>[
      _buildItemTitle(required, title),
      _buildItemContent(userId, value, rowCount, hasIcon, allowCopy: allowCopy),
    ]
        .toColumn(crossAxisAlignment: CrossAxisAlignment.start)
        .width((Get.width - AppSpace.page * 2) / rowCount);
  }

  Widget _buildItemContent(
    String? userId,
    String value,
    int rowCount,
    bool hasIcon, {
    bool allowCopy = false,
  }) {
    double rowW = (Get.width - AppSpace.page * 2) / rowCount;
    double iconW = hasIcon ? 20 + 6 : 0;
    double copyW = allowCopy ? 30 + 6 : 0;
    double textW = rowW - iconW - copyW;

    return <Widget>[
      userId == null
          ? const SizedBox()
          : XImage.entityIcon(widget.belong, entityId: userId, height: 20),
      <Widget>[
        XText.fieldValue(
          value,
          // color: userId != null ? XColors.primary : XColors.gray_66,
          highlight: userId != null,
          overflow: TextOverflow.clip,
        ).width(textW),
        allowCopy ? CopyButtonWidget(content: value) : const SizedBox()
      ].toRow(),
    ].toRow(crossAxisAlignment: CrossAxisAlignment.center).width(rowW);
  }

  Widget _buildItemTitle(bool required, String title) {
    return <Widget>[
      XText.fieldName(
        title,
        requiredField: required,
        // style: const TextStyle(color: XColors.gray_66),
      ),
    ].toRow().paddingBottom(AppSpace.listRow);
  }
}
