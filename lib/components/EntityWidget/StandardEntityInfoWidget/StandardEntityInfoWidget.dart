import 'package:flutter/material.dart';
import 'package:orginone/dart/extension/index.dart';
import 'package:orginone/components/XStatefulWidget/XStatefulWidget.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/thing/standard/index.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../../XText/XText.dart';
import 'components/StandardEntityBaseInfoWidget.dart';
import 'components/StandardSpeciesListWidget.dart';

// ignore: must_be_immutable
class StandardEntityInfoWidget extends XStatefulWidget {
  StandardEntityInfoWidget({super.key, super.data});

  @override
  State<StandardEntityInfoWidget> createState() => _StandardEntityInfoState();
}

class _StandardEntityInfoState
    extends XStatefulState<StandardEntityInfoWidget, dynamic> {
  @override
  Widget buildWidget(BuildContext context, dynamic data) {
    return _buildMainView(context, data);
  }

  // 主视图
  Widget _buildMainView(BuildContext context, data) {
    if (data == null || data.metadata == null) {
      return const Text('数据异常');
    }

    // ignore: prefer_typing_uninitialized_variables
    var item;
    // if (data is Property) {
    //   item = data.metadata.toJson();
    //   LogUtil.d(item);
    // } else if (data is Species) {
    //   item = data.metadata.toJson();
    //   LogUtil.d(item);
    // } else {
    //   item = data.metadata.toJson();
    // }

    item = data.metadata.toJson();

    if (data is Species) {
      //如果是字典和分类项目  加载loadContent
      return FutureBuilder(
        future: data.loadContent(reload: true),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator()
                .center()
                .backgroundColor(XColors.white);
          }
          if (snapshot.connectionState != ConnectionState.done &&
              !snapshot.hasData) {
            return Container();
          }
          XLogUtil.d('FutureBuilder');
          // LogUtil.d('-------------------');
          // LogUtil.d(item);
          XSpecies species = XSpecies.fromJson(item);
          species.speciesItems = data.items;
          // LogUtil.d('-------------------');
          // LogUtil.d(item);
          item = species.toJson();
          // LogUtil.d('-------------------');
          XLogUtil.d(item);

          return _buildBody(data, item, species);
        },
      );
    }
    return _buildBody(data, item, XSpecies.fromJson(item));
  }

  _buildBody(dynamic data, dynamic item, XSpecies species) {
    return SingleChildScrollView(
        child: <Widget>[
      XText.boxTitle('${data.typeName ?? ''}${[data.name ?? '']}基本信息'),
      StandardEntityBaseInfoWidget(
        item: item,
        belong: data.belong,
      ),
      StandardSpeciesListWidget(
        data: species,
      )
    ].toColumn().backgroundColor(XColors.white));
  }
}
