import 'package:flutter/foundation.dart';

abstract class EntitySwitchModel<T> extends ChangeNotifier {
  String title;
  T? _currData;
  List<T> _datas = [];

  EntitySwitchModel(this.title);

  T? get currData => _currData;
  List<T> get datas => _datas;

  Future<T?> initData() async {
    _datas = await getInitDatas();
    return _currData ??= await getInitData();
  }

  set currData(T? currData) {
    _currData = currData;
    setCurrData(currData);
    notifyListeners();
  }

  set datas(List<T> datas) {
    _datas.addAll(datas);
    notifyListeners();
  }

  void setCurrData(T? currData);

  Future<T?> getInitData();
  Future<List<T>> getInitDatas();
}
