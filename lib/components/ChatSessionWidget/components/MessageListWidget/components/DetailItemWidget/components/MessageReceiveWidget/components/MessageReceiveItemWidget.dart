import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:orginone/dart/extension/index.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/space.dart';
import 'package:orginone/config/theme/text.dart';
import 'package:orginone/dart/base/schema.dart';

class MessageReceiveItemWidget extends StatelessWidget {
  const MessageReceiveItemWidget({
    super.key,
    required this.target,
    required this.hint,
  });

  final XTarget target;
  final String hint;

  @override
  Widget build(BuildContext context) {
    return _buildView();
  }

  Widget _buildView() {
    return <Widget>[
      XImage.entityIcon(target, width: 40)
          .clipRRect(all: 8.w)
          .marginOnly(right: AppSpace.listItem),
      <Widget>[
        Text(target.name),
        Text(
          hint,
          maxLines: 1,
          style: AppTextStyles.labelMedium?.copyWith(color: Colors.grey),
          overflow: TextOverflow.ellipsis,
        )
      ]
          .toColumn(crossAxisAlignment: CrossAxisAlignment.start)
          .paddingVertical(AppSpace.listItem)
          .border(bottom: 0.5, color: Colors.grey.shade300)
          .expanded()
    ].toRow().paddingLeft(AppSpace.page);
  }
}
