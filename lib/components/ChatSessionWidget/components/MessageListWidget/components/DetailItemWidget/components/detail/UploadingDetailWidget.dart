import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/ChatSessionWidget/components/MessageListWidget/components/DetailItemWidget/DetailItemWidget.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/core/chat/message.dart';

import '../../../../../../../../config/theme/unified_style.dart';
import 'FileDetailWidget.dart';
import 'ImageDetailWidget.dart';

class UploadingDetailWidget extends StatelessWidget {
  final IMessage message;
  late final FileItemShare fileItemShare;
  final bool isSelf;

  UploadingDetailWidget(
      {super.key, required this.isSelf, required this.message}) {
    fileItemShare = message.msgBody.isNotEmpty
        ? FileItemShare.fromJson(jsonDecode(message.msgBody))
        : FileItemShare();
  }

  @override
  Widget build(BuildContext context) {
    String extension = fileItemShare.extension ?? "";
    double progress = message.progress ?? 0;
    Widget body;
    if (imageExtension.contains(extension.toLowerCase())) {
      body = ImageDetailWidget(
        isSelf: isSelf,
        message: message,
        showShadow: true,
        bgColor: isSelf ? XColors.chatmyselfbgColor : XColors.chatmyotherColor
      );
    } else {
      body = FileDetailWidget(
        isSelf: isSelf,
        message: message,
        showShadow: true,
        bgColor: isSelf ? XColors.chatmyselfbgColor : XColors.chatmyotherColor
      );
    }

    Widget gradient = Stack(
      alignment: Alignment.center,
      fit: StackFit.passthrough,
      children: [
        body,
        Text(
          progress >= 0 ? '${(progress * 100).toStringAsFixed(0)}%' : '上传失败',
          style: TextStyle(color: Colors.white, fontSize: 24.sp),
        ),
      ],
    );
    return gradient;
  }
}
