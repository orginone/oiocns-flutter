import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:orginone/components/XImage/ImageWidget.dart';
import 'package:orginone/components/ShadowWidget/ShadowWidget.dart';
import 'package:orginone/config/constant.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/main.dart';
import 'package:orginone/routers/pages.dart';

import 'BaseDetailWidget.dart';

class ImageDetailWidget extends BaseDetailWidget {
  final bool showShadow;
  late final FileItemShare msgBody;

  ImageDetailWidget(
      {super.key,
      this.showShadow = false,
      required super.isSelf,
      super.constraints = const BoxConstraints(maxWidth: 200),
      super.bgColor,
      required super.message,
      super.clipBehavior = Clip.hardEdge,
      super.padding = EdgeInsets.zero,
      super.isReply = false,
      super.chat}) {
    msgBody = FileItemShare.fromJson(jsonDecode(message.msgBody));
  }

  @override
  Widget body(BuildContext context) {
    dynamic link = msgBody.shareLink ?? '';
    dynamic thumbnail = msgBody.thumbnailUint8List;

    /// 待处理小的预览图
    if (msgBody.extension == ".gif") {
      link = '${Constant.host}$link';
    } else if (thumbnail != null) {
      link = thumbnail;
    } else if (!link.startsWith('/orginone/kernel/load/')) {
      link = File(link);
    } else {
      link = '${Constant.host}$link';
    }

    Map<String, String> headers = {
      "Authorization": kernel.accessToken,
    };
    // LogUtil.d('ImageDetail');
    // LogUtil.d(link);
    Widget child = ImageWidget(link, httpHeaders: headers, size: 200);

    if (showShadow) {
      child = ShadowWidget(
        child: child,
      );
    }

    return child;
  }

  @override
  void onTap(BuildContext context) {
    dynamic link = msgBody.shareLink ?? '';
    link = '${Constant.host}$link';
    RoutePages.jumpImagePageview(context, [link], 0);
  }
}
