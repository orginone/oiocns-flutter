import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XScaffold/XScaffold.dart';
import 'package:orginone/dart/base/schema.dart';
import 'package:orginone/dart/core/chat/message.dart';
import 'package:orginone/routers/pages.dart';
import 'package:orginone/utils/date_util.dart';
import 'package:orginone/config/theme/unified_style.dart';
import 'components/MessageReceiveItemWidget.dart';

///消息已读未读
class MessageReceiveWidget extends StatefulWidget {
  const MessageReceiveWidget({super.key});

  @override
  State<MessageReceiveWidget> createState() => _MessageReceivePageState();
}

class _MessageReceivePageState extends State<MessageReceiveWidget>
    with SingleTickerProviderStateMixin {
  List<XTarget> get readMember =>
      RoutePages.routeData.currPageData.data['readMember'] ?? [];
  List<XTarget> get unreadMember =>
      RoutePages.routeData.currPageData.data['unreadMember'] ?? [];
  List<IMessageLabel> get labels =>
      RoutePages.routeData.currPageData.data['labels'] ?? [];

  late TabController tabController;

  final List<String> tabs = ['已读', '未读'];
  // _initData() {
  //   update(["message_receive_page"]);
  // }

  @override
  void initState() {
    tabController = TabController(length: tabs.length, vsync: this);
    // _initData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return XScaffold(
      titleName: "消息接收人列表",
      backgroundColor: Colors.white,
      leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.close,
            color: Colors.black,
          )),
      body: SafeArea(
        child: _buildView(),
      ),
    );
  }

  Widget buildList(List<XTarget> targets, [List<IMessageLabel>? labels]) {
    return ListView.builder(
      itemBuilder: (context, index) {
        var target = targets[index];
        String hint = target.remark ?? "";
        if (labels != null) {
          IMessageLabel label =
              labels.firstWhere((element) => element.userId == target.id);
          hint = "已读:${CustomDateUtil.getSessionTime(label.time)}";
        }
        return MessageReceiveItemWidget(target: target, hint: hint);
      },
      itemCount: targets.length,
    );
  }

  // 主视图
  Widget _buildView() {
    return Column(
      children: [
        TabBar(
          tabs: [
            Tab(
              text: "${tabs[0]}(${readMember.length})",
            ),
            Tab(
              text: "${tabs[1]}(${unreadMember.length})",
            )
          ],
          controller: tabController,
          indicatorSize: TabBarIndicatorSize.label,
          indicatorColor: XColors.black,
          unselectedLabelColor: Colors.grey,
          unselectedLabelStyle: TextStyle(fontSize: 21.sp),
          labelColor: XColors.black,
          labelStyle: TextStyle(fontSize: 23.sp),
        ),
        Container(
          height: 10.h,
          width: double.infinity,
          color: Colors.grey.shade100,
        ),
        Expanded(
          child: TabBarView(
            controller: tabController,
            children: [
              buildList(readMember, labels),
              buildList(unreadMember),
            ],
          ),
        )
      ],
    );
  }
}
