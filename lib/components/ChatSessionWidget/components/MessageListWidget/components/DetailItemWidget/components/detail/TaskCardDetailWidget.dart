import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/components/XImage/XImage.dart';
import 'package:orginone/config/theme/UIConfig.dart';
import 'package:orginone/dart/base/model.dart';
import 'BaseDetailWidget.dart';

///任务详情
class TaskCardDetailWidget extends BaseDetailWidget {
  late final TaskInfo msgBody;

  TaskCardDetailWidget({
    super.key,
    required super.isSelf,
    required super.message,
    super.clipBehavior = Clip.hardEdge,
    super.padding = const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
    super.bgColor,
    super.constraints,
    super.isReply = false,
    super.chat,
  }) {
    msgBody = TaskInfo.fromJson(jsonDecode(message.msgBody));
  }

  @override
  Widget body(BuildContext context) {
    BoxConstraints boxConstraints =
        BoxConstraints(minWidth: 280.w, maxWidth: UIConfig.screenWidth - 110);
    Widget child = GestureDetector(
      onTap: () {
        onTap(context);
      },
      child: Container(
          constraints: boxConstraints,
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(
            horizontal: 10.w,
          ),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Container(
              // margin: EdgeInsets.symmetric(vertical: 10.h),
              padding: const EdgeInsets.only(left: 8.0, right: 8),
              alignment: Alignment.centerLeft,
              height: 40.w,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  XImage.localImage(XImage.homeTask,
                      color: Colors.blue,
                      width: 40,
                      height: 40,
                      fit: BoxFit.contain),
                  Container(
                    height: 40.w,
                    padding: const EdgeInsets.only(left: 8.0, right: 8),
                    alignment: Alignment.center,
                    child: const Text(
                      "任务",
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 40.w,
              padding: const EdgeInsets.only(left: 8.0, right: 8, top: 5),
              child: Text(
                "${msgBody.name!}  (${msgBody.period!})",
                style: const TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
                maxLines: 1,
              ),
            ),
            Container(
              // margin: EdgeInsets.symmetric(vertical: 10.h),
              padding: const EdgeInsets.only(left: 12.0, right: 8, top: 5),
              height: 35.w,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: 35.w,
                    width: 35.w,
                    padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(
                        color: Colors.lightGreen, // 设置边框颜色为蓝色
                        width: 1.0, // 设置边框宽度为1像素
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(4.w)),
                    ),
                    child: Text(
                      msgBody.periodType!,
                      style: const TextStyle(
                          color: Colors.lightGreen, fontSize: 12),
                      maxLines: 1,
                    ),
                  ),
                  Container(
                    height: 35.w,
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(left: 15.0, right: 8),
                    child: Text(
                      msgBody.content!.type!,
                      style: const TextStyle(color: Colors.blue, fontSize: 12),
                      maxLines: 1,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.h),
              height: 30.w,
              child: Container(
                height: 60.w,
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Text(
                  "办事：${msgBody.content!.workName ?? ""}",
                  style: const TextStyle(color: Colors.black87, fontSize: 14),
                  maxLines: 1,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.h),
              height: 30.w,
              child: Container(
                height: 60.w,
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Text(
                  "报表树：${msgBody.content!.treeName ?? ""}",
                  style: const TextStyle(color: Colors.black87, fontSize: 14),
                  maxLines: 1,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.h),
              height: 30.w,
              child: Container(
                height: 60.w,
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Text(
                  "数据时期：${msgBody.period ?? ""}",
                  style: const TextStyle(color: Colors.black87, fontSize: 14),
                  maxLines: 1,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.h),
              height: 30.w,
              child: Container(
                height: 60.w,
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Text(
                  "填报开始时间：${msgBody.content!.startDate ?? ""}",
                  style: const TextStyle(color: Colors.black87, fontSize: 14),
                  maxLines: 1,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.h),
              height: 30.w,
              child: Container(
                height: 60.w,
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Text(
                  "填报结束时间：${msgBody.content!.endDate ?? ""}",
                  style: const TextStyle(color: Colors.black87, fontSize: 14),
                  maxLines: 1,
                ),
              ),
            ),
            const Divider(),
            // GestureDetector(
            //   onTap: () {
            //     // 接收
            //
            //   },
            //   child: Container(
            //     margin: EdgeInsets.only(top: 5, bottom: 5),
            //     alignment: Alignment.center,
            //     child: Text(
            //       "接收",
            //       style: TextStyle(color: Colors.blue, fontSize: 16),
            //     ),
            //   ),
            // )
          ])),
    );
    return child;
  }

  @override
  void onTap(BuildContext context) async {}
}
