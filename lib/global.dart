/*
 * @Descripttion: 
 * @version: 
 * @Author: congsir
 * @Date: 2022-11-24 15:23:10
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-12-07 17:22:06
 */
import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:logging/logging.dart';
import 'package:orginone/utils/system/notify/notification_util.dart';
import 'package:orginone/dart/base/storages/hive_utils.dart';
import 'package:orginone/dart/base/storages/storage.dart';
import 'package:orginone/utils/log/log_util.dart';

import 'dart/base/common/systemError.dart';
import 'main.dart';

class Global {
  static Future<void> init() async {
    //这个表示先就行原生端（ios android）插件注册，然后再处理后续操作，这样能保证代码运行正确。
    // WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
    WidgetsFlutterBinding.ensureInitialized();
    setSystemUi();
    // 初始化通知配置
    await Storage.init();

    await HiveUtils.init();

    // await ForegroundUtils().initForegroundTask();

    // WalletChannel().init();
    // 日志初始化
    Logger.root.level = Level.ALL;
    Logger.root.onRecord.listen((event) {
      if (kDebugMode) {
        XLogUtil.d('${event.level.name}: ${event.time}: ${event.message}');
      }
    });
    SystemLog().init();

    SystemChannels.lifecycle.setMessageHandler((msg) async {
      XLogUtil.d(
          '>>>===state:$msg user:${kernel.user == null} isOnline:${kernel.isOnline} inited:${relationCtrl.provider.inited}');
      relationCtrl.appStartController.appLifecycleState = msg ?? "";
      return msg;
    });
    // 监听网络状态变化
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      XLogUtil.d('网络状态变化: $result ${result.index}');
      relationCtrl.appStartController.connectivityResult = result;
    });
  }

  // 系统样式
  static void setSystemUi() {
    if (GetPlatform.isMobile) {
      // 屏幕方向 竖直上
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      // 透明状态栏
      // SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      //   statusBarColor: Colors.transparent, // transparent status bar
      // ));
    }

    if (GetPlatform.isAndroid) {
      // 去除顶部系统下拉和底部系统按键
      // SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
      // 去掉底部系统按键
      // SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
      //     overlays: [SystemUiOverlay.bottom]);

      // 自定义样式
      SystemUiOverlayStyle systemUiOverlayStyle = const SystemUiOverlayStyle(
        // 顶部状态栏颜色
        statusBarColor: Colors.transparent,
        // 该属性仅用于 iOS 设备顶部状态栏亮度
        // statusBarBrightness: Brightness.light,
        // 顶部状态栏图标的亮度
        // statusBarIconBrightness: Brightness.light,

        // 底部状态栏与主内容分割线颜色
        systemNavigationBarDividerColor: Colors.white,
        // 底部状态栏颜色
        systemNavigationBarColor: Colors.white,
        // 底部状态栏图标样式
        systemNavigationBarIconBrightness: Brightness.dark,
      );
      SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
      SystemChrome.setApplicationSwitcherDescription(
          ApplicationSwitcherDescription(
        label: '奥集能', // 设置应用名称
        primaryColor: Colors.white.value, // 主色
      ));
    }
  }
}
