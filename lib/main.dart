/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-12-07 18:33:34
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-12-08 20:50:17
 */

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:orginone/config/theme/AppThemeData.dart';
import 'package:orginone/dart/base/api/kernelapi.dart';
import 'package:orginone/dart/controller/index.dart';
import 'package:orginone/dart/core/provider/auth.dart';
import 'package:orginone/global.dart';
import 'package:orginone/pages/auth/login_transition/index.dart';
import 'package:orginone/routers/pages.dart';
import 'package:provider/provider.dart';

import 'dart/base/common/systemError.dart';
import 'env.dart';

final SystemLog _systemLog = SystemLog();
final KernelApi _kernel = KernelApi();
KernelApi get kernel => _kernel;
final IndexController _indexController = IndexController();
IndexController get relationCtrl => _indexController;
final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

Future<void> main() async {
  //TODO：打包发布的时候修改一下发布平台
  EnvConfig.platform = ReleasePlatform.universal;

  await Global.init();

  runApp(
    const App(),
  );
}

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with AuthMixin {
  late String _errKey;

  @override
  void initState() {
    super.initState();
    relationCtrl.onInit();
    relationCtrl.appStartController.onStarting();
    // _errKey = command.subscribeByFlag("err_401", ([args]) {
    //   ToastUtils.showMsg(msg: "登陆token校验失败");
    // }, false);
  }

  @override
  void dispose() {
    super.dispose();
    // command.unsubscribeByFlag(_errKey);
  }

  @override
  Widget build(BuildContext context) {
    const Size screenSize = Size(540, 1170);
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => AppData()),
          ChangeNotifierProvider(create: (context) => RoutePages.routeData),
        ],
        child: ScreenUtilInit(
          designSize: screenSize,
          minTextAdapt: true,
          splitScreenMode: true,
          builder: (context, child) {
            return Consumer<AppData>(builder: (context, ad, child) {
              return MaterialApp(
                debugShowCheckedModeBanner: false,
                title: "奥集能",
                navigatorObservers: [RoutePages.observer],
                navigatorKey: navigatorKey,
                // onInit: () async {
                //   await relationCtrl.onInit();
                //   await relationCtrl.appStartController.onStarting();
                // },
                localizationsDelegates: const [
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate, //iOS
                ],
                supportedLocales: const [
                  Locale('zh', 'CN'),
                  Locale('en', 'US'),
                ],
                themeMode: ad.themeMode,
                // darkTheme: ThemeData(
                //     useMaterial3: false,
                //     //主标题
                //     textTheme:
                //         const TextTheme(titleLarge: TextStyle(fontSize: 19)),
                //     listTileTheme: const ListTileThemeData(
                //         titleAlignment: ListTileTitleAlignment.center)),
                theme: ad.themeData,
                // textDirection: TextDirection.ltr,
                initialRoute: initialRoute,
                routes: RoutePages.getInitRouters,
                // defaultTransition: Transition.fadeIn,
                // getPages: RoutePages.getInitRouters,
              );
            });
          },
          child: const LoginTransPage(),
        ));
  }
}

class AppData extends ChangeNotifier {
  ThemeMode themeMode = ThemeMode.system;
  late AppThemeData _themeData;
  AppData() {
    _themeData = AppThemeData();
  }

  ThemeData get themeData {
    return _themeData.defaultThemeData;
  }
}
