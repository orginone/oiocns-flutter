//路由 Pages

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart' hide Form;
import 'package:get/get.dart';
import 'package:orginone/components/FileWidget/FilePreview/BrowerWidget/BrowerWidget.dart';
import 'package:orginone/components/MySettingWidget/components/AboutWidget/components/AboutOrginoneWidget.dart';
import 'package:orginone/components/ChatSessionWidget/ChatSessionWidget.dart';
import 'package:orginone/components/MySettingWidget/components/MyCollectionWidget.dart';
import 'package:orginone/components/PortalManagerWidget/PortalManagerWidget.dart';
import 'package:orginone/components/SearchBarWidget/SearchBarWidget.dart';
import 'package:orginone/components/ComplainPage/ComplainPage.dart';
import 'package:orginone/components/ChatSessionWidget/components/MessageListWidget/components/DetailItemWidget/components/MessageReceiveWidget/MessageReceiveWidget.dart';
import 'package:orginone/components/RelationWidget/RelationCohortWidget/RelationCohortWidget.dart';
import 'package:orginone/components/RelationWidget/RelationFriendWidget/RelationFriendWidget.dart';
import 'package:orginone/components/EntityWidget/EntityInfoWidget.dart';
import 'package:orginone/components/ProcessDetailsWidget/ProcessDetailsWidget.dart';
import 'package:orginone/components/FileWidget/FilePreview/md/mark_down_preview.dart';
import 'package:orginone/components/FileWidget/FilePreview/md/text_preview.dart';
import 'package:orginone/components/FileWidget/FilePreview/pdf/view.dart';
import 'package:orginone/components/form/formInfo/form_info_page.dart';
import 'package:orginone/components/form/form_data_list_page/form_data_list_page.dart';
import 'package:orginone/components/form/form_page/view.dart';
import 'package:orginone/components/form/form_widget/form_detail/form_detail_page.dart';
import 'package:orginone/components/form/form_widget/form_tool.dart';
import 'package:orginone/components/MySettingWidget/components/SecurityWidget.dart';
import 'package:orginone/components/ActivityWidget/ActivityListWidget/components/ActivityReleaseWidget.dart';
import 'package:orginone/config/constant.dart';
import 'package:orginone/dart/base/model.dart';
import 'package:orginone/dart/base/ui.dart';
import 'package:orginone/dart/controller/index.dart';
import 'package:orginone/dart/core/chat/activity.dart';
import 'package:orginone/dart/core/chat/session.dart';
import 'package:orginone/dart/core/public/enums.dart';
import 'package:orginone/dart/core/thing/standard/application.dart';
import 'package:orginone/dart/core/thing/standard/form.dart';
import 'package:orginone/dart/core/thing/systemfile.dart';
import 'package:orginone/dart/core/work/index.dart';
import 'package:orginone/dart/core/work/task.dart';
import 'package:orginone/main.dart';
import 'package:orginone/pages/auth/forgot_password/view.dart';
import 'package:orginone/pages/auth/login/view.dart';
import 'package:orginone/pages/auth/login_scan/scan_login.dart';
import 'package:orginone/pages/auth/login_transition/index.dart';
import 'package:orginone/pages/auth/register/view.dart';
import 'package:orginone/pages/home/HomePage.dart';
import 'package:orginone/pages/portal/assets_management/assets_module/assets_type_list.dart';
import 'package:orginone/pages/portal/workBench/widgets/group_data_list.dart';
import 'package:orginone/pages/relation/relation_page.dart';
import 'package:orginone/pages/store/store_page.dart';
import 'package:orginone/utils/file_utils.dart';
import 'package:orginone/utils/log/log_util.dart';

import '../components/EntityWidget/EntityQRCodeWidget/EntityQRCodeWidget.dart';
import '../components/EntityWidget/StandardEntityInfoWidget/StandardEntityInfoWidget.dart';
import '../components/FileWidget/FileDownloadWidget/FileDownloadWidget.dart';
import '../components/FileWidget/FileListWidget/FileListWidget.dart';
import '../components/FileWidget/FilePreview/VideoPlayWidget/VideoPlayWidget.dart';
import '../components/FileWidget/FilePreview/PhotoWidget/PhotoWidget.dart';
import '../components/MemberListWidget/MemberListWidget.dart';
import '../components/MySettingWidget/components/AboutWidget/AboutWidget.dart';
import '../components/MySettingWidget/components/AboutWidget/components/VersionListWidget/VersionListWidget.dart';
import '../components/MySettingWidget/components/ErrorWidget/ErrorListWidget.dart';
import '../components/ScanWidget/ScanWidget.dart';
import '../components/MySettingWidget/components/EditUserInfomationWidget/EditUserInfomationWidget.dart';
import '../components/ActivityWidget/ActivityListWidget/components/ActivityMessageWidget/components/TargetActivityWidget/components/TargetActivityListWidget/TargetActivityListWidget.dart';
import '../components/Tip/ErrorGuideWidget.dart';
import '../dart/base/common/commands.dart';
import 'app_route.dart';
import 'observers.dart';
import 'router_const.dart';

class RoutePages {
  static final RouteObserver<Route> observer = RouteObservers();
  static final List<String> _history = [];
  static final AppRoute _routeData = AppRoute();

  static Map<String, Widget Function(BuildContext)> getInitRouters = {
    ///过度页面
    Routers.logintrans: (context) => const LoginTransPage(),

    ///登陆
    Routers.login: (context) => const LoginPage(),

    ///注册
    Routers.register: (context) => const RegisterPage(),

    ///重制密码
    Routers.forgotPassword: (context) => const ForgotPasswordPage(account: ""),

    /// 首页
    Routers.home: (context) => HomePage(),

    /// 数据页面
    Routers.storePage: (context) => const StorePage(),

    /// 关系页面
    Routers.relation: (context) => const RelationPage(),

    /// 关系>好友
    Routers.relationFriend: (context) => RelationFriendWidget(),

    /// 关系>群组
    Routers.relationCohort: (context) => RelationCohortWidget(),

    /// 实体详情
    Routers.entityInfo: (context) => EntityInfoWidget(),

    /// 好友成员列表
    Routers.memberList: (context) => MemberListWidget(),

    /// 文件目录列表
    Routers.fileList: (context) => FileListWidget(),

    ///动态详情
    Routers.targetActivity: (context) => TargetActivityListWidget(),

    ///markdown文件查看
    Routers.markDownPreview: (context) => MarkDownPreview(),

    ///text文本查看
    Routers.textPreview: (context) => TextPreview(),

    ///pdf文件查看
    Routers.pdfReader: (context) => PDFReaderPage(),

    ///图片查看
    Routers.photoView: (context) => const PhotoWidget(),

    ///视频播放
    Routers.videoPlay: (context) => const VideoPlayWidget(),

    ///webview查看
    Routers.webView: (context) => const BrowerWidget(),

    ///文件下载
    Routers.messageFile: (context) => const FileDownloadWidget(),

    ///动态发布
    Routers.activityRelease: (context) => ActivityReleaseWidget(),

    ///错误日志
    Routers.errorPage: (context) => const ErrorListWidget(),

    // ///用户信息
    // Routers.userInfo: (context) => const UserInfoWidget(),

    /// 沟通会话页面
    Routers.chatSession: (context) => ChatSessionWidget(),

    ///已读未读列表
    Routers.messageReceive: (context) => const MessageReceiveWidget(),

    ///办事详情
    Routers.processDetails: (context) => ProcessDetailsWidget(),

    // ///办事审批意见
    // Routers.approveOpinion: (context) => const ApproveOpinionPage(),

    ///办事子表详情
    Routers.formDetail: (context) => FormDetailPage(),

    ///数据--表单数据预览详情
    Routers.formDetailPreview: (context) => FormDetailPreviewPage(),

    ///表单
    Routers.formPage: (context) => FormPage(),

    ///表单数据列表
    Routers.formDataListPage: (context) => FormDataListPage(),

    ///通用信息查看页面
    Routers.standardEntityDetailPage: (context) => StandardEntityInfoWidget(),
    Routers.standardEntityInfoPage: (context) => EntityInfoWidget(),
    Routers.formInfoPage: (context) => FormInfoPage(),
    Routers.appAssetListPage: (context) => AssetsTypeListPage(),

    ///举报
    Routers.complainPage: (context) => const ComplainPage(),

    ///我的收藏
    Routers.myCollection: (context) => const MyCollectionWidget(),

    /// 好友成员列表
    Routers.showactivitymsgpage: (context) => ShowActivityPage(),

    /// 个人信息
    Routers.editUserInfo: (context) => const EditUserInfomationWidget(),

    ///集群消息列表
    Routers.groupList: (context) => GroupDataList(),

    ///分享二维码
    Routers.shareQrCode: (context) => EntityQRCodeWidget(),

    ///扫码页面
    Routers.scan: (context) => const ScanWidget(),

    ///扫码登陆
    Routers.scanLogin: (context) => ScanLoginPage(),

    ///门户设置
    Routers.security: (context) => const SecurityWidget(),

    ///关于奥集能
    Routers.about: (context) => const AboutWidget(),

    ///版本信息
    Routers.originone: (context) => const AboutOrginoneWidget(),

    ///版本列表
    Routers.versionList: (context) => const VersionListWidget(),

    ///门户模版新增
    Routers.portalManager: (context) => const PortalManagerWidget(),

    ///异常引导页面
    Routers.errorGuide: (context) => ErrorGuideWidget(),
  };

  static AppRoute get routeData {
    return _routeData;
  }

  static List<String> history() {
    return _history;
  }

  static void addHistory(String? path) {
    if (null != path && path.isNotEmpty) {
      _history.add(path);
      if (_routeData.currPageData.path != path) {
        _routeData.currPageData.path = path;
      }
    }
  }

  static void removeHistory(String? path) {
    if (null != path && path.isNotEmpty) {
      _history.remove(path);
    }
    if (path == _routeData.currPageData.path) {
      _routeData.back();
    }
  }

  static bool isHomePage() {
    XLogUtil.dd("did 路由信息：${_routeData.pageHistorys.length}");
    return _routeData.currPageData.path == Routers.home &&
        _routeData.pageHistorys.isEmpty;
  }

  static dynamic get getPageParams {
    return RoutePages.routeData.currPageData.data;
  }

  static Future<T?> _jumpPush<T extends Object?>(
      {BuildContext? context,
      String? path,
      String? title,
      dynamic parameters,
      dynamic data,
      Widget? child}) {
    context ??= navigatorKey.currentState?.context;
    if (null == context) {
      XLogUtil.e("页面跳转参数异常");
      return Future(() => null);
    }

    if (data is IEntityUI) {
      return _routeData.go<T>(context, path,
          title: title, entity: data, data: parameters ?? data, child: child);
    } else {
      return _routeData.go<T>(context, path,
          title: title, data: parameters ?? data, child: child);
    }
  }

  ///路由跳转
  static Future<T?> to<T extends Object?>(
      {BuildContext? context,
      String? path,
      dynamic parameters,
      dynamic data,
      String? title,
      Widget? child}) {
    return _jumpPush<T>(
        context: context,
        path: path,
        data: data,
        parameters: parameters,
        child: child,
        title: title);
  }

  static void back([BuildContext? context]) {
    _routeData.back(context);
  }

  static void changeTransition([Transition? transition]) {
    Get.config(
      enableLog: Get.isLogEnable,
      defaultTransition: transition ?? Transition.native,
      defaultOpaqueRoute: Get.isOpaqueRouteDefault,
      defaultPopGesture: Get.isPopGestureEnable,
      defaultDurationTransition: Get.defaultTransitionDuration,
    );
  }

  /// 跳转到动态发布页面
  static void jumpActivityRelease(BuildContext context,
      {required IActivity activity}) {
    _jumpPush(context: context, path: Routers.activityRelease, data: activity);
    // Get.toNamed(Routers.activityRelease,
    //     arguments: RouterParam(
    //         parents: [..._getParentRouteParams(), RouterParam(datas: activity)],
    //         datas: activity));
  }

  /// 跳转到markdown查看页面
  static void jumpAssectMarkDown(
      {BuildContext? context,
      String? title,
      String? path,
      FileItemShare? file}) {
    if (null == file && null != title && null != path) {
      file = FileItemShare(name: title, shareLink: path, extension: "md");
    }
    _jumpPush(
        context: context,
        path: Routers.markDownPreview,
        title: title,
        data: file);
  }

  static void jumpText({required String text}) {
    RoutePages.to(path: Routers.textPreview, title: '文本信息', data: text);
  }

  static void jumpNewWork({BuildContext? context, required IWork work}) {
    XLogUtil.i(
        '网页地址：http://192.168.31.190:8080/?companyId=${work.belongId}&appId=${work.application.id}&workId=${work.id}');
    // String url =
    //     'http://192.168.31.154:8080/?companyId=${work.belongId}&appId=${work.application.id}&workId=${work.id}';
    String url =
        'https://h5.orginone.cn/?companyId=${work.belongId}&appId=${work.application.id}&workId=${work.id}';
    jumpWeb(context: context, arguments: work, url: url);
    // jumpWeb(

    //     arguments: work,
    //     url:
    //         'http://192.168.31.190:8080/?companyId=${work.belongId}&appId=${work.application.id}&workId=${work.id}');
  }

  static void jumpFile(
      {BuildContext? context,
      required FileItemShare file,
      String type = 'chat'}) {
    var extension = file.extension?.toLowerCase() ?? "";
    //TODO:react分支 无此方法  具体调试看这个业务是什么
    // if (type == "store") {
    //   settingCtrl.store.onRecordRecent(RecentlyUseModel(
    //       type: StoreEnum.file.label,
    //       file: FileItemModel.fromJson(file.toJson())));
    // }
    if (FileUtils.isPdf(extension)) {
      _jumpPush(
          context: context,
          path: Routers.pdfReader,
          title: file.name,
          data: file);
    } else if (FileUtils.isImage(extension)) {
      _jumpPush(
          context: context,
          path: Routers.photoView,
          title: file.name,
          data: {
            "images": file.shareLink != null && file.shareLink!.contains('http')
                ? [file.shareLink!]
                : ['${Constant.host}${file.shareLink}']
          });
    } else if (FileUtils.isWord(extension)) {
      // TODO 临时处理方案，让其可以下载通过第三方软件查看
      // _jumpPush(context:context, path:RouterRouters.messageFiles.fileReader, title:file.name,data: {'file': file});
      // _jumpPush(, title:file.name,data: {'file': file});
      if (Platform.isIOS) {
        jumpWeb(url: '${Constant.host}${file.shareLink}');
      } else {
        _jumpPush(
            context: context,
            path: Routers.messageFile,
            title: file.name,
            data: file);
      }
    } else if (FileUtils.isVideo(extension)) {
      _jumpPush(
          context: context,
          path: Routers.videoPlay,
          title: file.name,
          data: file);
    } else if (FileUtils.isMarkDown(extension)) {
      RoutePages.jumpAssectMarkDown(file: file);
    } else {
      _jumpPush(
          context: context,
          path: Routers.messageFile,
          title: file.name,
          data: file);
    }
  }

  static void jumpImagePageview(
      BuildContext context, List<String> file, int initIndex) {
    RoutePages.to(
        context: context,
        path: Routers.photoView,
        data: {"images": file, "index": initIndex});
  }

  /// 跳转到数据二级页面
  static void jumpStore(BuildContext context,
      {dynamic parentData, List? listDatas}) async {
    if (parentData is SysFileInfo) {
      return RoutePages.jumpFile(
          context: context, file: parentData.shareInfo());
    }
    SpaceEnum? type = SpaceEnum.getType(parentData.typeName);
    switch (type) {
      case SpaceEnum.report:
      case SpaceEnum.form:
        Form form = await FormTool.loadForm(parentData);
        _jumpDetailOrInfoPage(context, Routers.formDataListPage, form);
        break;
      case SpaceEnum.property:
      case SpaceEnum.dict:
      case SpaceEnum.species:
        _jumpDetailOrInfoPage(
            context, Routers.standardEntityDetailPage, parentData);
        break;
      case SpaceEnum.work:
      case SpaceEnum.workStore:
        RoutePages.jumpNewWork(work: parentData);
        break;
      default:
        to(data: parentData, parameters: listDatas);
    }
  }

  /// 点击叹号调用 跳转到数据详情页面
  /// data 需要展示的目标对象
  /// defaultActiveTabs 默认激活页签
  static void jumpStoreInfoPage(BuildContext context,
      {dynamic data, List<String> defaultActiveTabs = const []}) async {
    XLogUtil.d('jumpStoreInfoPage');
    XLogUtil.d(data.runtimeType);
    XLogUtil.d(data.groupTags);
    SpaceEnum? spaceEnum = SpaceEnum.getType(data.typeName);
    WorkType? workType = WorkType.getType(data.typeName);
    TargetType? targetType = TargetType.getType(data.typeName);
    if (data is SysFileInfo) {
      _jumpDetailOrInfoPage(context, Routers.standardEntityInfoPage, data);
      return;
    }

    switch (workType) {
      case WorkType.thing:
        _jumpDetailOrInfoPage(context, Routers.standardEntityInfoPage, data);
        break;
      default:
      // _jumpHomeSub(home: HomeEnum.store, parentData: data, listDatas: data);
    }
    switch (targetType) {
      case TargetType.person:
      case TargetType.company:
        _jumpDetailOrInfoPage(context, Routers.standardEntityInfoPage, data);
        break;
      default:
      // _jumpHomeSub(home: HomeEnum.store, parentData: data, listDatas: data);
    }

    switch (spaceEnum) {
      // case SpaceEnum.form:
      //   Form form = await FormTool.loadForm(data);
      //   _jumpDetailPage(Routers.formPage, form);
      //   break;
      // case SpaceEnum.form:

      case SpaceEnum.person:
      case SpaceEnum.property:
      case SpaceEnum.dict:
      case SpaceEnum.species:
      case SpaceEnum.applications:
      case SpaceEnum.work:
      case SpaceEnum.workStore:
      case SpaceEnum.module:
        _jumpDetailOrInfoPage(context, Routers.standardEntityInfoPage, data);
        break;
      case SpaceEnum.form:
        _jumpDetailOrInfoPage(context, Routers.formInfoPage, data);
        break;
      default:
      // _jumpHomeSub(home: HomeEnum.store, parentData: data, listDatas: data);
    }
  }

  static _jumpDetailOrInfoPage(
      BuildContext context, String router, dynamic params) {
    RoutePages.to(context: context, path: router, data: params);
  }

  static jumpFormPage(BuildContext context, dynamic params) {
    RoutePages.to(context: context, path: Routers.formPage, data: params);
  }

  /// 跳转到关系二级页面
  static void jumpRelation({dynamic parentData, List? listDatas}) {
    _jumpHomeSub(
        home: HomeEnum.relation, parentData: parentData, listDatas: listDatas);
  }

  /// 跳转到关系详情页面
  /// data 需要展示的目标对象
  /// defaultActiveTabs 默认激活页签
  static void jumpRelationInfo(
      {BuildContext? context, dynamic data, List<String>? defaultActiveTabs}) {
    if (data.typeName == TargetType.person.label) {
      _jumpPush(
          context: context,
          path: Routers.relationFriend,
          data: data,
          parameters: defaultActiveTabs);
      // Get.toNamed(Routers.relationFriend,
      //     preventDuplicates: false,
      //     arguments: RouterParam(
      //         parents: [..._getParentRouteParams(), RouterParam(datas: data)],
      //         defaultActiveTabs: defaultActiveTabs ?? ["设置"],
      //         datas: data));
    } else if (data.typeName == SpaceEnum.property.label) {
    } else {
      RoutePages.to(
          context: context,
          path: Routers.relationCohort,
          data: data,
          parameters: defaultActiveTabs);
    }
  }

  /// 跳转到关系详情页面
  /// data 需要展示的目标对象
  /// defaultActiveTabs 默认激活页签
  static void jumpRelationMember(
      {BuildContext? context, dynamic data, List<String>? defaultActiveTabs}) {
    if (data.typeName == TargetType.person.label) {
      _jumpPush(context: context, path: Routers.relationFriend, data: data);
      // Get.toNamed(Routers.relationFriend,
      //     arguments: RouterParam(
      //         parents: [..._getParentRouteParams(), RouterParam(datas: data)],
      //         defaultActiveTabs: defaultActiveTabs ?? [TargetType.person.label],
      //         datas: data));
    } else if (data.typeName != TargetType.storage.label) {
      _jumpPush(context: context, path: Routers.relationCohort, data: data);
      // Get.toNamed(Routers.relationCohort,
      //     arguments: RouterParam(
      //         parents: [..._getParentRouteParams(), RouterParam(datas: data)],
      //         defaultActiveTabs: defaultActiveTabs ?? [SpaceEnum.member.label],
      //         datas: data));
    }
  }

  /// 好友/成员列表
  static void jumpMemberList<T>(BuildContext context, {required T data}) {
    RoutePages.to(context: context, path: Routers.memberList, data: data);
  }

  /// 动态列表搜索结果页
  static void jumpActivityPage<T>(BuildContext context, {required T data}) {
    _jumpPush(context: context, path: Routers.showactivitymsgpage, data: data);
    RoutePages.to(
        context: context, path: Routers.showactivitymsgpage, data: data);
  }

  /// 跳转到实体详情页面
  static void jumpEneityInfo<T>(BuildContext context, {required T data}) {
    RoutePages.to(context: context, path: Routers.entityInfo, data: data);
    // {
    //   "parents": [..._getParentRouteParams(), data],
    //   "defaultActiveTabs": [],
    //   "datas": data
    // }
  }

  /// 跳转到文件目录列表页面
  static void jumpFileList<T>({BuildContext? context, required T data}) {
    _jumpPush(context: context, path: Routers.fileList, data: data);
  }

  ///跳转应用下分类办事页面
  static void jumpAppAssetsList(BuildContext context,
      {required IApplication iApplication}) {
    _jumpPush(
        context: context, path: Routers.appAssetListPage, data: iApplication);
    // Navigator.push(
    //   navigatorKey.currentState!.context,
    //   SlidePageRoute(
    //     builder: (BuildContext context) {
    //       return AssetsTypeListPage(); // 要导航到的页面
    //     },
    //     // ... 其他参数
    //   ),
    // );
    // Get.toNamed(Routers.appAssetListPage,
    //     arguments: RouterParam(parents: [
    //       ..._getParentRouteParams(),
    //       RouterParam(datas: iApplication)
    //     ], datas: iApplication));
  }

  /// 跳转到消息模块
  static void jumpChat({List<String> defaultActiveTabs = const []}) {
    _jumpHomeSub(home: HomeEnum.chat, defaultActiveTabs: defaultActiveTabs);
  }

  /// 跳转到办事模块
  static void jumpWork({List<String> defaultActiveTabs = const []}) {
    _jumpHomeSub(home: HomeEnum.work, defaultActiveTabs: defaultActiveTabs);
  }

  /// 跳转到数据子模块
  static void jumpStoreSub(
      {dynamic parentData,
      List? listDatas,
      List<String> defaultActiveTabs = const [],
      bool preventDuplicates = false}) {
    _jumpHomeSub(
        home: HomeEnum.store,
        parentData: parentData,
        listDatas: listDatas,
        defaultActiveTabs: defaultActiveTabs);
  }

  /// 跳转到网页
  static void jumpWeb(
      {BuildContext? context, required String url, dynamic arguments}) {
    _jumpPush(
        context: context,
        path: Routers.webView,
        data: {'url': url, 'data': arguments});
    // Get.toNamed(Routers.webView,
    //     arguments: arguments, parameters: {'url': url});
  }

  /// 跳转到首页子页面
  static void _jumpHomeSub(
      {required HomeEnum home,
      dynamic parentData,
      List? listDatas,
      List<String> defaultActiveTabs = const [],
      bool preventDuplicates = false}) {
    _jumpHome(
        home: home,
        parentData: parentData,
        listDatas: listDatas,
        defaultActiveTabs: defaultActiveTabs,
        preventDuplicates: preventDuplicates,
        page: Routers.homeSub);
  }

  /// 跳转到门户
  static void jumpPortal(
      {dynamic parentData,
      List? listDatas,
      List<String> defaultActiveTabs = const [],
      bool preventDuplicates = false}) {
    _jumpHome(
        home: HomeEnum.door,
        parentData: parentData,
        listDatas: listDatas,
        defaultActiveTabs: defaultActiveTabs,
        preventDuplicates: preventDuplicates);
  }

  // static void jumpTo(String path) {
  //   if (Get.currentRoute != path) {
  //     LogUtil.dd("跳转到路由:$path");
  //     Get.offAndToNamed(path);
  //   }
  // }

  static void jumpHome({HomeEnum home = HomeEnum.door, IEntityUI? parentData}) {
    _jumpHome(home: home, parentData: parentData);
  }

  static void _jumpHome(
      {required HomeEnum home,
      String? page,
      dynamic parentData,
      List? listDatas,
      List<String> defaultActiveTabs = const [],
      bool preventDuplicates = false}) {
    if (relationCtrl.homeEnum.value != home) {
      relationCtrl.homeEnum.value = home;
      command.emitterFlag('switchHome');
    }
    _jumpPush(path: Routers.home, data: listDatas ?? parentData);
    // if (relationCtrl.homeEnum.value != home) {
    //   relationCtrl.setHomeEnum(home);
    //   Get.offAndToNamed(Routers.home,
    //       arguments: RouterParam(
    //           modelName: home.label,
    //           parents: [
    //             ..._getParentRouteParams(),
    //             if (null != parentData) RouterParam(datas: parentData)
    //           ],
    //           datas: listDatas,
    //           defaultActiveTabs: defaultActiveTabs));
    // } else {
    //   Get.toNamed(page ?? Routers.home,
    //       preventDuplicates: preventDuplicates,
    //       arguments: RouterParam(
    //           modelName: home.label,
    //           parents: [
    //             if (parentData is! ICompany) ..._getParentRouteParams(),
    //             if (null != parentData) RouterParam(datas: parentData)
    //           ],
    //           datas: listDatas,
    //           defaultActiveTabs: defaultActiveTabs));
    // }
  }

  static void jumpChatSession(
      {BuildContext? context, required ISession data, dynamic defaultTabs}) {
    jumpRelationMember(
        context: context, data: data, defaultActiveTabs: defaultTabs ?? ["沟通"]);
  }

  /// 跳转到办事详情
  static Future<void> jumpWorkInfo<T extends IWorkTask>(
      {BuildContext? context, required T work}) async {
    // if (work.targets.isEmpty) {
    //   //加载流程实例数据
    //   await work.loadInstance();
    // }
    //跳转办事详情
    _jumpPush(
        context: context,
        path: Routers.processDetails,
        data: work,
        title: work.name);
  }

  ///跳转动态详情
  static void jumpActivityInfo(IActivity data) {
    // Get.toNamed(Routers.targetActivity,
    //     arguments: RouterParam(
    //       parents: [..._getParentRouteParams(), RouterParam(datas: data)],
    //       datas: data,
    //     ));
  }

  ///工作台跳转集群相关页面
  static void jumpGroupDataListByCompany<T extends IEntityUI>(
      BuildContext context, List<T> data, IEntityUI? spaceEntity) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return GroupDataList<T>(
        data: data,
        spaceEntity: spaceEntity,
      );
    }));
  }

  /// 批量上传文件
  static void jumpUploadFiles({FilePickerResult? filePicker}) {}

  /// 获取关系子页面参数
  static List<RouterParam> _getParentRouteParams() {
    var params = RoutePages.routeData.currPageData.data;
    if (isValidParams()) {
      if (params is RouterParam) {
        return params.parents;
      } else if (params is Map) {
        return params["parents"];
      }
    }
    return [];
  }

  /// 获取关系子页面参数
  static String? getRouteTitle() {
    var params = RoutePages.routeData.currPageData.entity;
    if (params is IEntityUI) {
      return params.name;
    }
    return null;
  }

  /// 获取关系子页面参数
  static List<String>? getRouteDefaultActiveTab() {
    var params = RoutePages.routeData.currPageData.data;
    // if (isValidParams()) {
    if (params is RouterParam) {
      return params.defaultActiveTabs;
    } else if (params is Map) {
      return params["defaultActiveTabs"];
    } else if (params is List<String>) {
      return params;
    }
    // }
    return null;
  }

  /// 获取关系子页面参数
  static dynamic getParentRouteParam() {
    var params = RoutePages.routeData.currPageData.entity ??
        RoutePages.routeData.currPageData.data;

    return params;
  }

  /// 获取关系子页面参数
  static dynamic getRootRouteParam() {
    var params = RoutePages.routeData.currPageData.data;

    if (isValidParams()) {
      if (params is RouterParam) {
        return params.parents.first.datas;
      } else if (params is Map) {
        return params["parents"].first.datas;
      }
    }
    return;
  }

  /// 路由层级
  static int getRouteLevel() {
    // var params = RoutePages.routeData.currPageData.data;
    // if (isValidParams()) {
    //   if (params is RouterParam) {
    //     return params.parents.length;
    //   } else if (params is Map) {
    //     return params["parents"].length;
    //   }
    // }
    // return Get.currentRoute == Routers.home ? 0 : 1;
    return RoutePages.history().length - 1;
  }

  /// 获取关系子页面参数
  static dynamic getRouteParams({HomeEnum? homeEnum}) {
    // var params = RoutePages.routeData.currPageData.data;
    // if (isValidParams(homeEnum: homeEnum) && getRouteLevel() > 0) {
    //   if (params is RouterParam) {
    //     return params.datas;
    //   } else if (params is Map) {
    //     return params["datas"];
    //   }
    // }
    return _routeData.currPageData.data;
  }

  static bool isValidParams({HomeEnum? homeEnum}) {
    var params = RoutePages.routeData.currPageData.data;
    if (params is RouterParam) {
      return null == homeEnum
          ? true
          : homeEnum == relationCtrl.homeEnum.value
              ? getRouteLevel() > 0
              : false;
    } else if (params is Map) {
      return params["parents"]?.firstOrNull?['modelName'] ==
          relationCtrl.homeEnum.value.label;
    }
    return false;
  }

  static void jumpSetting(BuildContext context, SettingEnum item) {
    switch (item) {
      // case SettingEnum.security:
      //   // _jumpPush(context: context, path: Routers.forgotPassword);
      //   break;
      // // case SettingEnum.cardbag:
      // //   RoutePages.jumpCardBag();
      // //   break;
      // case SettingEnum.gateway:
      //   _jumpPush(
      //     context: context,
      //     path: Routers.security,
      //   );
      //   break;
      // case SettingEnum.theme:
      //   break;
      case SettingEnum.errorLog:
        _jumpPush(
          context: context,
          path: Routers.errorPage,
        );
        break;
      // case SettingEnum.vsrsion:
      //   _jumpPush(context:context, path:
      //     Routers.version,
      //   );
      //   break;
      case SettingEnum.about:
        _jumpPush(
          context: context,
          path: Routers.about,
        );
        break;
      case SettingEnum.exitLogin:
        relationCtrl.exitLogin();
        break;
      case SettingEnum.collection:
        _jumpPush(context: context, path: Routers.myCollection);
        break;
      case SettingEnum.edit:
        _jumpPush(
          context: context,
          path: Routers.editUserInfo,
        );
        break;
    }
  }
}
