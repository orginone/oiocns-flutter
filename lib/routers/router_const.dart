/*
 * @Descripttion: 
 * @version: 
 * @Author: congsir
 * @Date: 2022-11-24 15:23:10
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-12-08 14:13:16
 */
class Routers {
  /// 登录
  static const String login = "/login";

  ///扫描二维码
  static const String scan = "/scan";

  ///扫码登录
  static const String scanLogin = "/scanLogin";

  // ///验证码
  // static const String verificationCode = "/verificationCode";

  ///注册
  static const String register = "/register";

  ///忘记密码
  static const String forgotPassword = "/forgotPassword";

  ///登录过渡加载页面
  static const String logintrans = "/logintrans";

  ///关于奥集能
  static const String about = "/about";

  ///版本信息
  static const String versionList = "/versionList";

  ///版本更新
  static const String originone = "/originone";

  ///错误日志
  static const errorPage = '/error_page';

  ///版本信息
  // static const String version = "/version";
  ///用户信息
  static const String userInfo = "/userInfo";

  ///安全
  static const String security = "/security";

  /// 首页
  static const String home = "/home";
  static const String homeSub = "/homeSub";

  ///动态
  static const String targetActivity = "/targetActivity";

  ///动态发布
  static const String activityRelease = "/activityRelease";

  /// 数据
  static const String storePage = "/storePage";

  /// 标准数据信息页面
  static const String standardEntityInfoPage = "/standardEntityInfoPage";

  /// 表单信息页面 formInfoPage
  static const String formInfoPage = "/formInfoPage";

  /// 标准数据详情页 standardEntityDetailPage
  static const String standardEntityDetailPage = "/standardEntityDetailPage";

  /// 关系
  static const String relation = "/relation";

  /// 好友关系
  static const String relationFriend = "/relationFriend";

  /// 群组关系
  static const String relationCohort = "/relationCohort";

  /// 共用页面
  ///
  /// 实体信息
  static const String entityInfo = "/entityInfo";

  ///分享二维码
  static const String shareQrCode = "/shareQrCode";

  /// 好友/成员列表
  static const String memberList = "/memberList";

  /// 文件目录列表
  static const String fileList = "/fileList";

  /// 沟通会话页面
  static const String chatSession = "/chatSession";

  ///已读未读列表
  static const String messageReceive = "/messageReceive";
  static const String messageChat = "/messageChat";

  ///webView
  static const String webView = "/webView";

  ///文件下载页面
  static const String messageFile = "/messageFile";

  ///pdf预览
  static const String pdfReader = "/pdfReader";

  ///图片预览
  static const String photoView = "/photoView";

  ///视频播放
  static const String videoPlay = "/videoPlay";

  ///markdown预览
  static const String markDownPreview = "/markDownPreview";

  ///文本预览
  static const String textPreview = "/textPreview";

  // static const String fileReader = "/fileReader";

  /// 待办
  // static const String todo = "/todo";
  // static const String todoList = "/todoList";
  // static const String todoDetail = "/todoDetail";

  ///审批详情
  static const String processDetails = '/processDetails';

  ///审批意见
  static const String approveOpinion = '/approveOpinion';

  // // 简单表单编辑器
  // static const String form = "/form";
  /// 简单表单编辑器
  static const String formPage = "/formPage";

  /// 表单数据列表
  static const String formDataListPage = "/formDataListPage";

  ///表单详情
  static const String formDetail = "/formDetail";

  ///应用下资产分类列表
  static const String appAssetListPage = "/appAssetListPage";

  ///表单数据预览详情
  static const String formDetailPreview = "/formDetailPreview";

  ///举报
  static const String complainPage = "/complainPage";

  /// 动态列表
  static const String showactivitymsgpage = "/showactivitymsg";

  ///我的收藏
  static const String myCollection = "/myCollection";

  ///个人信息
  static const String editUserInfo = "/editUserInfo";

  ///个人信息
  static const String groupList = "/groupList";

  /// 模版编辑页
  static const String portalManager = "/portalManager";

  /// 异常引导页面
  static const String errorGuide = "/Routers.errorGuide";
}
